//
//  ANSViewController.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"
#import "StudentEntity.h"
#import "AvatarsViewController.h"

@class StudentsViewController;

@interface ANSViewController : UIViewController <UITextFieldDelegate, AvatarsViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
	StudentsViewController __weak *studentsViewController;
	StudentEntity *student;
	
	IBOutlet UIView *contentView;
	IBOutlet UIImageView *backgroundImageView;
	IBOutlet UIButton *photoButton;
	IBOutlet FXLabel *newPlayerNameLabel;
	IBOutlet UITextField *nameTextField;
	IBOutlet UIButton *deleteButton;
	IBOutlet UIButton *cancelButton;
	IBOutlet UIButton *saveButton;
	
	UIImage *photo;
}

@property (nonatomic, weak) StudentsViewController *studentsViewController;
@property (nonatomic, strong) StudentEntity *student;

- (IBAction)onPhotoButton:(id)sender;
- (IBAction)onDeleteButton:(id)sender;
- (IBAction)onCancelButton:(id)sender;
- (IBAction)onSaveButton:(id)sender;

@end
