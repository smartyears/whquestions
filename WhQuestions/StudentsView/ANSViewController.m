//
//  ANSViewController.m
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "ANSViewController.h"
#import "StudentsViewController.h"
#import "CustomImagePickerController.h"
#import "NSString+Folder.h"

@interface ANSViewController ()

@end

@implementation ANSViewController

@synthesize studentsViewController;
@synthesize student;

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.preferredContentSize = CGSizeMake(screenWidth, screenHeight);
	
	if (IS_IPAD)
		newPlayerNameLabel.font = [UIFont fontWithName:ARIAL_FONT size:24];
	else
		newPlayerNameLabel.font = [UIFont fontWithName:ARIAL_FONT size:12];
	newPlayerNameLabel.characterSpacing = -0.05;
	newPlayerNameLabel.oversampling = 4;
	newPlayerNameLabel.text = getStringWithKey(NEW_PLAYER_NAME_STRING);
	
	if (IS_IPAD)
		nameTextField.font = [UIFont fontWithName:ARIAL_FONT size:24];
	else
		nameTextField.font = [UIFont fontWithName:ARIAL_FONT size:12];
	
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_FONT size:24];
		else
			label.font = [UIFont fontWithName:ARIAL_FONT size:12];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:227 / 255.0 green:14 / 255.0 blue:31 / 255.0 alpha:1.0];
		label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
		if (IS_IPAD)
			label.shadowOffset = CGSizeMake(0, 1);
		else
			label.shadowOffset = CGSizeMake(0, 0.5);
		label.characterSpacing = -0.05;
		label.oversampling = 4;
		label.text = getStringWithKey(DELETE_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[deleteButton setImage:buttonImage forState:UIControlStateNormal];
	}
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_FONT size:24];
		else
			label.font = [UIFont fontWithName:ARIAL_FONT size:12];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:227 / 255.0 green:14 / 255.0 blue:31 / 255.0 alpha:1.0];
		label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
		if (IS_IPAD)
			label.shadowOffset = CGSizeMake(0, 1);
		else
			label.shadowOffset = CGSizeMake(0, 0.5);
		label.characterSpacing = -0.05;
		label.oversampling = 4;
		label.text = getStringWithKey(CANCEL_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[cancelButton setImage:buttonImage forState:UIControlStateNormal];
	}
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_FONT size:24];
		else
			label.font = [UIFont fontWithName:ARIAL_FONT size:12];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:227 / 255.0 green:14 / 255.0 blue:31 / 255.0 alpha:1.0];
		label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
		if (IS_IPAD)
			label.shadowOffset = CGSizeMake(0, 1);
		else
			label.shadowOffset = CGSizeMake(0, 0.5);
		label.characterSpacing = -0.05;
		label.oversampling = 4;
		label.text = getStringWithKey(SAVE_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[saveButton setImage:buttonImage forState:UIControlStateNormal];
	}
	
	if (IS_IPAD)
		photoButton.layer.cornerRadius = 8;
	else
		photoButton.layer.cornerRadius = 4;
	photoButton.clipsToBounds = YES;

	if (student == nil) {
		nameTextField.text = @"";
		photo = [UIImage imageNamed:@"ANSEmpty.png"];

		backgroundImageView.image = [UIImage imageNamed:@"ANSBackgroundPad.png"];
		deleteButton.hidden = YES;
		
		{
			FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, photoButton.frame.size.width, photoButton.frame.size.height)];
			label.backgroundColor = [UIColor whiteColor];
			label.textColor = [UIColor blackColor];
			label.textAlignment = NSTextAlignmentCenter;
			label.numberOfLines = 0;
			label.lineSpacing = -0.2;
			label.oversampling = 4;
			label.text = getStringWithKey(TAP_TO_ADD_PHOTO_STRING);
			
			NSInteger fontSize;
			if (IS_IPAD)
				fontSize = 33;
			else
				fontSize = 16;
			while (YES) {
				label.frame = CGRectMake(0, 0, photoButton.frame.size.width, MAXFLOAT);
				label.font = [UIFont fontWithName:@"LaneHumouresque" size:fontSize];
				[label sizeToFit];
				if (label.frame.size.height <= photoButton.frame.size.height || fontSize == 1) {
					break;
				}
				else {
					fontSize --;
				}
			}
			label.frame = CGRectMake(0, 0, photoButton.frame.size.width, photoButton.frame.size.height);
			
			UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
			[label.layer renderInContext:UIGraphicsGetCurrentContext()];
			UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
			
			[photoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
		}
	}
	else {
		nameTextField.text = student.name;
		photo = [student headerImage];
		[photoButton setBackgroundImage:[self getPhotoButtonImage:photo] forState:UIControlStateNormal];
	}
}

- (IBAction)onPhotoButton:(id)sender {
	[nameTextField resignFirstResponder];
	
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(CANCEL_STRING) style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(AN_AVATAR_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self onAvatarButton];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(STUDENT_PHOTO_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self onPhotoButton];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(NOTHING_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self onNothingButton];
    }]];
    
    if (IS_IPAD) {
        alertController.modalPresentationStyle = UIModalPresentationPopover;
        alertController.popoverPresentationController.sourceView = contentView;
        alertController.popoverPresentationController.sourceRect = photoButton.frame;
    }
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)onAvatarButton {
	if (IS_IPAD) {
        AvatarsViewController *avatarsViewController = [[AvatarsViewController alloc] initWithNibName:@"AvatarsViewController" bundle:nil];
        avatarsViewController.delegate = self;
        CustomNavigationController *navigationController = [[CustomNavigationController alloc] initWithRootViewController:avatarsViewController];
        
        navigationController.modalPresentationStyle = UIModalPresentationPopover;
        navigationController.popoverPresentationController.sourceRect = photoButton.frame;
        navigationController.popoverPresentationController.sourceView = contentView;
        navigationController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
        [studentsViewController presentViewController:navigationController animated:YES completion:nil];
	}
	else {
		AvatarsViewController *avatarsViewController = [[AvatarsViewController alloc] initWithNibName:@"AvatarsViewController" bundle:nil];
		avatarsViewController.delegate = self;
		CustomNavigationController *navigationController = [[CustomNavigationController alloc] initWithRootViewController:avatarsViewController];
		[studentsViewController presentViewController:navigationController animated:YES completion:nil];
	}
}

- (void)onPhotoButton {
	if (IS_IPAD) {
        CustomImagePickerController *imagePickerController = [[CustomImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        imagePickerController.modalPresentationStyle = UIModalPresentationPopover;
        imagePickerController.popoverPresentationController.sourceView = contentView;
        imagePickerController.popoverPresentationController.sourceRect = photoButton.frame;
        [studentsViewController presentViewController:imagePickerController animated:YES completion:nil];
	}
	else {
		CustomImagePickerController *imagePickerController = [[CustomImagePickerController alloc] init];
		imagePickerController.delegate = self;
		imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		imagePickerController.allowsEditing = YES;
		[studentsViewController presentViewController:imagePickerController animated:YES completion:nil];
	}
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)onNothingButton {
	if (photo) {
		photo = nil;
	}
	photo = [UIImage imageNamed:@"ANSEmpty.png"];
	[photoButton setBackgroundImage:[self getPhotoButtonImage:photo] forState:UIControlStateNormal];
	[photoButton setImage:nil forState:UIControlStateNormal];
}

// UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	if (photo) {
		photo = nil;
	}
	UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
	if (image.size.width < image.size.height) {
		UIImageView *photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, image.size.width, image.size.width)];
		photoImageView.contentMode = UIViewContentModeCenter;
		photoImageView.image = image;
		UIGraphicsBeginImageContext(CGSizeMake(image.size.width, image.size.width));
		[photoImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		photo = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
	}
	else {
		UIImageView *photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, image.size.height, image.size.height)];
		photoImageView.contentMode = UIViewContentModeCenter;
		photoImageView.image = image;
		UIGraphicsBeginImageContext(CGSizeMake(image.size.height, image.size.height));
		[photoImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		photo = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
	}
	
	[photoButton setBackgroundImage:[self getPhotoButtonImage:photo] forState:UIControlStateNormal];
	[photoButton setImage:nil forState:UIControlStateNormal];
	
    [studentsViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [studentsViewController dismissViewControllerAnimated:YES completion:nil];
}

// AvatarsViewControllerDelegate
- (void)didSelectAvatar:(AvatarsViewController *)controller image:(UIImage *)image {
	if (photo) {
		photo = nil;
	}
	photo = image;
	[photoButton setBackgroundImage:[self getPhotoButtonImage:photo] forState:UIControlStateNormal];
	[photoButton setImage:nil forState:UIControlStateNormal];
	
    [studentsViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didCancelAvatar:(AvatarsViewController *)controller {
    [studentsViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onDeleteButton:(id)sender {
	[nameTextField resignFirstResponder];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(CONFIRM_STRING) message:getStringWithKey(DELETE_STUDENT_STRING) preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(NO_STRING) style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(YES_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self->student remove];

        [self->nameTextField resignFirstResponder];
        [self->studentsViewController reloadStudents];
    }]];
    [self presentViewController:alertController animated:true completion:nil];
}

- (IBAction)onCancelButton:(id)sender {
	[studentsViewController dismissANSPopover];
}

- (IBAction)onSaveButton:(id)sender {
	[nameTextField resignFirstResponder];
	
	NSString *errorMessage = nil;
	if ([[nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
		errorMessage = getStringWithKey(ENTER_NAME_ERROR_STRING);
	}
	else {
		if (![[student.name lowercaseString] isEqualToString:[nameTextField.text lowercaseString]]) {
			if ([StudentEntity getStudent:nameTextField.text]) {
				errorMessage = [NSString stringWithFormat:getStringWithKey(PLAYER_NAME_USE_STRING), nameTextField.text];
			}
		}
	}
	
	if (errorMessage == nil) {
		self.view.userInteractionEnabled = NO;
		[self saveButton];
	}
	else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];
	}
}

- (void)saveButton {
	if (photo == nil) {
		photo = [UIImage imageNamed:@"ANSEmpty.png"];
	}

	if (student == nil) {
		StudentEntity *newStudent = [[StudentEntity alloc] init];
		newStudent.name = nameTextField.text;
		newStudent.grade = @"";
		newStudent.dateOfbirth = [NSDate date];
		newStudent.photoName = [StudentEntity fileNameWithUIImage:photo];
        newStudent.username = @"";

		[StudentEntity insertNewStudent:newStudent];
	}
	else {
		if (student.photoName) {
			NSString *path = [[NSString headerFolderPath] stringByAppendingPathComponent:student.photoName];
			[fileManager removeItemAtPath:path error:nil];
		}

		student.name = nameTextField.text;
		student.photoName = [StudentEntity fileNameWithUIImage:photo];
		
		[StudentEntity updateAStudent:student];
	}
	
	[studentsViewController reloadStudents];
}

- (UIImage *)getPhotoButtonImage:(UIImage *)image {
	UIImageView *photoImageView;
	if (IS_IPAD)
		photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 131, 133)];
	else
		photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 65, 66)];
	photoImageView.backgroundColor = [UIColor whiteColor];
	photoImageView.contentMode = UIViewContentModeScaleAspectFill;
	photoImageView.image = image;
	
	UIGraphicsBeginImageContextWithOptions(photoImageView.frame.size, NO, 0);
	[photoImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return resultImage;
}

// UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	self.view.userInteractionEnabled = NO;
	[UIView animateWithDuration:0.2
					 animations:^(void) {
                         self->contentView.transform = CGAffineTransformMakeTranslation(0, -80);
					 }
					 completion:^(BOOL finished) {
						 self.view.userInteractionEnabled = YES;
					 }];
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	self.view.userInteractionEnabled = NO;
	[UIView animateWithDuration:0.2
					 animations:^(void) {
                         self->contentView.transform = CGAffineTransformIdentity;
					 }
					 completion:^(BOOL finished) {
						 self.view.userInteractionEnabled = YES;
					 }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

@end
