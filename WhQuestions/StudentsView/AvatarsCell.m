//
//  AvatarsCell.m
//  iPracticeVerbs
//
//  Created by Frank J. on 9/21/13.
//  Copyright (c) 2013 Smarty Ears. All rights reserved.
//

#import "AvatarsCell.h"

@implementation AvatarsCell

@synthesize button1;
@synthesize button2;
@synthesize button3;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.tag = 0;
        button2.tag = 1;
        button3.tag = 2;
		button1.frame = CGRectMake(5, 5, 100, 100);
		[self.contentView addSubview:button1];
		button2 = [UIButton buttonWithType:UIButtonTypeCustom];
		button2.frame = CGRectMake(110, 5, 100, 100);
		[self.contentView addSubview:button2];
		button3 = [UIButton buttonWithType:UIButtonTypeCustom];
		button3.frame = CGRectMake(215, 5, 100, 100);
        
		[self.contentView addSubview:button3];
	}
	return self;
}

@end
