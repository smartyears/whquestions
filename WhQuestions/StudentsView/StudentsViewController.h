//
//  StudentsViewController.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "CustomViewController.h"
#import "FXLabel.h"
#import "MBProgressHUD.h"
#import "PopoverController.h"
#import "RestrictedPopupViewController.h"

@interface StudentsViewController : CustomViewController <PopoverControllerDelegate, UIAlertViewDelegate> {
	IBOutlet FXLabel *titleLabel;
	IBOutlet FXLabel *descriptionLabel;
	IBOutlet UIButton *addPlayerButton;
	IBOutlet UIScrollView *contentScrollView;
	IBOutlet UIButton *playButton;
	
	NSMutableArray *studentsArray;
	NSMutableArray *tempStudentsArray;
	NSThread *loadPhotoThread;
	MBProgressHUD *hud;
	NSInteger currentIndex;
	
	NSMutableArray *selectedArray;
	NSMutableArray *photoViewsArray;
	NSMutableArray *nameLabelsArray;
	NSMutableArray *editButtonsArray;
	
	NSMutableArray *selectedStudentsArray;
	
	BOOL shouldPlayEffectAudio;
	
	PopoverController *ansPopoverController;
	PopoverController *selectActivityPopoverController;
	PopoverController *selectQuestionsPopoverController;
	
	RestrictedPopupViewController *importRestrictedPopupViewController;
	
	NSInteger selectedActivityType;
}

- (IBAction)onBackButton:(id)sender;
- (IBAction)onAddPlayerButton:(id)sender;
- (IBAction)onImportFromAcademy:(id)sender;
- (IBAction)onPlayButton:(id)sender;

- (void)reloadStudents;
- (void)dismissANSPopover;
- (void)dismissSelectQuestionsPopoverController;

- (void)onSpeakButton;
- (void)onSelectButton;

- (void)startGame;

@end
