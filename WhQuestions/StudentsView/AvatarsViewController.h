//
//  AvatarsViewController.h
//  WhQuestions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AvatarsViewControllerDelegate;

@interface AvatarsViewController : CustomViewController <UITableViewDelegate, UITableViewDataSource> {
	id<AvatarsViewControllerDelegate> __weak delegate;
	IBOutlet UITableView *contentTableView;

	NSArray *avatarsArray;

	NSString *resourcePath;
}

@property (nonatomic, weak) id<AvatarsViewControllerDelegate> delegate;

@end

@protocol AvatarsViewControllerDelegate <NSObject>

@optional
- (void)didSelectAvatar:(AvatarsViewController *)controller image:(UIImage *)image;
- (void)didCancelAvatar:(AvatarsViewController *)controller;

@end
