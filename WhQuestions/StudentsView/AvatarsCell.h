//
//  AvatarsCell.h
//  iPracticeVerbs
//
//  Created by Frank J. on 9/21/13.
//  Copyright (c) 2013 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvatarsCell : UITableViewCell {
	UIButton *button1;
	UIButton *button2;
	UIButton *button3;
}

@property (nonatomic) UIButton *button1;
@property (nonatomic) UIButton *button2;
@property (nonatomic) UIButton *button3;

@end
