//
//  AvatarsViewController.m
//  WhQuestions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "AvatarsViewController.h"
#import "AvatarsCell.h"

@interface AvatarsViewController ()

@end

@implementation AvatarsViewController

@synthesize delegate;

#pragma mark - View lifecycle

- (void)viewDidLoad {
	[super viewDidLoad];

	self.title = getStringWithKey(AVATARS_STRING);
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:getStringWithKey(CANCEL_STRING) style:UIBarButtonItemStylePlain target:self action:@selector(onCancelButton:)];

	self.preferredContentSize = CGSizeMake(320, 490);

	avatarsArray = [[NSArray alloc] initWithContentsOfFile:[NSBundle.mainBundle pathForResource:@"AvatarsArray" ofType:@"plist"]];

	resourcePath = NSBundle.mainBundle.resourcePath;
}

- (void)onCancelButton:(id)sender {
	if ([delegate respondsToSelector:@selector(didCancelAvatar:)])
		[delegate didCancelAvatar:self];
}

// UITableViewDelegate, UITableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return avatarsArray.count / 3 + (avatarsArray.count % 3 == 0 ? 0 : 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *identifier = @"AVATARS_CELL";
	AvatarsCell *cell = (AvatarsCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
	if (cell == nil) {
		cell = [[AvatarsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
		cell.backgroundColor = [UIColor clearColor];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		[cell.button1 addTarget:self action:@selector(onClickAvatar:) forControlEvents:UIControlEventTouchUpInside];
		[cell.button2 addTarget:self action:@selector(onClickAvatar:) forControlEvents:UIControlEventTouchUpInside];
		[cell.button3 addTarget:self action:@selector(onClickAvatar:) forControlEvents:UIControlEventTouchUpInside];
	}

	NSInteger row = indexPath.row;

	cell.button1.tag = row * 3;
	cell.button2.tag = row * 3 + 1;
	cell.button3.tag = row * 3 + 2;

	if (row < avatarsArray.count / 3) {
		[cell.button1 setImage:[UIImage imageWithContentsOfFile:[resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [avatarsArray objectAtIndex:row * 3]]]] forState:UIControlStateNormal];
		[cell.button2 setImage:[UIImage imageWithContentsOfFile:[resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [avatarsArray objectAtIndex:row * 3 + 1]]]] forState:UIControlStateNormal];
		[cell.button3 setImage:[UIImage imageWithContentsOfFile:[resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [avatarsArray objectAtIndex:row * 3 + 2]]]] forState:UIControlStateNormal];

		cell.button1.hidden = NO;
		cell.button2.hidden = NO;
		cell.button3.hidden = NO;
	}
	else
    {
		if (avatarsArray.count % 3 > 0) {
			[cell.button1 setImage:[UIImage imageWithContentsOfFile:[resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [avatarsArray objectAtIndex:row * 3]]]] forState:UIControlStateNormal];
			cell.button1.hidden = NO;
		}
		else{
			cell.button1.hidden = YES;
		}

		if (avatarsArray.count % 3 > 1) {
			[cell.button2 setImage:[UIImage imageWithContentsOfFile:[resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [avatarsArray objectAtIndex:row * 3 + 1]]]] forState:UIControlStateNormal];
			cell.button2.hidden = NO;
		}
		else{
			cell.button2.hidden = YES;
		}

		cell.button3.hidden = YES;
	}

	return cell;
}

- (void)onClickAvatar:(id)sender {
	if ([delegate respondsToSelector:@selector(didSelectAvatar:image:)]) {
		NSInteger index = ((UIButton *)sender).tag;
		UIImage *image = [UIImage imageWithContentsOfFile:[NSBundle.mainBundle pathForResource:[avatarsArray objectAtIndex:index] ofType:@"png"]];
		[delegate didSelectAvatar:self image:image];
	}
}

@end
