//
//  StudentsViewController.m
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "StudentsViewController.h"
#import "StudentEntity.h"
#import "ANSViewController.h"
#import "SelectActivityViewController.h"
#import "SelectQuestionsViewController.h"
#import "TestViewController.h"
#import "LoginViewController.h"

#define PHOTO_X_PAD			8
#define PHOTO_Y_PAD			8
#define PHOTO_WIDTH_PAD		143
#define PHOTO_HEIGHT_PAD	143

#define PHOTO_X_PHONE		4
#define PHOTO_Y_PHONE		4
#define PHOTO_WIDTH_PHONE	71.5
#define PHOTO_HEIGHT_PHONE	71.5

@interface StudentsViewController ()

@end

@implementation StudentsViewController

- (void)dealloc {
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (IS_IPAD)
		titleLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:40];
	else
		titleLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
	titleLabel.characterSpacing = -0.1;
	titleLabel.oversampling = 4;
	titleLabel.text = getStringWithKey(SELECT_PLAYER_STRING);
	
	if (IS_IPAD)
		descriptionLabel.font = [UIFont fontWithName:ARIAL_FONT size:26];
	else
		descriptionLabel.font = [UIFont fontWithName:ARIAL_FONT size:17];
	descriptionLabel.characterSpacing = -0.1;
	descriptionLabel.oversampling = 4;
	descriptionLabel.text = getStringWithKey(SELECT_UP_TO_6_PLAYERS_STRING);
	
	{
		UIImageView *imageView;
		if (IS_IPAD)
			imageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 159, 159)];
		else
			imageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 80, 80)];
		imageView.image = [UIImage imageNamed:@"StudentsAddPlayerButtonPad.png"];
		
		FXLabel *label;
		if (IS_IPAD) {
			label = [FXLabel.alloc initWithFrame:CGRectMake(18, 105, 129, 54)];
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:32];
		}
		else {
			label = [FXLabel.alloc initWithFrame:CGRectMake(9, 53, 65, 27)];
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		}
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor whiteColor];
		label.characterSpacing = -0.1;
        label.minimumScaleFactor = 0.1;
		label.adjustsFontSizeToFitWidth = YES;
		label.textAlignment = NSTextAlignmentCenter;
		label.oversampling = 4;
		label.text = getStringWithKey(ADD_PLAYER_STRING);
		[imageView addSubview:label];
		
		UIGraphicsBeginImageContextWithOptions(imageView.frame.size, NO, 0);
		[imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[addPlayerButton setBackgroundImage:image forState:UIControlStateNormal];
		
		if (IS_IPAD)
			addPlayerButton.frame = CGRectMake(63, 0, 159, 159);
		else
			addPlayerButton.frame = CGRectMake(14, 0, 80, 80);
	}
	
	selectedArray = [[NSMutableArray alloc] init];
	photoViewsArray = [[NSMutableArray alloc] init];
	nameLabelsArray = [[NSMutableArray alloc] init];
	editButtonsArray = [[NSMutableArray alloc] init];
	
	hud = [[MBProgressHUD alloc] initWithView:self.view];
	
	shouldPlayEffectAudio = YES;

	[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(reloadStudents) name:RELOAD_STUDENTS object:nil];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(loginDone) name:KNotification_loginDone object:nil];
    
    contentScrollView.contentInset = UIEdgeInsetsMake(0, 0, 60.f, 0);

	[self reloadStudents];
}

- (IBAction)onBackButton:(id)sender {
	[APP_DELEGATE stopEffectAudio];

	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onPlayButton:(id)sender {
	if (selectedArray.count < 1 || selectedArray.count > 6) {
		UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(SELECT_STUDENTS_ERROR_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];
		
		return;
	}
	
	selectedStudentsArray = nil;
	selectedStudentsArray = [[NSMutableArray alloc] init];
	for (NSNumber *number in selectedArray) {
		int index = [number intValue];
		[selectedStudentsArray addObject:[studentsArray objectAtIndex:index]];
	}
	
	SelectActivityViewController *selectActivityViewController;
	if (IS_IPAD)
		selectActivityViewController = [[SelectActivityViewController alloc] initWithNibName:@"SelectActivityViewControllerPad" bundle:nil];
	else
		selectActivityViewController = [[SelectActivityViewController alloc] initWithNibName:@"SelectActivityViewControllerPhone" bundle:nil];
	selectActivityViewController.studentsViewController = self;
	selectActivityPopoverController = [[PopoverController alloc] initWithContentViewController:selectActivityViewController];
	selectActivityPopoverController.delegate = self;
	selectActivityPopoverController.showDarkBackground = YES;
	
	if (IS_IPAD)
		[selectActivityPopoverController presentPopoverFromPoint:CGPointMake((screenWidth - 802) / 2, 100) inView:self.view];
	else
		[selectActivityPopoverController presentPopoverFromPoint:CGPointMake((screenWidth - 402) / 2, 28) inView:self.view];
}

- (IBAction)onAddPlayerButton:(id)sender {
	ANSViewController *ansViewController;
	if (IS_IPAD)
		ansViewController = [[ANSViewController alloc] initWithNibName:@"ANSViewControllerPad" bundle:nil];
	else
		ansViewController = [[ANSViewController alloc] initWithNibName:@"ANSViewControllerPhone" bundle:nil];
	ansViewController.studentsViewController = self;
	ansPopoverController = [[PopoverController alloc] initWithContentViewController:ansViewController];
	ansPopoverController.delegate = self;
	ansPopoverController.showDarkBackground = YES;
	
	[ansPopoverController presentPopoverFromPoint:CGPointMake(0, 0) inView:self.view];
}

- (IBAction)onImportFromAcademy:(id)sender {
    LoginViewController *loginViewController;
    if (IS_IPAD) {
        loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewControllerPad" bundle:nil];
    }
    else {
        loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewControllerPhone" bundle:nil];
    }
    loginViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:loginViewController animated:NO completion:nil];
}

- (void)loginDone {
    [self reloadStudents];
}

- (void)reloadStudents {
	if (tempStudentsArray)
		return;
	
	[ansPopoverController dismissPopover];
	
	contentScrollView.hidden = YES;
	
	for (UIImageView *photoView in photoViewsArray)
		[photoView removeFromSuperview];
	[photoViewsArray removeAllObjects];
	
	for (FXLabel *nameLabel in nameLabelsArray)
		[nameLabel removeFromSuperview];
	[nameLabelsArray removeAllObjects];
	
	for (UIButton *editButton in editButtonsArray)
		[editButton removeFromSuperview];
	[editButtonsArray removeAllObjects];
	
	[selectedArray removeAllObjects];
	playButton.hidden = YES;
	
	studentsArray = nil;
	
	studentsArray = [[NSMutableArray alloc] initWithArray:[StudentEntity allStudents]];
	tempStudentsArray = [[NSMutableArray alloc] initWithArray:studentsArray];
	
	[self.view addSubview:hud];
	[hud showAnimated:YES];
	
	currentIndex = -1;
	[self loadPhoto];
}

- (void)loadPhoto {
	currentIndex ++;
	
	if (studentsArray.count == 0) {
		NSInteger rowCount;
		if (IS_IPAD)
        {
			rowCount = (currentIndex + 1) / 5 + ((currentIndex + 1) % 5 == 0 ? 0 : 1);
			contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width, 216 * rowCount);
		}
		else
        {
			if (screenWidth == 568)
				rowCount = (currentIndex + 1) / 6 + ((currentIndex + 1) % 6 == 0 ? 0 : 1);
			else
				rowCount = (currentIndex + 1) / 5 + ((currentIndex + 1) % 5 == 0 ? 0 : 1);
			contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width, 108 * rowCount);
		}
		contentScrollView.hidden = NO;
		
		[hud hideAnimated:YES];
		[hud removeFromSuperview];
		
		[studentsArray addObjectsFromArray:tempStudentsArray];
		tempStudentsArray = nil;
		
		for (NSInteger i = 0; i < selectedArray.count; i ++) {
			NSInteger index = [[selectedArray objectAtIndex:i] intValue];
			UIImageView *photoView = [photoViewsArray objectAtIndex:index];
			photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundSelectedPad.png"];
			FXLabel *nameLabel = [nameLabelsArray objectAtIndex:index];
			nameLabel.textColor = [UIColor colorWithRed:208 / 255.0 green:91 / 255.0 blue:39 / 255.0 alpha:1.0];
			nameLabel.strokeColor = [UIColor colorWithRed:117 / 255.0 green:47 / 255.0 blue:47 / 255.0 alpha:1.0];
		}
		
		if (shouldPlayEffectAudio) {
			shouldPlayEffectAudio = NO;
			
			if (studentsArray.count == 0) {
				[APP_DELEGATE playEffectAudio:@"addplayer"];
			}
			else {
				[APP_DELEGATE playEffectAudio:@"selectplayer"];
			}
		}
	}
	else {
		loadPhotoThread = [[NSThread alloc] initWithTarget:self selector:@selector(loadPhotoThreadFunc) object:nil];
		[loadPhotoThread start];
	}
}

- (void)loadPhotoThreadFunc {
	@autoreleasepool {
		StudentEntity *student = [studentsArray objectAtIndex:0];
		
		[self performSelectorOnMainThread:@selector(addStudent:) withObject:[student headerImage] waitUntilDone:YES];
		
		[studentsArray removeObjectAtIndex:0];
	}
	
	loadPhotoThread = nil;
	
	[self performSelectorOnMainThread:@selector(loadPhoto) withObject:nil waitUntilDone:NO];
}

- (void)addStudent:(UIImage *)photo {
	NSInteger column;
	NSInteger row;
	if (IS_IPAD) {
		column = (currentIndex + 1) % 5;
		row = (currentIndex + 1) / 5;
	}
	else {
		if (screenWidth == 568) {
			column = (currentIndex + 1) % 6;
			row = (currentIndex + 1) / 6;
		}
		else {
			column = (currentIndex + 1) % 5;
			row = (currentIndex + 1) / 5;
		}
	}
	
	UIImageView *photoView;
	if (IS_IPAD) {
		photoView = [UIImageView.alloc initWithFrame:CGRectMake(63 + 185 * column, 216 * row, 159, 159)];
	}
	else {
		if (screenWidth == 568) {
			photoView = [UIImageView.alloc initWithFrame:CGRectMake(12 + 93 * column, 108 * row, 80, 80)];
		}
		else {
			photoView = [UIImageView.alloc initWithFrame:CGRectMake(14 + 93 * column, 108 * row, 80, 80)];
		}
	}
	photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundPad.png"];
	photoView.tag = currentIndex;
	photoView.userInteractionEnabled = YES;

	UIImageView *photoImageView;
	if (IS_IPAD)
		photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PAD, PHOTO_Y_PAD, PHOTO_WIDTH_PAD, PHOTO_HEIGHT_PAD)];
	else
		photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PHONE, PHOTO_Y_PHONE, PHOTO_WIDTH_PHONE, PHOTO_HEIGHT_PHONE)];
	photoImageView.contentMode = UIViewContentModeScaleAspectFill;
	if (IS_IPAD)
		photoImageView.layer.cornerRadius = 17;
	else
		photoImageView.layer.cornerRadius = 8.5;
	photoImageView.clipsToBounds = YES;
	photoImageView.image = photo;
	photoImageView.transform = CGAffineTransformMakeRotation(-1.6 * M_PI / 180.0);
	[photoView addSubview:photoImageView];

	UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTapGesture:)];
	doubleTapGesture.numberOfTapsRequired = 2;
	[photoView addGestureRecognizer:doubleTapGesture];
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
	tapGesture.numberOfTapsRequired = 1;
	[tapGesture requireGestureRecognizerToFail:doubleTapGesture];
	[photoView addGestureRecognizer:tapGesture];
	
	[photoViewsArray addObject:photoView];
	
	FXLabel *nameLabel;
	if (IS_IPAD) {
		nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 42)];
		nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:34];
	}
	else {
		nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 21)];
		nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:17];
	}
	nameLabel.backgroundColor = [UIColor clearColor];
	nameLabel.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
	nameLabel.strokeColor = [UIColor whiteColor];
	if (IS_IPAD)
		nameLabel.strokeSize = 4;
	else
		nameLabel.strokeSize = 2;
    nameLabel.minimumScaleFactor = 0.1;
	nameLabel.adjustsFontSizeToFitWidth = YES;
	nameLabel.textAlignment = NSTextAlignmentCenter;
	nameLabel.oversampling = 4;
	nameLabel.text = [[studentsArray objectAtIndex:0] name];
	
	[nameLabelsArray addObject:nameLabel];
	
	UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
	editButton.tag = photoView.tag;
	if (IS_IPAD) {
		editButton.frame = CGRectMake(CGRectGetMaxX(photoView.frame) - 42, CGRectGetMinY(photoView.frame), 42, 42);
	}
	else {
		editButton.frame = CGRectMake(CGRectGetMaxX(photoView.frame) - 21, CGRectGetMinY(photoView.frame), 21, 21);
	}
	[editButton setBackgroundImage:[UIImage imageNamed:@"StudentsEditStudentButtonPad.png"] forState:UIControlStateNormal];
	[editButton addTarget:self action:@selector(onEditButton:) forControlEvents:UIControlEventTouchUpInside];
	[editButtonsArray addObject:editButton];
	
	[contentScrollView addSubview:photoView];
	[contentScrollView addSubview:nameLabel];
	[contentScrollView addSubview:editButton];
}

- (void)onTapGesture:(UITapGestureRecognizer *)gesture {
	NSInteger index = gesture.view.tag;
	
	BOOL bExist = NO;
	for (NSString *number in selectedArray) {
		if ([number intValue] == index) {
			bExist = YES;
			break;
		}
	}
	
	if (bExist) {
		[selectedArray removeObject:[NSNumber numberWithInteger:index]];
		UIImageView *photoView = [photoViewsArray objectAtIndex:index];
		photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundPad.png"];
		FXLabel *nameLabel = [nameLabelsArray objectAtIndex:index];
		nameLabel.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		nameLabel.strokeColor = [UIColor whiteColor];
	}
	else {
		if (selectedArray.count < 6) {
			[selectedArray addObject:[NSNumber numberWithInteger:index]];
		}
	}
	
	for (NSInteger i = 0; i < selectedArray.count; i ++) {
		index = [[selectedArray objectAtIndex:i] intValue];
		UIImageView *photoView = [photoViewsArray objectAtIndex:index];
		photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundSelectedPad.png"];
		FXLabel *nameLabel = [nameLabelsArray objectAtIndex:index];
		nameLabel.textColor = [UIColor colorWithRed:208 / 255.0 green:91 / 255.0 blue:39 / 255.0 alpha:1.0];
		nameLabel.strokeColor = [UIColor colorWithRed:117 / 255.0 green:47 / 255.0 blue:47 / 255.0 alpha:1.0];
	}
	
	if (selectedArray.count > 0)
		playButton.hidden = NO;
	else
		playButton.hidden = YES;
}

- (void)onDoubleTapGesture:(UITapGestureRecognizer *)gesture {
	NSInteger index = gesture.view.tag;
	
	[self editStudent:index];
}

- (void)onEditButton:(UIButton *)editButton {
	NSInteger index = editButton.tag;
	
	[self editStudent:index];
}

- (void)editStudent:(NSInteger)index {
	StudentEntity *student = [studentsArray objectAtIndex:index];
	
	ANSViewController *ansViewController;
	if (IS_IPAD)
		ansViewController = [[ANSViewController alloc] initWithNibName:@"ANSViewControllerPad" bundle:nil];
	else
		ansViewController = [[ANSViewController alloc] initWithNibName:@"ANSViewControllerPhone" bundle:nil];
	ansViewController.studentsViewController = self;
	ansViewController.student = student;
	ansPopoverController = [[PopoverController alloc] initWithContentViewController:ansViewController];
	ansPopoverController.delegate = self;
	ansPopoverController.showDarkBackground = YES;
	
	[ansPopoverController presentPopoverFromPoint:CGPointMake(0, 0) inView:self.view];
}

#pragma mark - PopoverControllerDelegate
- (void)popoverControllerDidDismissed:(PopoverController *)popoverController {
	if (popoverController == ansPopoverController) {
		ansPopoverController = nil;
	} else if (popoverController == selectActivityPopoverController) {
		selectActivityPopoverController = nil;
	} else if (popoverController == selectQuestionsPopoverController) {
		selectQuestionsPopoverController = nil;
	}
}

- (void)dismissANSPopover {
	[ansPopoverController dismissPopover];
}

- (void)onSpeakButton {
	[selectActivityPopoverController dismissPopover];
	selectedActivityType = ACTIVITY_SPEAK;
	[self showSelectQuestionsPopover];
}

- (void)onSelectButton {
	[selectActivityPopoverController dismissPopover];
	selectedActivityType = ACTIVITY_SELECT;
	[self showSelectQuestionsPopover];
}

- (void)showSelectQuestionsPopover {
	SelectQuestionsViewController *vc = [SelectQuestionsViewController.alloc initWithNibName:IS_IPAD ? @"SelectQuestionsViewControllerPad" : @"SelectQuestionsViewControllerPhone" bundle:nil];
	vc.studentsViewController = self;
	vc.studentsArray = selectedStudentsArray;
	vc.activityType = selectedActivityType;
	selectQuestionsPopoverController = [PopoverController.alloc initWithContentViewController:vc];
	selectQuestionsPopoverController.delegate = self;
	selectQuestionsPopoverController.showDarkBackground = YES;
	
    [selectQuestionsPopoverController presentPopoverFromPoint:IS_IPAD ? CGPointMake((screenWidth - 802) / 2, 100) : CGPointMake((screenWidth - 402) / 2, 28) inView:self.view];
}

- (void)dismissSelectQuestionsPopoverController {
    [selectQuestionsPopoverController dismissPopover];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self onPlayButton:nil];
    });
}

- (void)startGame {
	[APP_DELEGATE stopEffectAudio];

	[selectQuestionsPopoverController dismissPopover];

    TestViewController *testViewController = [[TestViewController alloc] initWithNibName:IS_IPAD ? @"TestViewControllerPad" : @"TestViewControllerPhone" bundle:nil];
	testViewController.studentsArray = selectedStudentsArray;
	testViewController.activityType = selectedActivityType;
	[self.navigationController pushViewController:testViewController animated:YES];
}

@end
