//
//  AppDelegate.m
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "AppDelegate.h"
#import "FMDatabase+SharedInstance.h"
#import "NSData+CocoaDevUsersAdditions.h"
#import "StudentEntity.h"
#import "MainViewController.h"
#import "CustomPDFRenderer.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "WhQuestionsSetting.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <WebKit/WebKit.h>

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate() <UIApplicationDelegate, WKNavigationDelegate, AVAudioPlayerDelegate>
{
    CustomNavigationController *navigationController;
    WKWebView *printWebView;
    MBProgressHUD *hud;
    NSMutableArray *importArray;
    AVAudioPlayer *backgroundAudioPlayer, *effectAudioPlayer;
}
@property (nonatomic, readonly) MainViewController *mainViewController;
@end


@implementation AppDelegate

@synthesize backgroundAudioMute = _backgroundAudioMute;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
	application.statusBarHidden = YES;
	
    [AVAudioSession.sharedInstance setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth error:nil];
    [AVAudioSession.sharedInstance setActive:YES error:nil];
	exportFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/export.trc"];
	tempImportDirectoryPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/TempImport"];
	tempExportDirectoryPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/TempExport"];
	syncFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/export.trcsync"];
	reportPDFPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/WhQuestions-Report.pdf"];
	defaults = [NSUserDefaults standardUserDefaults];
	fileManager = [[NSFileManager alloc] init];
	screenWidth = UIScreen.mainScreen.bounds.size.width;
	screenHeight = UIScreen.mainScreen.bounds.size.height;
	if (screenWidth < screenHeight) {
		CGFloat temp = screenWidth;
		screenWidth = screenHeight;
		screenHeight = temp;
	}
    screenWidthRatio = screenWidth / (IS_IPAD ? 1024 : 480);
    screenHeightRatio = screenHeight / (IS_IPAD ? 768 : 320);
	languageStringsDictionary = [[NSDictionary alloc] initWithContentsOfFile:[NSBundle.mainBundle pathForResource:@"Strings" ofType:@"plist"]];
	
	_backgroundAudioMute = [defaults boolForKey:@"BACKGROUND_AUDIO_MUTE"];
	
	backgroundAudioPlayer = [AVAudioPlayer.alloc initWithContentsOfURL:[NSURL fileURLWithPath:[NSBundle.mainBundle pathForResource:@"intro" ofType:@"m4a"]] error:nil];
	backgroundAudioPlayer.numberOfLoops = -1;

    [FMDatabase sharedFMDatabase];

    MainViewController *vc = [MainViewController.alloc initWithNibName:IS_IPAD ? @"MainViewControllerPad" : @"MainViewControllerPhone" bundle:nil];
	navigationController = [CustomNavigationController.alloc initWithRootViewController:vc];
	navigationController.navigationBarHidden = YES;
	
    self.window = [UIWindow.alloc initWithFrame:UIScreen.mainScreen.bounds];
    self.window.backgroundColor = UIColor.blackColor;
	self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

#pragma mark - Deep Link Handle

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    if (!url.isFileURL && [url.path.pathExtension.lowercaseString isEqualToString:@"trcx"])
        [self importFile:url];
    return YES;
}

- (void)importFile:(NSURL*)url {
    if (!url.isFileURL)
        return;

    hud = [MBProgressHUD.alloc initWithView:self.window];
    hud.label.text = getStringWithKey(IMPORTING_STRING);
    hud.removeFromSuperViewOnHide = YES;
    [self.window addSubview:hud];
    [hud showAnimated:YES];

    NSData *zippedData = [NSData dataWithContentsOfFile:url.path];
    NSData *unzippedData = zippedData.gzipInflate;

    NSFileWrapper *dirWrapper = [NSFileWrapper.alloc initWithSerializedRepresentation:unzippedData];
    if (dirWrapper == nil) {
        [hud hideAnimated:YES];
        hud = nil;
        return;
    }

    [fileManager removeItemAtPath:tempImportDirectoryPath error:nil];
    [fileManager createDirectoryAtPath:tempImportDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil];
    [dirWrapper writeToURL:[NSURL fileURLWithPath:tempImportDirectoryPath] options:NSFileWrapperWritingAtomic originalContentsURL:nil error:nil];
    importArray = [NSMutableArray arrayWithContentsOfFile:[tempImportDirectoryPath stringByAppendingPathComponent:@"students.plist"]];

    [self importItem];
}

- (void)importItem {
    if (importArray.count == 0) {
        importArray = nil;
        [hud hideAnimated:YES];
        hud = nil;
        [NSNotificationCenter.defaultCenter postNotificationName:RELOAD_STUDENTS object:nil];
        return;
    }

    NSDictionary *studentDictionary = importArray[0];
    NSString *name = studentDictionary[@"name"];
    StudentEntity *student = [StudentEntity getStudent:name];
    if (student) {

        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(CONFIRM_STRING) message:getStringWithKey(REPLACE_PLAYER_CONFIRM_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(IGNORE_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(REPLACE_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSDictionary *studentDictionary = [self->importArray objectAtIndex:0];
            NSString *name = [studentDictionary objectForKey:@"name"];
            StudentEntity *student = [StudentEntity getStudent:name];
            
            student.name = name;
            student.photoName = [StudentEntity fileNameWithUIImage:[UIImage imageWithContentsOfFile:[tempImportDirectoryPath stringByAppendingPathComponent:[studentDictionary objectForKey:@"photoPath"]]]];
            
            [StudentEntity updateAStudent:student];
        }]];
        [APP_DELEGATE.window.rootViewController presentViewController:alertController animated:true completion:nil];
    } else {
        StudentEntity *student = StudentEntity.new;
        student.name = name;
        student.grade = @"";
        student.dateOfbirth = NSDate.date;
        student.photoName = [StudentEntity fileNameWithUIImage:[UIImage imageWithContentsOfFile:[tempImportDirectoryPath stringByAppendingPathComponent:[studentDictionary objectForKey:@"photoPath"]]]];
        student.username = @"";
        [StudentEntity insertNewStudent:student];
        [importArray removeObjectAtIndex:0];
        [self importItem];
    }
}

#pragma mark - PDF

- (void)generatePDF:(NSString *)htmlString {
	if (printWebView) {
		[printWebView removeFromSuperview];
		printWebView = nil;
	}
	
	[fileManager removeItemAtPath:reportPDFPath error:nil];
	
	printWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, REPORT_WIDTH - REPORT_MARGIN * 2, REPORT_HEIGHT - REPORT_MARGIN * 2)];
	printWebView.navigationDelegate = self;
	[printWebView loadHTMLString:htmlString baseURL:nil];
}

#pragma mark - WKWebViewDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    CustomPDFRenderer *pdfRenderer = CustomPDFRenderer.new;
    [pdfRenderer addPrintFormatter:[printWebView viewPrintFormatter] startingAtPageAtIndex:0];
    [pdfRenderer printToPDF:reportPDFPath];
    
    [printWebView removeFromSuperview];
    printWebView = nil;
    
    [NSNotificationCenter.defaultCenter postNotificationName:REPORT_GENERATED object:nil];
}

#pragma mark - BackgroundMusic
- (void)playBackgroundMusic {
    if (!backgroundAudioPlayer.playing) {
        backgroundAudioPlayer.volume = _backgroundAudioMute ? 0.0 : 0.2;
        [backgroundAudioPlayer play];
    }
}

- (void)stopBackgroundMusic {
	[backgroundAudioPlayer pause];
}

- (void)setBackgroundAudioMute:(BOOL)backgroundAudioMute {
	_backgroundAudioMute = backgroundAudioMute;
    backgroundAudioPlayer.volume = _backgroundAudioMute ? 0.0 : 0.2;
	[defaults setBool:_backgroundAudioMute forKey:@"BACKGROUND_AUDIO_MUTE"];
	[defaults synchronize];
}

- (void)playEffectAudio:(NSString *)filename {
	if (![WhQuestionsSetting audioInstructions])
		return;

	[effectAudioPlayer stop];
	effectAudioPlayer = nil;
	
	NSString *languageKey;
	NSInteger language = [defaults integerForKey:@"LANGUAGE"];
	if (language == LANGUAGE_ENGLISH)
		languageKey = @"EN";
	else if (language == LANGUAGE_PORTUGUESE)
		languageKey = @"PT";
	else if (language == LANGUAGE_SPANISH)
		languageKey = @"SP";
	else
		languageKey = @"FR";
	
	NSString *audioPath = [NSBundle.mainBundle pathForResource:[filename stringByAppendingString:languageKey] ofType:@"mp3"];
	if ([fileManager fileExistsAtPath:audioPath]) {
		effectAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:audioPath] error:nil];
		effectAudioPlayer.delegate = self;
		[effectAudioPlayer play];
	}
}

- (void)stopEffectAudio {
	[effectAudioPlayer stop];
	effectAudioPlayer = nil;
}

#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	[self processAudioPlayerFinishedPlaying:player];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
	[self processAudioPlayerFinishedPlaying:player];
}

- (void)processAudioPlayerFinishedPlaying:(AVAudioPlayer *)player {
	if (player == effectAudioPlayer) {
		effectAudioPlayer = nil;
	}
}

@end
