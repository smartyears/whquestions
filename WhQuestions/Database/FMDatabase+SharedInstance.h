//
//  FMDatabase+SharedInstance.h
//  UTTServices
//
//  Created by Frank J. on 6/27/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

#define CURRENT_SCHEMA_VERSION		8

@interface FMDatabase(SharedInstance)
+ (FMDatabase*)sharedFMDatabase;
+ (void)closeDatabaseIfOpened;
+ (void)removeDatabase;
@end
