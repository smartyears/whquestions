//
//  SessionEntity.m
//  WhQuesions
//
//  Created by Frank J. on 10/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "SessionEntity.h"
#import "QuestionEntity.h"
#import "FMDatabase.h"
#import "FMDatabase+SharedInstance.h"
#import "StudentEntity.h"
#import "RecordingDAO.h"

static NSInteger maxSessionID = 0;
static BOOL hasLoadFromDB = NO;

@implementation SessionEntity

@synthesize identifier;
@synthesize studentIdentifier;
@synthesize date;

@synthesize howCorrect;
@synthesize howWrong;

@synthesize whatCorrect;
@synthesize whatWrong;

@synthesize whereCorrect;
@synthesize whereWrong;

@synthesize whenCorrect;
@synthesize whenWrong;

@synthesize whyCorrect;
@synthesize whyWrong;

@synthesize whoCorrect;
@synthesize whoWrong;

@synthesize activityType;

- (id)init {
	self = [super init];
	if (self) {
		self.identifier = [SessionEntity occupyASessionID];
		self.date = [NSDate date];
	}
	return self;
}

- (NSString *)displayedDate {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MM/dd"];
	
	NSString *stringFromDate = [formatter stringFromDate:self.date];
	return stringFromDate;
}

- (void)markCorrectForQuestion:(QuestionEntity *)aQuestion {
	switch ([aQuestion type]) {
		case QuestionButton_Why:
			self.whyCorrect += 1;
			break;
		case QuestionButton_How:
			self.howCorrect += 1;
			break;
		case QuestionButton_Where:
			self.whereCorrect += 1;
			break;
		case QuestionButton_Who:
			self.whoCorrect += 1;
			break;
		case QuestionButton_When:
			self.whenCorrect += 1;
			break;
		case QuestionButton_What:
			self.whatCorrect += 1;
			break;
		default:
			break;
	}
}

- (void)markWrongForQuestion:(QuestionEntity *)aQuestion {
	switch ([aQuestion type]) {
		case QuestionButton_Why:
			self.whyWrong += 1;
			break;
		case QuestionButton_How:
			self.howWrong += 1;
			break;
		case QuestionButton_Where:
			self.whereWrong += 1;
			break;
		case QuestionButton_Who:
			self.whoWrong += 1;
			break;
		case QuestionButton_When:
			self.whenWrong += 1;
			break;
		case QuestionButton_What:
			self.whatWrong += 1;
			break;
		default:
			break;
	}
}

- (BOOL)isEmpty {
	return (self.whyCorrect + self.whyWrong + self.howCorrect + self.howWrong + self.whereCorrect + self.whereWrong + self.whoCorrect + self.whoWrong + self.whenCorrect + self.whenWrong + self.whatCorrect + self.whatWrong == 0);
}

- (BOOL)save {	
	NSString *insertStatement = @"INSERT INTO session (identifier, studentidentifier, date, howcorrect, howwrong, whatcorrect, whatwrong, wherecorrect, wherewrong, whencorrect, whenwrong, whycorrect, whywrong, whocorrect, whowrong, activityType) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	return [[FMDatabase sharedFMDatabase] executeUpdate:insertStatement,
			[NSNumber numberWithInteger:self.identifier],
			[NSNumber numberWithInteger:self.studentIdentifier],
			self.date,
			
			[NSNumber numberWithInteger:self.howCorrect],
			[NSNumber numberWithInteger:self.howWrong],
			
			[NSNumber numberWithInteger:self.whatCorrect],
			[NSNumber numberWithInteger:self.whatWrong],
			
			[NSNumber numberWithInteger:self.whereCorrect],
			[NSNumber numberWithInteger:self.whereWrong],
			
			[NSNumber numberWithInteger:self.whenCorrect],
			[NSNumber numberWithInteger:self.whenWrong],
			
			[NSNumber numberWithInteger:self.whyCorrect],
			[NSNumber numberWithInteger:self.whyWrong],
			
			[NSNumber numberWithInteger:self.whoCorrect],
			[NSNumber numberWithInteger:self.whoWrong],
			
			[NSNumber numberWithInteger:self.activityType],
			nil];
}
- (void)remove {
	RecordingDAO* recordingDAO = [RecordingDAO recordingDAO];
	[recordingDAO deleteAllRecordingsForSSID:self.identifier];
	
	[SessionEntity removeSession:self];
	
}

+ (void)removeSession:(SessionEntity *)session {
	NSString *deleteStatement = @"DELETE FROM session WHERE identifier = ?;";
	[[FMDatabase sharedFMDatabase] executeUpdate:deleteStatement,
			[NSNumber numberWithInteger:session.identifier],
			nil];
}

+ (NSArray *)sessionsByStudent:(StudentEntity *)aStudent {
	NSString * query = @"SELECT * FROM session WHERE studentidentifier = ? ORDER BY date DESC";
	
	FMResultSet* rs = [[FMDatabase sharedFMDatabase] executeQuery:query, [NSNumber numberWithInteger:aStudent.identifier]];
							 
	NSMutableArray *array = [NSMutableArray array];
	while ([rs next]) {
		SessionEntity *aSession = [[SessionEntity alloc] init];
		aSession.identifier = [rs intForColumn:@"identifier"];
		aSession.studentIdentifier = [rs intForColumn:@"studentidentifier"];
		aSession.date = [rs dateForColumn:@"date"];
		aSession.howCorrect = [rs intForColumn:@"howcorrect"];
		aSession.howWrong = [rs intForColumn:@"howwrong"];
		aSession.whatCorrect = [rs intForColumn:@"whatcorrect"];
		aSession.whatWrong = [rs intForColumn:@"whatwrong"];
		aSession.whereCorrect = [rs intForColumn:@"wherecorrect"];
		aSession.whereWrong = [rs intForColumn:@"wherewrong"];
		aSession.whenCorrect = [rs intForColumn:@"whencorrect"];
		aSession.whenWrong = [rs intForColumn:@"whenwrong"];
		aSession.whyCorrect = [rs intForColumn:@"whycorrect"];
		aSession.whyWrong = [rs intForColumn:@"whywrong"];
		aSession.whoCorrect = [rs intForColumn:@"whocorrect"];
		aSession.whoWrong = [rs intForColumn:@"whowrong"];
		aSession.activityType = [rs intForColumn:@"activityType"];
		[array addObject:aSession];
	}
	
	return array;
}

+ (NSInteger)occupyASessionID {
	if (!hasLoadFromDB) {
		hasLoadFromDB = YES;
		
		NSString *query = @"SELECT MAX(identifier) AS maxSessionID FROM session";
		FMDatabase *db = [FMDatabase sharedFMDatabase];
		FMResultSet* rs = [db executeQuery:query];
		
		while ([rs next] ) {
			
			maxSessionID = [rs  intForColumn:@"maxSessionID"];
			break;
		}
		
		[rs close];
		if ([db hadError]) {
			NSLog(@"getMaxRecordingID Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
		}
		maxSessionID += 1;
	}
	return maxSessionID++;
}

- (NSString *)getTRCDescription {
	NSInteger correctCount = howCorrect + whatCorrect + whereCorrect + whenCorrect + whyCorrect + whoCorrect;
	NSInteger wrongCount = howWrong + whatWrong + whereWrong + whenWrong + whyWrong + whoWrong;
	NSInteger totalCount = correctCount + wrongCount;
	NSInteger accuracy;
	if (totalCount == 0)
		accuracy = -1;
	else
		accuracy = correctCount * 100 / totalCount;
	
	NSString *activityString;
	if (activityType == ACTIVITY_SELECT)
		activityString = [getStringWithKey(SELECT_MY_ANSWERS_STRING) stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
	else
		activityString = [getStringWithKey(SPEAK_MY_ANSWERS_STRING) stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
	
	NSString *description = [NSString stringWithFormat:@"%@ %ld%%", getStringWithKey(ACCURACY_STRING), (long)accuracy];
	description = [description stringByAppendingFormat:@"\n%@ %@", getStringWithKey(ACTIVITY_STRING), activityString];
	description = [description stringByAppendingFormat:@"\n%@", getStringWithKey(RESULTS_TARGET_STRING)];
	
	NSArray *titlesArray = [NSArray arrayWithObjects:
							getStringWithKey(WHY_STRING),
							getStringWithKey(HOW_STRING),
							getStringWithKey(WHERE_STRING),
							getStringWithKey(WHO_STRING),
							getStringWithKey(WHEN_STRING),
							getStringWithKey(WHAT_STRING),
							nil];
	
	NSArray *correctsArray = [NSArray arrayWithObjects:
							  [NSNumber numberWithInteger:whyCorrect],
							  [NSNumber numberWithInteger:howCorrect],
							  [NSNumber numberWithInteger:whereCorrect],
							  [NSNumber numberWithInteger:whoCorrect],
							  [NSNumber numberWithInteger:whenCorrect],
							  [NSNumber numberWithInteger:whatCorrect],
							  nil];
	
	NSArray *wrongsArray = [NSArray arrayWithObjects:
							[NSNumber numberWithInteger:whyWrong],
							[NSNumber numberWithInteger:howWrong],
							[NSNumber numberWithInteger:whereWrong],
							[NSNumber numberWithInteger:whoWrong],
							[NSNumber numberWithInteger:whenWrong],
							[NSNumber numberWithInteger:whatWrong],
							nil];
	
	for (NSInteger i = 0; i < titlesArray.count; i ++) {
		NSString *title = [titlesArray objectAtIndex:i];
		correctCount = [[correctsArray objectAtIndex:i] integerValue];
		wrongCount = [[wrongsArray objectAtIndex:i] integerValue];
		totalCount = correctCount + wrongCount;
		if (totalCount == 0)
			accuracy = -1;
		else
			accuracy = correctCount * 100 / totalCount;
		
		if (accuracy < 0)
			description = [description stringByAppendingFormat:@"\n  %@: %@", title, getStringWithKey(NA_STRING)];
		else
			description = [description stringByAppendingFormat:@"\n  %@: %ld%%", title, (long)accuracy];
	}
	
	return description;
}

@end
