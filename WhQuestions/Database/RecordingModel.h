//
//  RecordingModel.h
//  MAP
//
//  Created by Prasad Tandulwadkar on 09/09/10.
//  Copyright 2010 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordingModel : NSObject {
	NSInteger recording_id;
	NSInteger ss_id;
	NSInteger word_id;
	NSString *path;
}

@property (nonatomic, assign) NSInteger recording_id;
@property (nonatomic, assign) NSInteger ss_id;
@property (nonatomic, assign) NSInteger word_id;
@property (nonatomic, strong) NSString *path;

@end
