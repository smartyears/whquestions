//
//  StudentEntity.m
//  WhQuesions
//
//  Created by Frank J. on 10/7/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "StudentEntity.h"
#import "FMDatabase+SharedInstance.h"
#import "NSString+Folder.h"
#import "SessionEntity.h"

@implementation StudentEntity

@synthesize identifier;
@synthesize name;
@synthesize dateOfbirth;
@synthesize photoName;
@synthesize grade;
@synthesize addtionalObject;
@synthesize username;

- (id)init {
	self = [super init];
	if (self) {
		self.dateOfbirth = [NSDate date];
        self.username = @"";
	}
	return self;
}

- (UIImage *)headerImage {
	if ([self.photoName length] == 0) {
		return [UIImage imageNamed:@"ANSEmpty.png"];
	}
	return [UIImage imageWithContentsOfFile:[[NSString headerFolderPath] stringByAppendingPathComponent:self.photoName]];
}

- (NSArray *)allTestResult {
	return [SessionEntity sessionsByStudent:self];
}

- (void)remove {
	//remove header image
    [StudentEntity removeFileWithPhotoName:self.photoName];

	//remove all sessions
	[[self allTestResult] makeObjectsPerformSelector:@selector(remove)];
	[StudentEntity deleteAStudent:self];
}

+ (void)removeFileWithPhotoName:(NSString*)photoName {
    NSString *path = [[NSString headerFolderPath] stringByAppendingPathComponent:photoName];
    [fileManager removeItemAtPath:path error:nil];
}

+ (NSString *)fileNameWithUIImage:(UIImage *)image {
	if (image == nil) {
		return @"";
	}
	
	NSString *cachefolder = [NSString headerFolderPath];
	//NSArray *array = [cachefolder contents];
	CFUUIDRef theUUID = CFUUIDCreate(NULL);
	CFStringRef uuidString = CFUUIDCreateString(NULL, theUUID);
	CFRelease(theUUID);
    NSString *filePath = [cachefolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", (__bridge NSString*)uuidString]];
	
	CFRelease(uuidString);

	NSData *imageData = UIImagePNGRepresentation(image);
	[imageData writeToFile:filePath atomically:YES];
		
	return [filePath lastPathComponent];
}

+ (NSString *)stringFromDate:(NSDate *)date {
	NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
	
	
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	
	NSString *dateforBD =[formatter stringFromDate:date];
	
	return dateforBD;
}

+ (BOOL)insertNewStudent:(StudentEntity *)aStudent {
	NSString *insertStatement = @"INSERT INTO students (name, dateOfbirth, photo, grade, username) VALUES(?, ?, ?, ?, ?);";
	return [[FMDatabase sharedFMDatabase] executeUpdate:insertStatement,
			aStudent.name,
			aStudent.dateOfbirth,
			aStudent.photoName,
			aStudent.grade,
            aStudent.username,
			nil];
}
+ (BOOL)deleteAStudent:(StudentEntity *)student {
	NSString *deleteStatement = @"DELETE FROM students WHERE identifier = ?;";
	return [[FMDatabase sharedFMDatabase] executeUpdate:deleteStatement,
			[NSNumber numberWithInteger:student.identifier],
			nil];
}
+ (BOOL)updateAStudent:(StudentEntity *)student {
	NSString *updateStatement = @"UPDATE students SET name = ?, dateOfbirth = ?, grade = ?, photo = ?, username = ? WHERE identifier = ?;";
	return [[FMDatabase sharedFMDatabase] executeUpdate:updateStatement,
			student.name,
			student.dateOfbirth,
			student.grade,
			student.photoName,
            student.username,
			[NSNumber numberWithInteger:student.identifier],
			nil];
}
+ (NSArray *)allStudents {
	NSString * query = @"SELECT * FROM students order by lower(name)";
	
	FMResultSet* rs = [[FMDatabase sharedFMDatabase] executeQuery:query];
	
	NSMutableArray *array = [NSMutableArray array];
	while ([rs next]) {
		StudentEntity *aStudent = [[StudentEntity alloc] init];
		
		aStudent.identifier = [rs intForColumn:@"identifier"];
		aStudent.name = [rs stringForColumn:@"name"]; 
		aStudent.dateOfbirth = [rs dateForColumn:@"dateOfbirth"]; 
		aStudent.grade = [rs stringForColumn:@"grade"];
		aStudent.photoName = [rs stringForColumn:@"photo"];
        aStudent.username = [rs stringForColumn:@"username"];

		[array addObject:aStudent];
	}
	
	return array;
}
+ (StudentEntity *)getStudent:(NSString *)name {
	NSString *sql = [NSString stringWithFormat:@"SELECT * FROM students WHERE name LIKE '%@' COLLATE NOCASE", name];
	FMResultSet *resultSet = [[FMDatabase sharedFMDatabase] executeQuery:sql];

	StudentEntity *student = nil;
	if ([resultSet next]) {
		student = [[StudentEntity alloc] init];

		student.identifier = [resultSet intForColumn:@"identifier"];
		student.name = [resultSet stringForColumn:@"name"];
		student.dateOfbirth = [resultSet dateForColumn:@"dateOfbirth"];
		student.grade = [resultSet stringForColumn:@"grade"];
		student.photoName = [resultSet stringForColumn:@"photo"];
        student.username = [resultSet stringForColumn:@"username"];
	}

	[resultSet close];

	return student;
}

@end
