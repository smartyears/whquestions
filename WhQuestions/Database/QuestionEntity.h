//
//  QuestionEntity.h
//  WhQuesions
//
//  Created by Frank J. on 10/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	QuestionButton_Why = 1, 
	QuestionButton_How, 
	QuestionButton_Where, 
	QuestionButton_Who,
	QuestionButton_When, 
	QuestionButton_What,
	QuestionButtonCount = 6
} QuestionButton;

@interface QuestionEntity : NSObject {
	NSInteger randomValue;
}

@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *sound;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) NSString *questionEN;
@property (nonatomic, copy) NSString *correctEN;
@property (nonatomic, copy) NSString *wrong1EN;
@property (nonatomic, copy) NSString *wrong2EN;
@property (nonatomic, copy) NSString *questionPT;
@property (nonatomic, copy) NSString *correctPT;
@property (nonatomic, copy) NSString *wrong1PT;
@property (nonatomic, copy) NSString *wrong2PT;
@property (nonatomic, copy) NSString *questionSP;
@property (nonatomic, copy) NSString *correctSP;
@property (nonatomic, copy) NSString *wrong1SP;
@property (nonatomic, copy) NSString *wrong2SP;
@property (nonatomic, copy) NSString *questionFR;
@property (nonatomic, copy) NSString *correctFR;
@property (nonatomic, copy) NSString *wrong1FR;
@property (nonatomic, copy) NSString *wrong2FR;

+ (NSArray *)questionsByType:(QuestionButton)questiontype activityType:(NSInteger)activityType;
+ (NSArray *)questionsByTypes:(NSArray *)questionTypes activitType:(NSInteger)activityType;

@end
