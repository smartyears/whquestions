//
//  RecordingDAO.m
//  MAP
//
//  Created by Prasad Tandulwadkar on 09/09/10.
//  Copyright 2010 Smarty Ears. All rights reserved.
//

#import "RecordingDAO.h"
#import "RecordingModel.h"
#import "FMDatabase+SharedInstance.h"
#import "FMDatabase.h"

@implementation RecordingDAO

+ (id)recordingDAO {
	return [[RecordingDAO alloc] init];
}

- (NSInteger)getMaxRecordingID{
	NSString *query = @"SELECT MAX(recording_id) AS MAXRecordingID FROM Recordings";
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	FMResultSet* rs = [db executeQuery:query];

	int maxRecordingId = 0;
	while ([rs next] ) {
		
		maxRecordingId = [rs  intForColumn:@"MAXRecordingID"];
	}
	
	[rs close];
	if ([db hadError]) {
        NSLog(@"getMaxRecordingID Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
	return maxRecordingId;
}

- (void)insertRecording:(RecordingModel *)recordingModel{
	NSString *query= @"INSERT INTO Recordings (recording_id,ss_id,word_id,path) VALUES (?,?,?,?)";
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	[db executeUpdate:query,[NSNumber numberWithInteger:recordingModel.recording_id],[NSNumber numberWithInteger:recordingModel.ss_id],[NSNumber numberWithInteger:recordingModel.word_id],recordingModel.path];
	if ([[FMDatabase sharedFMDatabase] hadError]) {
        NSLog(@"insertRecording Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
}

- (BOOL)isRecordingPresentForSS_ID:(NSInteger)ssid forWord:(NSInteger)wordid{
	BOOL recordingPresent = NO;
	
	NSString *query = @"SELECT * FROM Recordings where ss_id=? and word_id=?";
	
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	
	FMResultSet *rs = [db executeQuery:query,[NSNumber numberWithInteger:ssid],[NSNumber numberWithInteger:wordid]];
	
	while([rs next]) {
		recordingPresent = YES;	
	}
	[rs close];	
	if ([db hadError]) {
        NSLog(@"isRecordingPresentForSS_ID forWord Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
	return recordingPresent;
}
- (void)deleteRecordingPresentForSS_ID:(NSInteger)ssid forWord:(NSInteger)wordid{
	NSString *query = @"DELETE FROM Recordings where ss_id=? and word_id=?";
	
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	
	[db executeUpdate:query,[NSNumber numberWithInteger:ssid],[NSNumber numberWithInteger:wordid]];
	
	if ([db hadError]) {
        NSLog(@"isRecordingPresentForSS_ID forWord Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
}

- (RecordingModel *)getRecordingForSS_ID:(NSInteger)ssid forWord:(NSInteger)wordid{
	NSString *query = @"SELECT * FROM Recordings where ss_id=? and word_id=?";
	
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	
	FMResultSet *rs = [db executeQuery:query,[NSNumber numberWithInteger:ssid],[NSNumber numberWithInteger:wordid]];
	
	NSMutableArray *results = [[NSMutableArray alloc] init];
	
	while([rs next]) {
		RecordingModel *model = [[RecordingModel alloc] init];
		
		model.recording_id = [rs intForColumn:@"recording_id"];
		model.ss_id = [rs intForColumn:@"ss_id"];
		model.word_id = [rs intForColumn:@"word_id"];
		model.path = [[rs stringForColumn:@"path"] lastPathComponent];
		
		[results addObject:model];
	}
	[rs close];	
	if ([db hadError]) {
        NSLog(@"getRecordingForSS_ID forWord Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
	return [results lastObject];
}

- (NSMutableArray *)getAllRecordingsForSSID:(NSInteger)ssid{
	NSString *query = @"SELECT * FROM Recordings where ss_id=?";
	
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	
	FMResultSet *rs = [db executeQuery:query,[NSNumber numberWithInteger:ssid]];
	
	NSMutableArray *results = [[NSMutableArray alloc] init];
	
	while([rs next]) {
		RecordingModel *model = [[RecordingModel alloc] init];
		
		model.recording_id = [rs intForColumn:@"recording_id"];
		model.ss_id = [rs intForColumn:@"ss_id"];
		model.word_id = [rs intForColumn:@"word_id"];
		model.path = [rs stringForColumn:@"path"];
		
		[results addObject:model];
	}
	[rs close];	
	if ([db hadError]) {
		NSLog(@"getAllRecordingsForSSID Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
	return results;
}

- (void)deleteAllRecordingsForSSID:(NSInteger)ssid{
	NSArray *allRecordedPaths = [self getAllRecordingsPathForSSID:ssid];
	for (NSString *path in allRecordedPaths) {
		[fileManager removeItemAtPath:path error:nil];
	}
	
	NSString *query=@"DELETE FROM Recordings WHERE ss_id=?";
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	[db executeUpdate:query,[NSNumber numberWithInteger:ssid]];
	
	if ([db hadError]) {
		NSLog(@"deleteAllRecordingsForSSID Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
}

- (NSMutableArray *)getAllRecordingsPathForSSID:(NSInteger)ssid{
	NSString *query = @"SELECT path from Recordings where ss_id=?";
	
	FMDatabase *db = [FMDatabase sharedFMDatabase];
	
	FMResultSet *rs = [db executeQuery:query,[NSNumber numberWithInteger:ssid]];
	
	NSString * recordingPath = nil;
	NSMutableArray * recordingPathArray = [[NSMutableArray alloc]init];
	
	while([rs next]) {
		recordingPath = [rs stringForColumn:@"path"];
		if(recordingPath != nil) {
			[recordingPathArray addObject:recordingPath];
		}
		
	}
	[rs close];	
	if ([db hadError]) {
        NSLog(@"getAllRecordingsPathForSSID Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
	return recordingPathArray;
}

@end
