//
//  QuestionEntity.m
//  WhQuesions
//
//  Created by Frank J. on 10/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "QuestionEntity.h"
#import "FMDatabase+SharedInstance.h"
#import "WhQuestionsSetting.h"
#import <objc/runtime.h>

@implementation QuestionEntity

@synthesize identifier;
@synthesize image;
@synthesize sound;
@synthesize type;
@synthesize questionEN;
@synthesize correctEN;
@synthesize wrong1EN;
@synthesize wrong2EN;
@synthesize questionPT;
@synthesize correctPT;
@synthesize wrong1PT;
@synthesize wrong2PT;
@synthesize questionSP;
@synthesize correctSP;
@synthesize wrong1SP;
@synthesize wrong2SP;
@synthesize questionFR;
@synthesize correctFR;
@synthesize wrong1FR;
@synthesize wrong2FR;

- (NSInteger)randomValue {
	if (randomValue == 0) {
		srandom((unsigned)time(0));
		randomValue = arc4random() % 10000;
	}
	return randomValue;
}

- (NSString *)description {
    unsigned int outCount, i;
    NSMutableArray *keys = [NSMutableArray arrayWithCapacity:20];
    Class klass = [self class];
	
    // I don't want to venture into (or beyond) NSObject
    while (klass != [NSObject class]) {
        objc_property_t *properties = class_copyPropertyList(klass, &outCount);
		
        for(i = 0; i < outCount; i++) 
		{
            objc_property_t property = properties[i];
            const char *propName = property_getName(property);
            if(propName) 
			{
                NSString *propertyName = [NSString stringWithCString:propName encoding:NSNonLossyASCIIStringEncoding];
                [keys addObject:propertyName];
			}
		}
        free(properties);
        
        klass = [klass superclass];
    }
    
    return [NSString stringWithFormat:@"<%@: %p; %@>", NSStringFromClass([self class]), self, [self dictionaryWithValuesForKeys:keys]];
}

+ (NSArray *)questionsByType:(QuestionButton)questiontype activityType:(NSInteger)activityType {
	NSString * query = @"SELECT * FROM Questions where type = ? order by identifier";
	
	FMResultSet* rs = [[FMDatabase sharedFMDatabase] executeQuery:query, [NSNumber numberWithInt:questiontype]];
	
	NSMutableArray *array = [NSMutableArray array];
	while ([rs next]) {
		QuestionEntity *aQuestion = [[QuestionEntity alloc] init];
		
		aQuestion.identifier = [rs intForColumn:@"identifier"];
		aQuestion.image = [rs stringForColumn:@"image"];
		aQuestion.sound = [rs stringForColumn:@"sound"];
		aQuestion.type = [rs intForColumn:@"type"];
		aQuestion.questionEN = [rs stringForColumn:@"questionEN"];
		aQuestion.correctEN = [rs stringForColumn:@"correctEN"];
		aQuestion.wrong1EN = [rs stringForColumn:@"wrong1EN"];
		aQuestion.wrong2EN = [rs stringForColumn:@"wrong2EN"];
		aQuestion.questionPT = [rs stringForColumn:@"questionPT"];
		aQuestion.correctPT = [rs stringForColumn:@"correctPT"];
		aQuestion.wrong1PT = [rs stringForColumn:@"wrong1PT"];
		aQuestion.wrong2PT = [rs stringForColumn:@"wrong2PT"];
		aQuestion.questionSP = [rs stringForColumn:@"questionSP"];
		aQuestion.correctSP = [rs stringForColumn:@"correctSP"];
		aQuestion.wrong1SP = [rs stringForColumn:@"wrong1SP"];
		aQuestion.wrong2SP = [rs stringForColumn:@"wrong2SP"];
		aQuestion.questionFR = [rs stringForColumn:@"questionFR"];
		aQuestion.correctFR = [rs stringForColumn:@"correctFR"];
		aQuestion.wrong1FR = [rs stringForColumn:@"wrong1FR"];
		aQuestion.wrong2FR = [rs stringForColumn:@"wrong2FR"];
		if (activityType == ACTIVITY_SELECT) {
			if (aQuestion.correctEN.length > 0 && aQuestion.wrong1EN.length > 0 && aQuestion.wrong2EN.length > 0 &&
				aQuestion.correctPT.length > 0 && aQuestion.wrong1PT.length > 0 && aQuestion.wrong2PT.length > 0 &&
				aQuestion.correctSP.length > 0 && aQuestion.wrong1SP.length > 0 && aQuestion.wrong2SP.length > 0 &&
				aQuestion.correctFR.length > 0 && aQuestion.wrong1FR.length > 0 && aQuestion.wrong2FR.length > 0) {
				[array addObject:aQuestion];
			}
		}
		else {
			[array addObject:aQuestion];
		}
	}
	
	return array;
}

+ (NSArray *)questionsByTypes:(NSArray *)questionTypes activitType:(NSInteger)activityType {
	NSMutableString* query = [[NSMutableString alloc] initWithString:@"SELECT * FROM Questions where type = "];
	[query appendString:[questionTypes componentsJoinedByString:@" or type = "]];
	
	//append order by
	[query appendString:@" order by identifier"];
	FMResultSet* rs = [[FMDatabase sharedFMDatabase] executeQuery:query, questionTypes];
	
	NSMutableArray *array = [NSMutableArray array];
	while ([rs next]) {
		QuestionEntity *aQuestion = [[QuestionEntity alloc] init];
		
		aQuestion.identifier = [rs intForColumn:@"identifier"];
		aQuestion.image = [rs stringForColumn:@"image"];
		aQuestion.sound = [rs stringForColumn:@"sound"];
		aQuestion.type = [rs intForColumn:@"type"];
		aQuestion.questionEN = [rs stringForColumn:@"questionEN"];
		aQuestion.correctEN = [rs stringForColumn:@"correctEN"];
		aQuestion.wrong1EN = [rs stringForColumn:@"wrong1EN"];
		aQuestion.wrong2EN = [rs stringForColumn:@"wrong2EN"];
		aQuestion.questionPT = [rs stringForColumn:@"questionPT"];
		aQuestion.correctPT = [rs stringForColumn:@"correctPT"];
		aQuestion.wrong1PT = [rs stringForColumn:@"wrong1PT"];
		aQuestion.wrong2PT = [rs stringForColumn:@"wrong2PT"];
		aQuestion.questionSP = [rs stringForColumn:@"questionSP"];
		aQuestion.correctSP = [rs stringForColumn:@"correctSP"];
		aQuestion.wrong1SP = [rs stringForColumn:@"wrong1SP"];
		aQuestion.wrong2SP = [rs stringForColumn:@"wrong2SP"];
		aQuestion.questionFR = [rs stringForColumn:@"questionFR"];
		aQuestion.correctFR = [rs stringForColumn:@"correctFR"];
		aQuestion.wrong1FR = [rs stringForColumn:@"wrong1FR"];
		aQuestion.wrong2FR = [rs stringForColumn:@"wrong2FR"];
		if (activityType == ACTIVITY_SELECT) {
			if (aQuestion.correctEN.length > 0 && aQuestion.wrong1EN.length > 0 && aQuestion.wrong2EN.length > 0 &&
				aQuestion.correctPT.length > 0 && aQuestion.wrong1PT.length > 0 && aQuestion.wrong2PT.length > 0 &&
				aQuestion.correctSP.length > 0 && aQuestion.wrong1SP.length > 0 && aQuestion.wrong2SP.length > 0 &&
				aQuestion.correctFR.length > 0 && aQuestion.wrong1FR.length > 0 && aQuestion.wrong2FR.length > 0) {
				[array addObject:aQuestion];
			}
		}
		else {
			[array addObject:aQuestion];
		}
	}
	if ([WhQuestionsSetting randomQuestion]) {
		[array sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"randomValue" ascending:YES]]];
	}
	
	return array;
}
@end
