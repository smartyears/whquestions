//
//  QuestionsManagement.h
//  WhQuesions
//
//  Created by Frank J. on 10/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SessionEntity;
@class QuestionEntity;
@class StudentEntity;

@interface QuestionsManagement : NSObject

@property (nonatomic, strong) NSArray *subQuestions;
@property (nonatomic, strong) SessionEntity *currentSession;

- (id)initWithQuestions:(NSArray *)questions andStudent:(StudentEntity *)aStudent activityType:(NSInteger)activityType;
- (QuestionEntity *)questionentityForIndex:(NSInteger)index;

@end
