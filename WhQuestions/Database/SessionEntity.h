//
//  SessionEntity.h
//  WhQuesions
//
//  Created by Frank J. on 10/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@class QuestionEntity;
@class StudentEntity;
@interface SessionEntity : NSObject {
@private
	NSInteger identifier;
	NSInteger studentIdentifier;
	NSDate * date;
	
	NSInteger howCorrect;
	NSInteger howWrong;
	
	NSInteger whatCorrect;
	NSInteger whatWrong;
	
	NSInteger whereCorrect;
	NSInteger whereWrong;
	
	NSInteger whenCorrect;
	NSInteger whenWrong;
	
	NSInteger whyCorrect;
	NSInteger whyWrong;
	
	NSInteger whoCorrect;
	NSInteger whoWrong;
	
	NSInteger activityType;
}

@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) NSInteger studentIdentifier;
@property (nonatomic, strong) NSDate *date;

@property (nonatomic, assign) NSInteger howCorrect;
@property (nonatomic, assign) NSInteger howWrong;

@property (nonatomic, assign) NSInteger whatCorrect;
@property (nonatomic, assign) NSInteger whatWrong;

@property (nonatomic, assign) NSInteger whereCorrect;
@property (nonatomic, assign) NSInteger whereWrong;

@property (nonatomic, assign) NSInteger whenCorrect;
@property (nonatomic, assign) NSInteger whenWrong;

@property (nonatomic, assign) NSInteger whyCorrect;
@property (nonatomic, assign) NSInteger whyWrong;

@property (nonatomic, assign) NSInteger whoCorrect;
@property (nonatomic, assign) NSInteger whoWrong;

@property (nonatomic, assign) NSInteger activityType;

- (void)markCorrectForQuestion:(QuestionEntity *)aQuestion;
- (void)markWrongForQuestion:(QuestionEntity *)aQuestion;
- (BOOL)isEmpty;
- (BOOL)save;
- (void)remove;
- (NSString *)displayedDate;
- (NSString *)getTRCDescription;

+ (void)removeSession:(SessionEntity *)session;
+ (NSArray *)sessionsByStudent:(StudentEntity *)aStudent;
+ (NSInteger)occupyASessionID;

@end
