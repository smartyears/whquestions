//
//  StudentEntity.h
//  WhQuesions
//
//  Created by Frank J. on 10/7/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudentEntity : NSObject

@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSDate *dateOfbirth;
@property (nonatomic, copy) NSString *photoName;
@property (nonatomic, copy) NSString *grade;
@property (nonatomic, strong) NSObject *addtionalObject;
@property (nonatomic, strong) NSString *username;


- (UIImage *)headerImage;
- (NSArray *)allTestResult;
- (void)remove;

+ (void)removeFileWithPhotoName:(NSString*)photoName;
+ (NSString *)fileNameWithUIImage:(UIImage *)image;
+ (NSString *)stringFromDate:(NSDate *)date;

+ (BOOL)insertNewStudent:(StudentEntity *)aStudent;
+ (BOOL)deleteAStudent:(StudentEntity *)student;
+ (BOOL)updateAStudent:(StudentEntity *)student;
+ (NSArray *)allStudents;
+ (StudentEntity *)getStudent:(NSString *)name;

@end
