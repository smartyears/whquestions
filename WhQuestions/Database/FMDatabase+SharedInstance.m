//
//  FMDatabase+SharedInstance.m
//  UTTServices
//
//  Created by Frank J. on 6/27/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "FMDatabase+SharedInstance.h"
#import "NSString+Folder.h"
#import "DatabaseMigrator.h"

static NSString *const KDBFileName = @"WhQuestion.sqlite";
static FMDatabase* shareFMDatabase = nil;

@implementation FMDatabase(SharedInstance)
+ (FMDatabase*)sharedFMDatabase {
	@synchronized(self)
	{
		if (shareFMDatabase == nil) {
			BOOL succeeded = NO;
#if !defined(UNITTEST)
			//First check if the database is already present in documents directory
			NSString *writableDBPath = [[NSString dataFolder] stringByAppendingPathComponent:KDBFileName];

			NSFileManager * fileManager = [NSFileManager defaultManager];
			succeeded = [fileManager fileExistsAtPath:writableDBPath]; 
			if (!succeeded) {
				NSString *dbpath = [[NSBundle bundleForClass:[self class]] pathForResource:KDBFileName ofType:nil];
				
				NSError* error;
				succeeded = [fileManager copyItemAtPath:dbpath toPath:writableDBPath error:&error]; 
				if(!succeeded)
				{
					//NSLog(@"Copy Database failed :%@, dbpath:%@ writableDBPath:%@", [error localizedDescription], dbpath, writableDBPath);
				}
			}
#else
			NSString *writableDBPath = [[NSBundle bundleForClass:[self class]] pathForResource:KDBFileName ofType:nil];
#endif
			DatabaseMigrator *dbMigrator = [[DatabaseMigrator alloc] initWithDatabaseFile:KDBFileName];
			[dbMigrator migrateToVersion:CURRENT_SCHEMA_VERSION];

			//NSLog(@"db path:%@", writableDBPath);
			FMDatabase *db = [FMDatabase databaseWithPath:writableDBPath];
			succeeded = [db open];
			if(succeeded)
			{
				shareFMDatabase = db;
				
#if defined(DEBUG)
				db.traceExecution = YES;
#endif
				//db.shouldCacheStatements = YES;
				db.logsErrors = YES;
			}
			else {
				shareFMDatabase = nil;
			}
		}
	}
	return shareFMDatabase;
}
+ (void)closeDatabaseIfOpened {
	@synchronized(self)
	{
		if (shareFMDatabase != nil) {
			[shareFMDatabase close];
			shareFMDatabase = nil;
		}
	}
}
+ (void)removeDatabase {
	[self closeDatabaseIfOpened];
	
#if !defined(UNITTEST)
	BOOL succeeded = NO;
	//First check if the database is already present in documents directory
	NSString *writableDBPath = [[NSString dataFolder] stringByAppendingPathComponent:KDBFileName];
	
	NSFileManager * fileManager = [NSFileManager defaultManager];
	succeeded = [fileManager fileExistsAtPath:writableDBPath]; 
	if (succeeded){
		[fileManager removeItemAtPath:writableDBPath error:nil];
	}
#endif
	
}
@end
