//
//  QuestionsManagement.m
//  WhQuesions
//
//  Created by Frank J. on 10/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "QuestionsManagement.h"
#import "SessionEntity.h"
#import "StudentEntity.h"

@implementation QuestionsManagement

@synthesize subQuestions;
@synthesize currentSession;

- (id)initWithQuestions:(NSArray *)questions  andStudent:(StudentEntity *)aStudent activityType:(NSInteger)activityType {
	self = [super init];
	if (self) {
		self.subQuestions = questions;
		
		SessionEntity *aSession = [[SessionEntity alloc] init];
		aSession.studentIdentifier = aStudent.identifier;
		aSession.activityType = activityType;
		self.currentSession = aSession;
	}
	return self;
}

- (QuestionEntity *)questionentityForIndex:(NSInteger)index {
	if ([self.subQuestions count] == 0) {
		return nil;
	}
	
	QuestionEntity *aQuestion = [self.subQuestions objectAtIndex:index % [self.subQuestions count]];
	return aQuestion;
}

@end
