//
//  RecordingDAO.h
//  MAP
//
//  Created by Prasad Tandulwadkar on 09/09/10.
//  Copyright 2010 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RecordingModel;

@interface RecordingDAO : NSObject

+ (id)recordingDAO;
- (NSInteger)getMaxRecordingID;
- (void)insertRecording:(RecordingModel *)recordingModel;
- (BOOL)isRecordingPresentForSS_ID:(NSInteger)ssid forWord:(NSInteger)wordid;
- (void)deleteRecordingPresentForSS_ID:(NSInteger)ssid forWord:(NSInteger)wordid;
- (RecordingModel *)getRecordingForSS_ID:(NSInteger)ssid forWord:(NSInteger)wordid;
- (NSMutableArray *)getAllRecordingsForSSID:(NSInteger)ssid;
- (void)deleteAllRecordingsForSSID:(NSInteger)ssid;
- (NSMutableArray *)getAllRecordingsPathForSSID:(NSInteger)ssid;

@end
