//
//  DatabaseMigrator.m
//  safecell
//
//  Created by Ben Scheirman on 4/20/10.
//  Copyright 2010 ChaiONE. All rights reserved.
//

#import "DatabaseMigrator.h"
#import "NSString+Common.h"
#import "NSString+Folder.h"

//if you need to force a migration # for whatever reason, uncomment this line.
//Just remember to put it back when you're done!
//#define FORCE_MIGRATION 5

@implementation DatabaseMigrator

@synthesize filename = _filename, overwriteDatabase;

- (id)initWithDatabaseFile:(NSString *)filename {
	if (self = [super init]) {
		self.filename = filename;
		database = [[FMDatabase alloc] initWithPath:[self databasePath]];
		[database setLogsErrors:YES];
	}
	
	return self;
}

- (NSString *)databasePath {
	return [[NSString dataFolder] stringByAppendingPathComponent:self.filename];
}

- (BOOL)moveDatabaseToUserDirectoryIfNeeded {
	NSString *databasePath = [self databasePath];
	
	NSFileManager *fileMgr = [NSFileManager defaultManager];
	if ([fileMgr fileExistsAtPath:databasePath]) {
		if (overwriteDatabase) {
			NSLog(@"Overwrite is set to YES, deleting old database file...");
			[fileMgr removeItemAtPath:databasePath error:nil];
		} else {
			return NO;
		}
	}
	
	NSArray *fileParts = [self.filename splitOnChar:'.'];
	if (fileParts == NULL || [fileParts count] < 2) {
		NSLog(@"Invalid filename passed to verifyWritableDatabase ==> %@", self.filename);
		[[NSException exceptionWithName:@"Invalid filename" reason:@"Expected a filename like foo.db" userInfo:nil] raise];
		exit(-1);
	}
	
	NSString *name = [fileParts objectAtIndex:0];
	NSString *extension = [fileParts objectAtIndex:1];
	
	NSLog(@"Moving database from app package to user directory");
	NSString *dbPathFromAppPackage = [NSBundle.mainBundle pathForResource:name ofType:extension];
	
	if (dbPathFromAppPackage == nil) {
		[[NSException exceptionWithName:@"Invalid resource path" reason:[NSString stringWithFormat:@"Couldn't find %@ in the bundle!", self.filename] userInfo:nil] raise];
		exit(1);
	}
	
	NSError *error;
	BOOL success = [fileMgr copyItemAtPath:dbPathFromAppPackage toPath:databasePath error:&error];
	
	NSLog(@"done copying");
	if (!success) {
		NSString *msg = [NSString stringWithFormat:@"Error moving database to user directory: %@", [error localizedDescription]];
		NSLog(@"error: %@", msg);
		UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Database error" message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [APP_DELEGATE.window.rootViewController presentViewController:alertController animated:true completion:nil];
	}
	
	return YES;
}

//gets the current version of the database
- (NSInteger)version {
	
#ifdef FORCE_MIGRATION
	[self setVersion:FORCE_MIGRATION];
#endif
	
	NSInteger version = [database intForQuery:@"PRAGMA user_version"];
	return version;
}

//sets the current version of the database
- (void)setVersion:(NSInteger)version {
	[database executeUpdate:[NSString stringWithFormat:@"PRAGMA user_version=%ld", (long)version]];
}

//applies a migration file (migration-X.sql -- where X is the migration #)
//the first migration file would be 1 (since the db starts at version 0)
- (BOOL)applyMigration:(NSInteger)version {
	NSString *migrationFile = [NSString stringWithFormat:@"/migration-%ld.sql", (long)version];
	NSLog(@"File: %@", migrationFile);
	NSString *fullPath = [NSBundle.mainBundle.resourcePath stringByAppendingString:migrationFile];
	NSLog(@"Path: %@", fullPath);
	
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
		NSLog(@"WARNING: Couldn't find migration-%ld.sql at %@", (long)version, fullPath);
		return NO;
	}
	
	NSString *migrationSql = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:nil];
	
	@try {
		NSArray *statements = [migrationSql splitOnChar:';'];
		
		for(NSString *statement in statements) {
			NSString *cleanedStatement = [statement stringByStrippingWhitespace];
			
			if ([cleanedStatement length] > 0) {
				[database executeUpdate:cleanedStatement];
				
				if ([database hadError]) {
					return NO;
				}
			}
		}
	}
	@catch (NSException *exception) {
		NSLog(@"Error executing migration %ld.  The error was: %@", (long)version, exception);
		NSLog(@"Continuing anyway...");
	}
	
	return YES;
}

//applies all necessary migrations to bring this database up to the specified version
- (void)migrateToVersion:(NSInteger)version {
	[database open];
	NSInteger currentVersion = [self version];
	
	if (currentVersion == version) {
//		NSLog(@"No migration needed, already at version %ld", (long)version);
		[database close];
		return;
	}
	
	BOOL success = NO;
	for(NSInteger m = currentVersion + 1; m <= version; m++) {
		NSLog(@"Running migration %ld", (long)m);
		
		[database beginTransaction];
		success = [self applyMigration:m];
		
        if (success) {
            [database commit];
			[self setVersion:m];
        }
		if (!success) {
            [database rollback];
			NSLog(@"Error executing migration %ld", (long)m);
			break;
		}
	}
	[database close];
	
	NSLog(@"Done with migrations....");
	if (!success) {
		NSLog(@"There were errors during the migration.  The current version is %ld", (long)[self version]);
	} else {
		NSLog(@"Successfully migrated to version %ld", (long)version);
	}
}


@end
