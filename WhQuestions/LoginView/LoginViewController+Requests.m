//
//  LoginViewController+Requests.m
//  ArticulateIt
//
//  Created by Alex Golovaschenko on 17.12.21.
//  Copyright © 2021 Smarty Ears. All rights reserved.
//

#import "LoginViewController+Requests.h"
#import "StudentEntity.h"

NSString *const KLoginURL = @"https://admin.speechandlanguageacademy.com/api/get-therapist-students";
NSString *const KImageBaseURL = @"https://admin.speechandlanguageacademy.com";

@implementation LoginViewController (Requests)

- (void)performLoginRequest {
    [self showProgressView];
    
    NSURLComponents *components = [NSURLComponents componentsWithString:KLoginURL];
    NSURLQueryItem *userNameItem = [NSURLQueryItem queryItemWithName:@"username" value:self.emailTextField.text];
    NSURLQueryItem *passwordItem = [NSURLQueryItem queryItemWithName:@"password" value:self.passwordTextField.text];
    components.queryItems = @[userNameItem, passwordItem];
        
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:components.URL];
    [urlRequest setHTTPMethod:@"POST"];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if(httpResponse.statusCode == 200) {
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                
                NSArray *studendsDataArray = [responseDictionary objectForKey:@"body"];
                if (parseError != nil || studendsDataArray == nil) {
                    [self showSomethingWentWrongAlert];
                    [self hideProgressView];
                    NSLog(@"No 'body' field in json");
                    return;
                }
                
                [self parseStudentsDataArray:studendsDataArray];
            }
            else if (httpResponse.statusCode == 401) {
                [self showWrongEmailOrPasswordAlert];
                [self hideProgressView];
                NSLog(@"Wrong email or password");
            }
            else {
                [self showSomethingWentWrongAlert];
                [self hideProgressView];
                NSLog(@"%@", error);
            }
        });
    }];
    [dataTask resume];
}

- (void)parseStudentsDataArray:(NSArray*)studentsDataArray {
    dispatch_async(self.studentsParsingQueue, ^{
        NSArray *allStudents = [StudentEntity allStudents];
        
        for (NSDictionary *studentModel in studentsDataArray) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@", studentModel[@"username"]];
            NSArray *studentsWithUsername = [allStudents filteredArrayUsingPredicate:predicate];
            StudentEntity *existingStudent = studentsWithUsername.count > 0 ? [studentsWithUsername firstObject] : nil;
            
            StudentEntity *student = existingStudent ? existingStudent : [[StudentEntity alloc] init];
            student.name = studentModel[@"name"];
            student.grade = @"";
            student.dateOfbirth = [NSDate date];
            student.username = studentModel[@"username"];
            
            UIImage *avatarImage = [self createAvatarImageWithAvatarData:studentModel[@"avatar"]];
            if (avatarImage) {
                if (existingStudent) {
                    [StudentEntity removeFileWithPhotoName:student.photoName];
                }
                student.photoName = [StudentEntity fileNameWithUIImage:avatarImage];
            }
            
            if (existingStudent) {
                [StudentEntity updateAStudent:student];
            } else {
                [StudentEntity insertNewStudent:student];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [NSNotificationCenter.defaultCenter postNotificationName:KNotification_loginDone object:nil];
            [self hideProgressView];
            [self closeViewController];
        });
    });
}

- (UIImage*)createAvatarImageWithAvatarData:(NSDictionary*)avatarData {
    NSArray<UIImage*> *avatarImages = [self loadAvatarImagesWithAvatarData:avatarData];
    if (avatarImages.count == 0) {
        return nil;
    }
    
    CGSize imageSize = CGSizeMake(400.f, 400.f);
    CGRect drawRect = CGRectMake(0.f, 0.f, 400.f, 400.f);
    
    UIGraphicsBeginImageContext(imageSize);
    for (UIImage *image in avatarImages) {
        [image drawInRect:drawRect];
    }
    UIImage *avatarImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return avatarImage;
}

- (NSArray<UIImage*>*)loadAvatarImagesWithAvatarData:(NSDictionary*)avatarData {
    dispatch_group_t downloadImagesGroup = dispatch_group_create();

    NSArray *urls = @[avatarData[@"body"],
                      avatarData[@"skin"],
                      avatarData[@"lips"],
                      avatarData[@"hairs"],
                      avatarData[@"eyes"],
                      avatarData[@"accesories"]];
    
    NSMutableArray *images = [NSMutableArray array];
    for (int i = 0; i < urls.count; i++) {
        [images addObject:[NSNull null]];
    }
    
    for (int i = 0; i < urls.count; i++) {
        NSString *urlString = urls[i];
        if ([urlString isEqual:[NSNull null]]) {
            continue;
        }
        dispatch_group_enter(downloadImagesGroup);
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        urlString = [NSString stringWithFormat:@"%@%@", KImageBaseURL, urlString];
        
        [self loadImageFromURL:[NSURL URLWithString:urlString] completion:^(UIImage *image) {
            if (image) {
                [images replaceObjectAtIndex:i withObject:image];
            }
            dispatch_group_leave(downloadImagesGroup);
        }];
    }
    
    dispatch_group_wait(downloadImagesGroup, DISPATCH_TIME_FOREVER);
    
    for (int i = 0; i < images.count; i++) {
        if ([images[i] isEqual:[NSNull null]]) {
            [images removeObjectAtIndex:i];
            i--;
        }
    }
    
    return images;
}

- (void)loadImageFromURL:(NSURL*)url completion:(void (^)(UIImage *image))completion {
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *imageDownloadingTask = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (error != nil) {
            NSLog(@"%@", error);
        }
        UIImage *downloadedImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:location]];
        completion(downloadedImage);
    }];
    [imageDownloadingTask resume];
}
@end
