//
//  LoginViewController+Requests.h
//  ArticulateIt
//
//  Created by Alex Golovaschenko on 17.12.21.
//  Copyright © 2021 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController (Requests)

- (void)performLoginRequest;

@end

NS_ASSUME_NONNULL_END
