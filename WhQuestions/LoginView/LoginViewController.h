//
//  LoginViewController.h
//  ArticulateIt
//
//  Created by Alex Golovaschenko on 16.12.21.
//  Copyright © 2021 Smarty Ears. All rights reserved.
//

extern NSString * _Nonnull const KNotification_loginDone;

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) dispatch_queue_t studentsParsingQueue;

- (void)closeViewController;
- (void)showProgressView;
- (void)hideProgressView;

- (void)showWrongEmailOrPasswordAlert;
- (void)showSomethingWentWrongAlert;

@end

NS_ASSUME_NONNULL_END
