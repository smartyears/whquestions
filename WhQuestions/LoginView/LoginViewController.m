//
//  LoginViewController.m
//  ArticulateIt
//
//  Created by Alex Golovaschenko on 16.12.21.
//  Copyright © 2021 Smarty Ears. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginViewController+Requests.h"

NSString *const KNotification_loginDone = @"KNotification_loginDone";
NSString *const KSignUpURL = @"https://speechandlanguageacademy.com/get-your-free-10-days-trial-to-speech-and-language-academy/";

@interface LoginViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *emailBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *passwordBackgroundView;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIView *progressView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicatorView;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *closeTapGesture;

@property (strong, nonatomic) UIView *uiBlockingView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configure];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self runAppearAnimation];
}

#pragma mark - Configuration

- (void)configure {
    [self configureView];
    [self configureNotifications];
    [self configureQueues];
}

- (void)configureView {
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    if (IS_IPAD) {
        self.contentView.layer.cornerRadius = 30.f;
        self.emailBackgroundView.layer.cornerRadius = 10.f;
        self.passwordBackgroundView.layer.cornerRadius = 10.f;
        self.loginButton.layer.cornerRadius = 10.f;
    }
    else {
        self.emailBackgroundView.layer.cornerRadius = 6.f;
        self.passwordBackgroundView.layer.cornerRadius = 6.f;
        self.loginButton.layer.cornerRadius = 6.f;
    }

    self.progressView.hidden = YES;
    self.progressView.layer.cornerRadius = 15.f;
    self.progressView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.progressView.layer.shadowOpacity = 0.1f;
    self.progressView.layer.shadowRadius = 8.f;
    self.progressView.layer.shadowOffset = CGSizeMake(0.f, 2.f);
}

- (void)configureNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)configureQueues {
    self.studentsParsingQueue = dispatch_queue_create("smartyears.articulateitpro.studentsParsingQueue", DISPATCH_QUEUE_SERIAL);
}

#pragma mark - Main Logic

- (void)closeViewController {
    [self runDisappearAnimation:^{
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

#pragma mark - Transition Animations

- (void)runAppearAnimation {
    self.view.backgroundColor = [UIColor clearColor];
    self.contentView.transform = CGAffineTransformMakeTranslation(0.f, [UIScreen mainScreen].bounds.size.height);
    
    [UIView animateWithDuration:0.4f delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.view.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.5f];
        self.contentView.transform = CGAffineTransformIdentity;
    } completion: nil];
}

- (void)runDisappearAnimation:(void (^)(void))completion {
    [UIView animateWithDuration:0.4f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.view.backgroundColor = [UIColor clearColor];
        self.contentView.transform = CGAffineTransformMakeTranslation(0.f, [UIScreen mainScreen].bounds.size.height);
    } completion: ^(BOOL finished) {
        completion();
    }];
}

#pragma mark - Progress View

- (void)showProgressView {
    self.progressView.hidden = NO;
    self.progressView.alpha = 0.f;
    [self.progressIndicatorView startAnimating];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.progressView.alpha = 1.f;
    }];
    
    self.uiBlockingView = [[UIView alloc] init];
    self.uiBlockingView.backgroundColor = [UIColor clearColor];
    self.uiBlockingView.frame = self.view.bounds;
    self.uiBlockingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.uiBlockingView];
}

- (void)hideProgressView {
    [UIView animateWithDuration:0.3f animations:^{
        self.progressView.alpha = 0.f;
    } completion:^(BOOL finished) {
        self.progressView.hidden = YES;
        [self.uiBlockingView removeFromSuperview];
        [self.progressIndicatorView stopAnimating];
    }];
}

#pragma mark - Alerts

- (void)showAdultGateAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Parental Gate" message:@"Solve this math in order to continue. How much is 10-6?" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = alertController.textFields[0];
        if ([textField.text isEqualToString:@"4"]) {
            [UIApplication.sharedApplication openURL:[NSURL URLWithString:KSignUpURL] options:@{} completionHandler:nil];
        } else {
            [self showAdultGateAlert];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showEmptyFieldAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Can't authorize" message:@"Please, enter your email and password" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showWrongEmailOrPasswordAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Can't authorize" message:@"Wrong email or password" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showSomethingWentWrongAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Something went wrong" message:@"Please, try again later" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)closeTapGestureDidTap:(NSNotification*)notification {
    if (IS_IPAD == NO) {
        return;
    }
    CGPoint location = [self.closeTapGesture locationInView:self.view];
    UIView *hitTestView = [self.view hitTest:location withEvent:nil];
    if (self.view != hitTestView) {
        return;
    }
    [self closeViewController];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSUInteger curve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:duration delay:0.f options:curve animations:^{
        if (IS_IPAD) {
            self.contentView.transform = CGAffineTransformMakeTranslation(0.f, -(keyboardSize.height / 2.f));
        }
        else {
            self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
        }
    } completion:nil];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    NSUInteger curve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:duration delay:0.f options:curve animations:^{
        if (IS_IPAD) {
            self.contentView.transform = CGAffineTransformIdentity;
        } else {
            self.scrollView.contentInset = UIEdgeInsetsZero;
        }
    } completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
    }
    return NO;
}

- (IBAction)loginButtonDidClick:(id)sender {
    if ([self.emailTextField isFirstResponder]) {
        [self.emailTextField resignFirstResponder];
    }
    if ([self.passwordTextField isFirstResponder]) {
        [self.passwordTextField resignFirstResponder];
    }
    
    if (self.emailTextField.text == nil || [self.emailTextField.text isEqualToString:@""] ||
        self.passwordTextField.text == nil || [self.passwordTextField.text isEqualToString:@""]) {
        [self showEmptyFieldAlert];
        return;
    }
    
    [self performLoginRequest];
}

- (IBAction)signUpButtonDidClick:(id)sender {
    [self showAdultGateAlert];
}

- (IBAction)closeButtonDidClick:(id)sender {
    [self closeViewController];
}

@end
