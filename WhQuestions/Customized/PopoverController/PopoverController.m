//
//  PopoverController.m
//  Version 1.1
//
//  Created by Frank J. on 3/13/13.
//  Copyright (c) 2013 Frank J.. All rights reserved.
//

#import "PopoverController.h"

#define SHADOW_SIZE            8

@implementation PopoverController

@synthesize delegate;
@synthesize dismissedWhenTap;
@synthesize showed;
@synthesize roundCornerAndShadow;
@synthesize popoverType;
@synthesize animationDuration;
@synthesize showDarkBackground;

- (instancetype)initWithContentViewController:(UIViewController *)viewController {
    self = [super init];
    if (self) {
        tapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        tapView.backgroundColor = [UIColor clearColor];
        tapView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
        [tapView addGestureRecognizer:tapGesture];

        darkBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        darkBackgroundView.image = [UIImage imageNamed:@"PopoverDarkBackground.png"];
        darkBackgroundView.hidden = YES;
        [tapView addSubview:darkBackgroundView];

        shadowView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        shadowView.image = [[UIImage imageNamed:@"PopoverOutline.png"] stretchableImageWithLeftCapWidth:11 topCapHeight:11];

        outlineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        outlineView.backgroundColor = [UIColor colorWithRed:53 / 255.0 green:62 / 255.0 blue:84 / 255.0 alpha:1.0];
        outlineView.clipsToBounds = YES;
        outlineView.layer.cornerRadius = 4;

        contentViewController = viewController;

        dismissedWhenTap = YES;
        roundCornerAndShadow = NO;
        popoverType = POPOVER_TYPE_CENTER;
        animationDuration = 0.2;
        showDarkBackground = NO;
    }

    return self;
}

- (void)presentPopoverFromPoint:(CGPoint)point inView:(UIView *)view {
    topLeftPoint = point;
    showed = YES;

    [contentViewController view]; // XCode 10 fix for get preferredContentSize
    contentViewController.view.frame = CGRectMake(point.x, point.y, contentViewController.preferredContentSize.width, contentViewController.preferredContentSize.height);

    [view addSubview:tapView];
    if (roundCornerAndShadow) {
        contentViewController.view.layer.cornerRadius = 3;
        contentViewController.view.clipsToBounds = YES;
        CGRect frame = contentViewController.view.frame;
        frame.origin.x -= SHADOW_SIZE;
        frame.origin.y -= SHADOW_SIZE;
        frame.size.width += SHADOW_SIZE * 2;
        frame.size.height += SHADOW_SIZE * 2;
        shadowView.frame = frame;
        [view addSubview:shadowView];

        frame = contentViewController.view.frame;
        frame.origin.x -= 1;
        frame.origin.y -= 1;
        frame.size.width += 2;
        frame.size.height += 2;
        outlineView.frame = frame;
        [view addSubview:outlineView];
    }
    [view addSubview:contentViewController.view];

    tapView.alpha = 0.0;
    contentViewController.view.alpha = 0.0;
    if (popoverType == POPOVER_TYPE_TOPRIGHT)
        contentViewController.view.transform = CGAffineTransformMakeTranslation(screenWidth - topLeftPoint.x, -contentViewController.preferredContentSize.height);
    else if (popoverType == POPOVER_TYPE_BOTTOM)
        contentViewController.view.transform = CGAffineTransformMakeTranslation(0, screenHeight - topLeftPoint.y);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    tapView.alpha = 1.0;
    contentViewController.view.alpha = 1.0;
    if (popoverType == POPOVER_TYPE_TOPRIGHT)
        contentViewController.view.transform = CGAffineTransformIdentity;
    else if (popoverType == POPOVER_TYPE_BOTTOM)
        contentViewController.view.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

- (void)dismissPopover {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(onDismissFinished)];
    [UIView setAnimationDuration:animationDuration];
    tapView.alpha = 0.0;
    contentViewController.view.alpha = 0.0;
    if (popoverType == POPOVER_TYPE_TOPRIGHT)
        contentViewController.view.transform = CGAffineTransformMakeTranslation(screenWidth - topLeftPoint.x, -topLeftPoint.y - contentViewController.preferredContentSize.height);
    else if (popoverType == POPOVER_TYPE_BOTTOM)
        contentViewController.view.transform = CGAffineTransformMakeTranslation(0, screenHeight - topLeftPoint.y);
    [UIView commitAnimations];
}

- (void)onDismissFinished {
    [contentViewController.view removeFromSuperview];
    [shadowView removeFromSuperview];
    [outlineView removeFromSuperview];
    [tapView removeFromSuperview];

    tapView.alpha = 1.0;
    contentViewController.view.alpha = 1.0;

    showed = NO;

    if ([delegate respondsToSelector:@selector(popoverControllerDidDismissed:)]) {
        [delegate popoverControllerDidDismissed:self];
    }
}

- (void)onTapGesture:(UITapGestureRecognizer *)gesture {
    if (dismissedWhenTap)
        [self dismissPopover];
}

- (void)setBackColor:(UIColor *)backColor {
    tapView.backgroundColor = backColor;
}

- (void)setShowDarkBackground:(BOOL)_showDarkBackground {
    showDarkBackground = _showDarkBackground;
    darkBackgroundView.hidden = !showDarkBackground;
}

@end
