//
//  PopoverController.h
//  Version 1.1
//
//  Created by Frank J. on 3/13/13.
//  Copyright (c) 2013 Frank J.. All rights reserved.
//

#import <Foundation/Foundation.h>

#define POPOVER_TYPE_CENTER			0
#define POPOVER_TYPE_TOPRIGHT		1
#define POPOVER_TYPE_BOTTOM			2

@protocol PopoverControllerDelegate;

@interface PopoverController : NSObject {
	id<PopoverControllerDelegate> __weak delegate;
	UIView *tapView;
	UIImageView *darkBackgroundView;
	UIImageView *shadowView;
	UIImageView *outlineView;
	UIViewController *contentViewController;
	BOOL dismissedWhenTap;
	BOOL showed;
	BOOL roundCornerAndShadow;
	NSInteger popoverType;
	float animationDuration;
	BOOL showDarkBackground;

	CGPoint topLeftPoint;
}

@property (nonatomic, weak) id<PopoverControllerDelegate> delegate;
@property (nonatomic) BOOL dismissedWhenTap;
@property (nonatomic, readonly) BOOL showed;
@property (nonatomic) BOOL roundCornerAndShadow;
@property (nonatomic) NSInteger popoverType;
@property (nonatomic) float animationDuration;
@property (nonatomic) BOOL showDarkBackground;

- (id)initWithContentViewController:(UIViewController *)viewController;
- (void)presentPopoverFromPoint:(CGPoint)point inView:(UIView *)view;
- (void)dismissPopover;
- (void)setBackColor:(UIColor *)backColor;

@end

@protocol PopoverControllerDelegate <NSObject>

@optional
- (void)popoverControllerDidDismissed:(PopoverController *)popoverController;

@end
