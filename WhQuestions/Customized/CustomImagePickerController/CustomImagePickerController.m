//
//  CustomImagePickerController.m
//  WhQuesions
//
//  Created by Frank J. on 11/7/12.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "CustomImagePickerController.h"

@implementation CustomImagePickerController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

@end
