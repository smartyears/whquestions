//
//  CustomImagePickerController.h
//  WhQuesions
//
//  Created by Frank J. on 11/7/12.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomImagePickerController : UIImagePickerController

@end
