//
//  PopupViewController.m
//  Version 1.1
//
//  Created by Frank J. on 3/13/13.
//  Copyright (c) 2013 Frank J.. All rights reserved.
//

#import "PopupViewController.h"

@interface PopupViewController ()

@end

@implementation PopupViewController

@synthesize delegate;
@synthesize subview;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onCancelButton:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDoneButton:)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.preferredContentSize = subview.frame.size;

//    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;

    [self.view addSubview:subview];
}

- (void)onCancelButton:(id)sender {
    if ([delegate respondsToSelector:@selector(didClickCancel:)])
        [delegate didClickCancel:self];
}

- (void)onDoneButton:(id)sender {
    if ([delegate respondsToSelector:@selector(didClickDone:)])
        [delegate didClickDone:self];
}

@end
