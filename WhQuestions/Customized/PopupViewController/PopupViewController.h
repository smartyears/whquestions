//
//  PopupViewController.h
//  Version 1.1
//
//  Created by Frank J. on 3/13/13.
//  Copyright (c) 2013 Frank J.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PopupViewControllerDelegate;

@interface PopupViewController : UIViewController {
	id<PopupViewControllerDelegate> __weak delegate;
	UIView *__weak subview;
}

@property (nonatomic, weak) id<PopupViewControllerDelegate> delegate;
@property (nonatomic, weak) UIView *subview;

@end


@protocol PopupViewControllerDelegate <NSObject>

@optional
- (void)didClickDone:(PopupViewController *)popupViewController;
- (void)didClickCancel:(PopupViewController *)popupViewController;

@end
