//
//  CustomPDFRenderer.m
//  WhQuesions
//
//  Created by Frank J. on 11/6/12.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "CustomPDFRenderer.h"

@implementation CustomPDFRenderer

- (CGRect)paperRect {
	return CGRectMake(0, 0, REPORT_WIDTH, REPORT_HEIGHT);
}

- (CGRect)printableRect {
	return CGRectInset(self.paperRect, REPORT_MARGIN, REPORT_MARGIN);
}

- (void)printToPDF:(NSString *)filepath {
	bGenerating = YES;
	
	[[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
	
	UIGraphicsBeginPDFContextToFile(filepath, self.paperRect, nil);
	[self prepareForDrawingPages:NSMakeRange(0, 1)];
	CGRect bounds = self.paperRect;
	for (int i = 0; i < self.numberOfPages; i ++) {
		UIGraphicsBeginPDFPage();
		[self drawPageAtIndex:i inRect:bounds];
	}
	UIGraphicsEndPDFContext();
	
	bGenerating = NO;
}

@end
