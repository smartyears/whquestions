//
//  CustomPDFRenderer.h
//  WhQuesions
//
//  Created by Frank J. on 11/6/12.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomPDFRenderer : UIPrintPageRenderer {
	BOOL bGenerating;
}

- (void)printToPDF:(NSString *)filepath;

@end
