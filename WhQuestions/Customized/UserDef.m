//
//  UserDef.m
//  WhQuesions
//
//  Created by Frank J. on 2/25/13.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "UserDef.h"

NSString *exportFilePath;
NSString *tempImportDirectoryPath;
NSString *tempExportDirectoryPath;
NSString *syncFilePath;
NSString *reportPDFPath;
NSUserDefaults *defaults;
NSFileManager *fileManager;
CGFloat screenWidth;
CGFloat screenHeight;
CGFloat screenWidthRatio;
CGFloat screenHeightRatio;
NSDictionary *languageStringsDictionary;
#define IPAD_SMALL        568
#define IPAD_9_7_INCH        1024
#define IPAD_11_INCH        1194
#define IPAD_12_9_INCH        1366

#define IPhone_4_7_INCH        667 //iPhone 6, iPhone 6S, iPhone 7, iPhone 8, iPhone SE2
#define IPhone_5_5_INCH        736 //iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus, iPhone 8 Plus
#define IPhone_5_8_INCH        812 //iPhone X, iPhone XS, iPhone 11 Pro, iPhone 12 Mini
#define IPhone_6_1_INCH        844 //iPhone 12, iPhone 12 Pro
#define IPhone_6_5_INCH        896 //iPhone XR, iPhone 11, iPhone XS Max, iPhone 11 Pro Max,
#define IPhone_6_7_INCH        926 //iPhone 12 Pro Max
NSString *getStringWithKey(NSString *key) {
	NSInteger language = [defaults integerForKey:@"LANGUAGE"];
	
	NSDictionary *stringDictionary = [languageStringsDictionary objectForKey:key];
	NSString *resultString = nil;
	if (language == LANGUAGE_ENGLISH)
		resultString = [stringDictionary objectForKey:@"EN"];
	else if (language == LANGUAGE_PORTUGUESE)
		resultString = [stringDictionary objectForKey:@"PT"];
	else if (language == LANGUAGE_SPANISH)
		resultString = [stringDictionary objectForKey:@"SP"];
	else
		resultString = [stringDictionary objectForKey:@"FR"];

	if (resultString.length == 0)
		resultString = [stringDictionary objectForKey:@"EN"];
	
	return [resultString stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
}
