//
//  NSString+Folder.m
//  iCommaker
//
//  Created by Frank J. on 9/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "NSString+Folder.h"

static NSString *const KImageSource_CacheFolder = @"cache";
static NSString *const KDataFolderName = @"userheaderfiles";

static NSString *const KDataFolderNameInlibrary = @"privatedata";
static NSString *const KHasUsingLibraryFolder = @"KHasUsingLibraryFolder";



@implementation NSString(Folder)

+ (NSString *) dataFolder {
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dataFolder = [libraryPath stringByAppendingPathComponent:KDataFolderNameInlibrary];
    
    NSError* error = nil;
	if (![fileManager fileExistsAtPath:dataFolder]) {
		if(![fileManager createDirectoryAtPath:dataFolder 
			   withIntermediateDirectories:YES
								attributes:nil
									 error:&error])
        {
            NSLog(@"error:%@", [error localizedDescription]);
        }
	}
    
    if (![defaults boolForKey:KHasUsingLibraryFolder]) {
        //handle copy
		NSString*documentfolder = [NSString documentFolder];
        
        NSArray *subFiles = [documentfolder contents];
        for (NSString *file in subFiles) {
            if(![fileManager moveItemAtPath:[documentfolder stringByAppendingPathComponent:file]
                                     toPath:[dataFolder stringByAppendingPathComponent:file]
                                      error:&error])
            {
                NSLog(@"error:%@", [error localizedDescription]);
            }
        }
		
		[defaults setBool:YES forKey:KHasUsingLibraryFolder];
    }
	
	return dataFolder;
}
+ (NSString *) documentFolder {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}
+ (NSString *)folderPathWithName:(NSString *)name {
    return [[self dataFolder]  folderPathWithName:name];
}
+ (NSString *)headerFolderPath {
    return [[self folderPathWithName:KDataFolderName] folderPathWithName:KImageSource_CacheFolder];
	
}
- (NSString *)folderPathWithName:(NSString *)name {
    NSString *folder = [self stringByAppendingPathComponent:name];
	
	if (![fileManager fileExistsAtPath:folder]) {
		[fileManager createDirectoryAtPath:folder 
			   withIntermediateDirectories:YES
								attributes:nil
									 error:nil];
	}
	return folder;
}
- (NSArray *)contents {	
	NSArray *array = [fileManager contentsOfDirectoryAtPath:self error:nil];
	return array;
}
- (NSArray *)fileContents {
	BOOL isFolder = NO;
	NSArray *array = [self contents];
	NSMutableArray* arrayFileList = [NSMutableArray arrayWithCapacity:[array count]];
	for (NSString *filename in array) {
		if ([[filename pathExtension] isEqualToString:@"data"]) {
			if ([fileManager fileExistsAtPath:[[NSString documentFolder] stringByAppendingPathComponent:filename] isDirectory:&isFolder]) {
				if (!isFolder) {
					[arrayFileList addObject:filename];
				}
			}
		}
	}
	return arrayFileList;
}
- (BOOL)fileExists {
	BOOL exists = [fileManager fileExistsAtPath:self];
	return exists;
}
@end
