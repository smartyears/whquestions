//
//  NSString+Folder.h
//  iCommaker
//
//  Created by Frank J. on 9/8/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(Folder)
+ (NSString *) dataFolder;
+ (NSString *) documentFolder;
+ (NSString *)headerFolderPath;
- (NSString *)folderPathWithName:(NSString *)name;
- (NSArray *)contents;
- (NSArray *)fileContents;
- (BOOL)fileExists;
@end
