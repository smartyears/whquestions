//
//  CustomNavigationController.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
