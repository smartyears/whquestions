//
//  UserDef.h
//  WhQuesions
//
//  Created by Frank J. on 2/25/13.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "AppDelegate.h"

extern NSString *exportFilePath;
extern NSString *tempImportDirectoryPath;
extern NSString *tempExportDirectoryPath;
extern NSString *syncFilePath;
extern NSString *reportPDFPath;
extern NSUserDefaults *defaults;
extern NSFileManager *fileManager;
extern CGFloat screenWidth;
extern CGFloat screenHeight;
extern CGFloat screenWidthRatio;
extern CGFloat screenHeightRatio;
extern NSDictionary *languageStringsDictionary;

extern NSString *getStringWithKey(NSString *key);

#define TRC_APP_URL								@"https://itunes.apple.com/us/app/therapy-report-center/id590197451?ls=1&mt=8"

#define REPORT_WIDTH							731
#define REPORT_HEIGHT							946
#define REPORT_MARGIN							20

#define REPORT_GENERATED						@"REPORT_GENERATED"
#define RELOAD_STUDENTS							@"RELOAD_STUDENTS"

#define ACTIVITY_SPEAK							0
#define ACTIVITY_SELECT							1

#define LANGUAGE_ENGLISH						0
#define LANGUAGE_PORTUGUESE						1
#define LANGUAGE_SPANISH						2
#define LANGUAGE_FRENCH							3

#define REFRESH_LANGUAGE						@"REFRESH_LANGUAGE"

#define ARIAL_FONT								@"ArialMT"
#define ARIAL_BOLD_FONT							@"Arial-BoldMT"
#define CALIBRI_FONT							@"Calibri"
