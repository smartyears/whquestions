//
//  BackupManager.m
//  iCommaker
//
//  Created by Frank J. on 9/26/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "BackupManager.h"
#import "NSString+Folder.h"
#import "NSData+CocoaDevUsersAdditions.h"

@implementation BackupManager

+ (BOOL)backupDataWithFileName:(NSString *)filename {
	NSError *error;
    NSURL *url = [NSURL fileURLWithPath:[NSString dataFolder]];
    NSFileWrapper *dirWrapper = [[NSFileWrapper alloc] initWithURL:url options:0 error:&error];
    if (dirWrapper == nil) {
        NSLog(@"Error creating directory wrapper: %@", error.localizedDescription);
        return NO;
    }   
    
    NSData *dirData = [dirWrapper serializedRepresentation];
    NSData *gzData = [dirData gzipDeflate];  
	return [gzData writeToFile:[[NSString documentFolder] stringByAppendingPathComponent:filename] atomically:YES];
}

+ (BOOL)restoreDataWithFileName:(NSString *)filename {
	NSData *zippedData = [NSData dataWithContentsOfFile:[[NSString documentFolder] stringByAppendingPathComponent:filename]];

	NSData *unzippedData = [zippedData gzipInflate];                
    NSFileWrapper *dirWrapper = [[NSFileWrapper alloc] initWithSerializedRepresentation:unzippedData];
    if (dirWrapper == nil) {
        NSLog(@"Error creating dir wrapper from unzipped data");
        return NO;
    }
    
    // Calculate desired name
    NSURL *dirUrl = [NSURL fileURLWithPath:[NSString dataFolder]];
    NSError *error;
    BOOL success = [dirWrapper writeToURL:dirUrl options:NSFileWrapperWritingAtomic originalContentsURL:nil error:&error];
    if (!success) {
        NSLog(@"Error importing file: %@", error.localizedDescription);
        return NO;
    }

	return success;
}

@end
