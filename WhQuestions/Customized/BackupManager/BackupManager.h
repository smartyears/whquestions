//
//  BackupManager.h
//  iCommaker
//
//  Created by Frank J. on 9/26/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BackupManager : NSObject {

}

+ (BOOL)backupDataWithFileName:(NSString *)filename;
+ (BOOL)restoreDataWithFileName:(NSString *)filename;
@end
