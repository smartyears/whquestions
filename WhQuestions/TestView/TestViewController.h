//
//  TestViewController.h
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "CustomViewController.h"
#import "FXLabel.h"
#import <AVFoundation/AVFoundation.h>
#import "TestScrollView.h"
#import "PopoverController.h"

@interface TestViewController : CustomViewController <UIScrollViewDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate, PopoverControllerDelegate> {
	NSArray *studentsArray;
	NSInteger activityType;
	
	IBOutlet FXLabel *questionLabel;
    IBOutlet UIView *containerView;
	IBOutlet TestScrollView *contentScrollView;
	
	IBOutlet UIView *speakScoreView;
	IBOutlet UIButton *speakCorrectButton;
	IBOutlet UIButton *speakWrongButton;
	
	IBOutlet UIView *selectScoreView;
	IBOutlet UIButton *selectButton1;
	IBOutlet UIButton *selectButton2;
	IBOutlet UIButton *selectButton3;
	
	IBOutlet UIButton *recordButton;
	IBOutlet UIButton *playButton;
	
	IBOutlet UIView *recordingView;
	IBOutlet FXLabel *recordingLabel;
	
	NSInteger language;
	
	NSMutableArray *photoViewsArray;
	NSMutableArray *nameLabelsArray;
	NSMutableArray *starViewsArray;
	NSMutableArray *starCountsArray;

	NSInteger currentQuestionIndex;
	NSMutableArray *questionImageViewsArray;
	
	NSArray *selectButtonsArray;
	
	AVAudioRecorder *audioRecorder;
	AVAudioPlayer *audioPlayer;
	AVAudioPlayer *transitionAudioPlayer;
	
	BOOL shouldPlayAnswersAudio;
	NSInteger answerAudioIndex;
	NSString *answerAudiosPath;
	
	NSArray *folderNamesArray;
	
	CGRect imageRect;
	
	BOOL bAnswered;
	BOOL bFirstAnswered;
	
	UIImageView *animationImageView;
	NSInteger animationIndex;
	
	PopoverController *settingsPopoverController;
}

@property (nonatomic, strong) NSArray *studentsArray;
@property (nonatomic) NSInteger activityType;

- (IBAction)onHomeButton:(id)sender;
- (IBAction)onSettingsButton:(id)sender;
- (IBAction)onSpeakCorrectButton:(id)sender;
- (IBAction)onSpeakWrongButton:(id)sender;
- (IBAction)onSelectItemButton:(UIButton *)selectButton;
- (IBAction)onRecordButton:(id)sender;
- (IBAction)onPlayButton:(id)sender;

@end
