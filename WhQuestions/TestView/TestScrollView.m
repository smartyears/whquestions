//
//  TestScrollView.m
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "TestScrollView.h"

@implementation TestScrollView

@synthesize responseRect;

- (void)awakeFromNib {
    [super awakeFromNib];
	responseRect = self.frame;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
	CGPoint parentLocation = [self convertPoint:point toView:[self superview]];
	return CGRectContainsPoint(responseRect, parentLocation);
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	if ([self pointInside:point withEvent:event])
    {
		return [super hitTest:point withEvent:event];
	}
	
	return nil;
}

@end
