//
//  TestViewController.m
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "TestViewController.h"
#import "StudentEntity.h"
#import "SessionEntity.h"
#import "QuestionEntity.h"
#import "QuestionsManagement.h"
#import "WhQuestionsSetting.h"
#import "RecordingDAO.h"
#import "RecordingModel.h"
#import "NSString+Folder.h"
#import "TestSettingsViewController.h"

#define PHOTO_X_PAD			7
#define PHOTO_Y_PAD			7
#define PHOTO_WIDTH_PAD		129
#define PHOTO_HEIGHT_PAD	129

#define PHOTO_X_PHONE		3
#define PHOTO_Y_PHONE		3
#define PHOTO_WIDTH_PHONE	54
#define PHOTO_HEIGHT_PHONE	54

#define STAR_LABEL_TAG		100


#define IPAD_SMALL        568
#define IPAD_9_7_INCH        1024
#define IPAD_11_INCH        1194
#define IPAD_12_9_INCH        1366

#define IPhone_4_7_INCH        667 //iPhone 6, iPhone 6S, iPhone 7, iPhone 8, iPhone SE2
#define IPhone_5_5_INCH        736 //iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus, iPhone 8 Plus
#define IPhone_5_8_INCH        812 //iPhone X, iPhone XS, iPhone 11 Pro, iPhone 12 Mini
#define IPhone_6_1_INCH        844 //iPhone 12, iPhone 12 Pro
#define IPhone_6_5_INCH        896 //iPhone XR, iPhone 11, iPhone XS Max, iPhone 11 Pro Max,
#define IPhone_6_7_INCH        926 //iPhone 12 Pro Max
@interface TestViewController ()

@end

@implementation TestViewController
@synthesize studentsArray;
@synthesize activityType;

- (void)viewDidLoad{
    [super viewDidLoad];
    NSLog(@"screensize %f",screenWidth);
	language = [defaults integerForKey:@"LANGUAGE"];
	
	photoViewsArray = [[NSMutableArray alloc] init];
	nameLabelsArray = [[NSMutableArray alloc] init];
	starViewsArray = [[NSMutableArray alloc] init];
	starCountsArray = [[NSMutableArray alloc] init];
	
	for (NSInteger i = 0; i < studentsArray.count; i ++) {
		StudentEntity *student = [studentsArray objectAtIndex:i];
		
		NSInteger column = i % 2;
		NSInteger row = i / 2;
		
		UIImageView *photoView;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                photoView = [UIImageView.alloc initWithFrame:CGRectMake(800 + 167 * column, 110 + 180 * row, 164, 164)];
            }
            else
            {
                photoView = [UIImageView.alloc initWithFrame:CGRectMake(700 + 167 * column, 110 + 180 * row, 144, 144)];
            }
		}
		else
        {
            if (screenWidth >= IPhone_6_1_INCH)
            {
                photoView = [UIImageView.alloc initWithFrame:CGRectMake(325 + 90 * column, 45 + 75 * row, 60, 60)];
            }
            else if (screenWidth == IPhone_6_7_INCH)
            {
                photoView = [UIImageView.alloc initWithFrame:CGRectMake(295 + 90 * column, 45 + 75 * row, 60, 60)];
            }
            else if (screenWidth > IPhone_5_5_INCH && screenWidth < IPhone_6_1_INCH)
            {
                photoView = [UIImageView.alloc initWithFrame:CGRectMake(295 + 90 * column, 45 + 75 * row, 60, 60)];
            }
            else
            {
                photoView = [UIImageView.alloc initWithFrame:CGRectMake(285 + 90 * column, 45 + 75 * row, 60, 60)];
            }
            
            
//            if (screenWidth == 568)
//            {
//                photoView = [UIImageView.alloc initWithFrame:CGRectMake(330 + 110 * column, 45 + 75 * row, 60, 60)];
//            }
		}
		photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundPad.png"];
		photoView.tag = i;
		photoView.userInteractionEnabled = YES;
		
		UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
		[photoView addGestureRecognizer:tapGesture];

		UIImageView *photoImageView;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PAD, PHOTO_Y_PAD, PHOTO_WIDTH_PAD + 20, PHOTO_HEIGHT_PAD + 20)];
            }
            else
            {
                photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PAD, PHOTO_Y_PAD, PHOTO_WIDTH_PAD, PHOTO_HEIGHT_PAD)];
            }
            if (screenWidth >= IPAD_12_9_INCH)
            {
                speakScoreView.frame = CGRectMake(210, screenHeight - 350, 362, 70);
            
            }
        }
		else
        {
            
			photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PHONE, PHOTO_Y_PHONE, PHOTO_WIDTH_PHONE, PHOTO_HEIGHT_PHONE)];
        }
		photoImageView.contentMode = UIViewContentModeScaleAspectFill;
		if (IS_IPAD)
			photoImageView.layer.cornerRadius = 15;
		else
			photoImageView.layer.cornerRadius = 6;
		photoImageView.clipsToBounds = YES;
		photoImageView.image = [student headerImage];
		photoImageView.transform = CGAffineTransformMakeRotation(-1.6 * M_PI / 180.0);
		[photoView addSubview:photoImageView];
		
		[photoViewsArray addObject:photoView];
		
		FXLabel *nameLabel;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 38)];
            }
            else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
            {
                nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 38)];
            }
            else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
            {
                nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 38)];
            }
            else  if (screenWidth == 568)
            {
                nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 38)];
            }
			nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:30];
			nameLabel.strokeSize = 4;
		}
		else
        {
			nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 16)];
			nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:13];
			nameLabel.strokeSize = 2;
		}
		nameLabel.backgroundColor = [UIColor clearColor];
		nameLabel.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		nameLabel.strokeColor = [UIColor whiteColor];
		nameLabel.minimumScaleFactor = 0.1;
		nameLabel.adjustsFontSizeToFitWidth = YES;
		nameLabel.textAlignment = NSTextAlignmentCenter;
		nameLabel.oversampling = 4;
		nameLabel.text = [student name];
		
		[nameLabelsArray addObject:nameLabel];
		
		UIImageView *starView;
		if (IS_IPAD)
			starView = [UIImageView.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame) + 90, CGRectGetMinY(photoView.frame) + 34, 68, 63)];
		else
			starView = [UIImageView.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame) + 37, CGRectGetMinY(photoView.frame) + 14, 28, 26)];
		starView.image = [UIImage imageNamed:@"TestStarIcon.png"];
		
		FXLabel *label;
		if (IS_IPAD)
        {
			label = [FXLabel.alloc initWithFrame:CGRectMake(18, 18, 32, 38)];
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:30];
			label.strokeSize = 4;
		}
		else
        {
			label = [FXLabel.alloc initWithFrame:CGRectMake(7, 7, 14, 17)];
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:12];
			label.strokeSize = 2;
		}
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		label.strokeColor = [UIColor whiteColor];
        label.minimumScaleFactor = 0.1;
		label.adjustsFontSizeToFitWidth = YES;
		label.textAlignment = NSTextAlignmentCenter;
		label.oversampling = 4;
		label.tag = STAR_LABEL_TAG;
		label.text = @"0";
		[starView addSubview:label];
		starView.hidden = YES;
		
		[starViewsArray addObject:starView];
		
		[containerView insertSubview:photoView belowSubview:contentScrollView];
		[containerView insertSubview:nameLabel belowSubview:contentScrollView];
		[containerView insertSubview:starView belowSubview:contentScrollView];
		
		[starCountsArray addObject:[NSNumber numberWithInteger:0]];
	}
	
	if (IS_IPAD)
    {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            imageRect = CGRectMake(28, 0, 732, 815);
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            imageRect = CGRectMake(28, 0, 632, 660);
        }
        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
        {
            imageRect = CGRectMake(28, 0, 632, 590);
        }
        else if (screenWidth == 568)
        {
            imageRect = CGRectMake(28, 0, 432, 415);
        }
    }
	else
    {
        if (screenWidth >= IPhone_6_1_INCH)
        {
            imageRect = CGRectMake(11, 0, 300, 330);
        }
        else if (screenWidth == IPhone_6_7_INCH)
        {
            imageRect = CGRectMake(11, 0, 300, 330);
        }
        else if (screenWidth > IPhone_5_5_INCH && screenWidth < IPhone_6_1_INCH)
        {
            imageRect = CGRectMake(11, 0, 280, 274);
        }
        else
        {
            imageRect = CGRectMake(11, 0, 260, 264);
        }
    }
    contentScrollView.responseRect = CGRectMake(imageRect.origin.x, contentScrollView.frame.origin.y, imageRect.size.width, imageRect.size.height);
  //  contentScrollView.backgroundColor  = [UIColor yellowColor];
	questionLabel.strokeColor = [UIColor whiteColor];
	if (IS_IPAD)
		questionLabel.strokeSize = 6;
	else
		questionLabel.strokeSize = 2;
	questionLabel.characterSpacing = -0.1;
	questionLabel.oversampling = 4;
	questionLabel.userInteractionEnabled = YES;
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onQuestionTapGesture:)];
	[questionLabel addGestureRecognizer:tapGesture];
	
	selectButtonsArray = [NSArray arrayWithObjects:
						  selectButton1,
						  selectButton2,
						  selectButton3,
						  nil];
	

//    if (screenWidth > 568)
//    {
//        selectButton1.frame = CGRectMake(0, 0, 189, 55);
//        selectButton2.frame = CGRectMake(189, 0, 189, 55);
//        selectButton3.frame = CGRectMake(378, 0, 189, 55);
//    }
//    else
        if (screenWidth == 568)
        {
		selectButton1.frame = CGRectMake(0, 0, 189, 35);
		selectButton2.frame = CGRectMake(189, 0, 189, 35);
		selectButton3.frame = CGRectMake(378, 0, 189, 35);
	}
	
	for (UIButton *selectButton in selectButtonsArray)
    {
		selectButton.layer.shadowColor = [UIColor yellowColor].CGColor;
		selectButton.layer.shadowOffset = CGSizeMake(0, 0);
		selectButton.layer.shadowOpacity = 1.0;
		selectButton.layer.shadowRadius = 0;
		selectButton.layer.masksToBounds = NO;
	}
	
	if (activityType == ACTIVITY_SPEAK)
    {
		speakScoreView.alpha = 1.0;
		selectScoreView.alpha = 0.0;
	}
	else
    {
		speakScoreView.alpha = 0.0;
		selectScoreView.alpha = 1.0;
		
		[recordButton removeFromSuperview];
		[playButton removeFromSuperview];
	}
	
	recordingView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
	recordingLabel.characterSpacing = -0.1;
	recordingLabel.oversampling = 4;
	recordingLabel.text = getStringWithKey(RECORDING_STRING);
	
	folderNamesArray = [NSArray arrayWithObjects:
						@"Why",
						@"How",
						@"Where",
						@"Who",
						@"When",
						@"What",
						nil];

	currentQuestionIndex = -1;
	questionImageViewsArray = [[NSMutableArray alloc] init];
	
	answerAudiosPath = [NSBundle.mainBundle.resourcePath stringByAppendingPathComponent:@"GameData/AnswerAudios"];
	
	if (IS_IPAD)
		animationImageView = [UIImageView.alloc initWithFrame:CGRectMake(112, 35, 800, 698)];
	else
		animationImageView = [UIImageView.alloc initWithFrame:CGRectMake((screenWidth - 332) / 2, 15, 332, 290)];

	[self refreshSettings];
	[self nextQuestion];
}

- (void)viewWillAppear:(BOOL)animated {
	[APP_DELEGATE stopEffectAudio];
	[APP_DELEGATE stopBackgroundMusic];
}

- (void)viewWillDisappear:(BOOL)animated {
	[APP_DELEGATE playBackgroundMusic];
}

- (IBAction)onHomeButton:(id)sender {
	BOOL goBack = YES;
	for (StudentEntity *student in studentsArray) {
		QuestionsManagement *questionsManagement = (QuestionsManagement *)student.addtionalObject;
		if (![questionsManagement.currentSession isEmpty]) {
			[questionsManagement.currentSession save];
			goBack = NO;
		}
		student.addtionalObject = nil;
	}
	
	if (goBack) {
		[self.navigationController popViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popToRootViewControllerAnimated:YES];
	}
}

- (IBAction)onSettingsButton:(id)sender {
	TestSettingsViewController *settingsViewController;
	if (IS_IPAD)
		settingsViewController = [[TestSettingsViewController alloc] initWithNibName:@"TestSettingsViewControllerPad" bundle:nil];
	else
		settingsViewController = [[TestSettingsViewController alloc] initWithNibName:@"TestSettingsViewControllerPhone" bundle:nil];
	settingsPopoverController = [[PopoverController alloc] initWithContentViewController:settingsViewController];
	settingsPopoverController.delegate = self;
	
	if (IS_IPAD)
		[settingsPopoverController presentPopoverFromPoint:CGPointMake(screenWidth - 490, 70) inView:self.view];
	else
		[settingsPopoverController presentPopoverFromPoint:CGPointMake(screenWidth - 307, 35) inView:self.view];
}

- (IBAction)onSpeakCorrectButton:(id)sender {
	if (bAnswered)
		return;
	
	NSInteger studentIndex = [self getStudentIndex:currentQuestionIndex];
	StudentEntity *student = [studentsArray objectAtIndex:studentIndex];
	QuestionsManagement *questionsManagement = (QuestionsManagement *)student.addtionalObject;
	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	[questionsManagement.currentSession markCorrectForQuestion:question];

	if ([WhQuestionsSetting moveAutomatically])
		[self nextQuestion];
	
	[self playCorrectAudio];
	
	bAnswered = YES;
	
	if (bFirstAnswered) {
		NSInteger starCount = [[starCountsArray objectAtIndex:studentIndex] integerValue];
		starCount ++;
		[starCountsArray replaceObjectAtIndex:studentIndex withObject:[NSNumber numberWithInteger:starCount]];

		[self refreshStarView];
	}
	bFirstAnswered = NO;
}

- (IBAction)onSpeakWrongButton:(id)sender {
	if (bAnswered)
		return;
	
	NSInteger studentIndex = [self getStudentIndex:currentQuestionIndex];
	StudentEntity *student = [studentsArray objectAtIndex:studentIndex];
	QuestionsManagement *questionsManagement = (QuestionsManagement *)student.addtionalObject;
	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	[questionsManagement.currentSession markWrongForQuestion:question];

	if ([WhQuestionsSetting moveAutomatically])
		[self nextQuestion];

	[self playWrongAudio];
	
	bAnswered = YES;
	bFirstAnswered = NO;
}

- (IBAction)onSelectItemButton:(UIButton *)selectButton {
	if (bAnswered)
		return;
	
	NSInteger studentIndex = [self getStudentIndex:currentQuestionIndex];
	StudentEntity *student = [studentsArray objectAtIndex:studentIndex];
	QuestionsManagement *questionsManagement = (QuestionsManagement *)student.addtionalObject;
	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	if (selectButton.tag == 0) {
		[questionsManagement.currentSession markCorrectForQuestion:question];

		[self playCorrectAudio];
		
		bAnswered = YES;
		
		if (bFirstAnswered) {
			NSInteger starCount = [[starCountsArray objectAtIndex:studentIndex] integerValue];
			starCount ++;
			[starCountsArray replaceObjectAtIndex:studentIndex withObject:[NSNumber numberWithInteger:starCount]];
			
			[self refreshStarView];
		}
		bFirstAnswered = NO;
		
		[self showCorrectAnimation];
	}
	else {
		[questionsManagement.currentSession markWrongForQuestion:question];
		selectButton.hidden = YES;
		
		bFirstAnswered = NO;
	}
}

- (void)showCorrectAnimation {
	self.view.userInteractionEnabled = NO;
    [containerView addSubview:animationImageView];
	animationIndex = -1;
	
	[self onAnimationFunction];
}

- (void)onAnimationFunction {
	animationIndex ++;
	if (animationIndex < 7) {
		animationImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ActivityAnimation%ld", (long)animationIndex + 1]];
		
		[self performSelector:@selector(onAnimationFunction) withObject:nil afterDelay:0.15];
	}
	else {
		[animationImageView removeFromSuperview];
		self.view.userInteractionEnabled = YES;
		
		if ([WhQuestionsSetting moveAutomatically])
			[self nextQuestion];
	}
}

- (IBAction)onRecordButton:(UIButton *)sender {
	if (audioPlayer) {
		[playButton setBackgroundImage:[UIImage imageNamed:@"TestPlayButtonPad.png"] forState:UIControlStateNormal];
		playButton.tag = 0;
		[audioPlayer stop];
		audioPlayer = nil;
	}
	
	if (sender.tag == 0) {
		QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
		SessionEntity *session = [self getCurrentSession];

		RecordingDAO *recordingDAO = [RecordingDAO recordingDAO];
		if ([recordingDAO isRecordingPresentForSS_ID:session.identifier forWord:question.identifier]) {
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(OVERWRITE_STRING) message:getStringWithKey(RECORDING_OVERWRITE_CONFIRM_STRING) preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(NO_STRING) style:UIAlertActionStyleCancel handler:nil]];
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(YES_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self startRecording:YES];
            }]];
            [self presentViewController:alertController animated:true completion:nil];
		}
		else {
			recordButton.userInteractionEnabled = NO;
			[self performSelector:@selector(startRecording) withObject:nil afterDelay:0.5];
		}
	}
	else {
		recordButton.tag = 0;
		[self stopRecording];
	}
}

- (void)startRecording:(BOOL)override {
	if (override) {
		QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
		SessionEntity *session = [self getCurrentSession];
		
		RecordingDAO *recordingDAO = [RecordingDAO recordingDAO];
		[recordingDAO deleteRecordingPresentForSS_ID:session.identifier forWord:question.identifier];
	}
	
	[self startRecording];
}

- (void)startRecording {
	[recordButton setBackgroundImage:[UIImage imageNamed:@"TestStopButtonPad.png"] forState:UIControlStateNormal];
	recordButton.tag = 1;
	
	if (audioPlayer) {
		[playButton setBackgroundImage:[UIImage imageNamed:@"TestPlayButtonPad.png"] forState:UIControlStateNormal];
		playButton.tag = 0;
		[audioPlayer stop];
		audioPlayer = nil;
	}

	NSDictionary *dictRecorderSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
										  [NSNumber numberWithFloat:44100.0], AVSampleRateKey,
										  [NSNumber numberWithInteger:kAudioFormatAppleLossless], AVFormatIDKey,
										  [NSNumber numberWithInteger:1], AVNumberOfChannelsKey,
										  [NSNumber numberWithInteger:AVAudioQualityMax], AVEncoderAudioQualityKey,
										  nil];
	
	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	SessionEntity *session = [self getCurrentSession];

	NSString *path = [[NSString dataFolder] stringByAppendingPathComponent:[NSString stringWithFormat:@"SS%ld-Word%ld.caf", (long)session.identifier, (long)question.identifier]];
	
	RecordingDAO *recordingDAO = [RecordingDAO recordingDAO];
	
	RecordingModel *recording = [[RecordingModel alloc] init];
	recording.recording_id = [recordingDAO getMaxRecordingID] + 1;
	recording.ss_id = session.identifier;
	recording.word_id = question.identifier;
	recording.path = path;
	
	[recordingDAO insertRecording:recording];
	
	audioRecorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:path] settings:dictRecorderSettings error:nil];
	[audioRecorder setDelegate:self];
	[audioRecorder prepareToRecord];
	[audioRecorder record];
	recordButton.userInteractionEnabled=YES;
	
	[self performSelector:@selector(showRecordingLabel) withObject:nil];
}

- (void)stopRecording {
	playButton.alpha = 1.0;
	[recordButton setBackgroundImage:[UIImage imageNamed:@"TestRecordButtonPad.png"] forState:UIControlStateNormal];
	[audioRecorder stop];
	audioRecorder = nil;
	
	[self hideRecordingLabel];
}

- (void)showRecordingLabel {
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRecordingLabel) object:nil];
	if (!audioRecorder.recording) {
		recordingLabel.alpha = 0.0;
		[recordingView removeFromSuperview];
		return;
	}
	
	if (recordingView.superview == nil) {
		[self.view insertSubview:recordingView belowSubview:recordButton];
	}
	
	if (recordingLabel.alpha < 1.0) {
		[UIView animateWithDuration:0.5
							  delay:0.5
							options:UIViewAnimationOptionTransitionNone
						 animations:^(void) {
                             self->recordingLabel.alpha = 1.0;
						 }
						 completion:^(BOOL finished) {
                             if (!self->audioRecorder.recording) {
                                 self->recordingLabel.alpha = 0.0;
							 }
							 else {
								 [self performSelector:@selector(showRecordingLabel) withObject:nil];
							 }
						 }];
	}
	else {
		[UIView animateWithDuration:0.5
							  delay:0.5
							options:UIViewAnimationOptionTransitionNone
						 animations:^(void) {
                             self->recordingLabel.alpha = 0.0;
						 }
						 completion:^(BOOL finished) {
                             if (!self->audioRecorder.recording) {
                                 self->recordingLabel.alpha = 0.0;
							 }
							 else {
								 [self performSelector:@selector(showRecordingLabel) withObject:nil];
							 }
						 }];
	}
}

- (void)hideRecordingLabel {
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRecordingLabel) object:nil];
	recordingLabel.alpha = 0.0;
	[recordingView removeFromSuperview];
}

- (IBAction)onPlayButton:(UIButton *)sender {
	if (sender.tag == 0) {
		[playButton setBackgroundImage:[UIImage imageNamed:@"TestStopButtonPad.png"] forState:UIControlStateNormal];
		playButton.tag = 1;
		
		if (audioPlayer) {
			[playButton setBackgroundImage:[UIImage imageNamed:@"TestPlayButtonPad.png"] forState:UIControlStateNormal];
			playButton.tag = 0;
			[audioPlayer stop];
			audioPlayer = nil;
		}

		QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
		SessionEntity *session = [self getCurrentSession];
		
		RecordingDAO *recordingDAO = [RecordingDAO recordingDAO];
		
		RecordingModel *recording = [recordingDAO getRecordingForSS_ID:session.identifier forWord:question.identifier];
		
		if (recording.path != nil) {
			NSString *path = [[NSString dataFolder] stringByAppendingPathComponent:[recording.path lastPathComponent]];
			
			NSURL *fileURL = [NSURL fileURLWithPath:path];
			if (fileURL != nil) {
				audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
				audioPlayer.delegate = self;
				[audioPlayer play];
				
				recordButton.userInteractionEnabled = NO;
				
				return;
			}
			
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Sound does not exist." preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
		}
	}
	else {
		if (audioPlayer) {
			[playButton setBackgroundImage:[UIImage imageNamed:@"TestPlayButtonPad.png"] forState:UIControlStateNormal];
			playButton.tag = 0;
			[audioPlayer stop];
			audioPlayer = nil;
		}
		
		recordButton.userInteractionEnabled = YES;
	}
}

- (void)playQuestionAudio {
	if ([WhQuestionsSetting speakQuestions]) {
		if (audioPlayer) {
			[playButton setBackgroundImage:[UIImage imageNamed:@"TestPlayButtonPad.png"] forState:UIControlStateNormal];
			playButton.tag = 0;
			[audioPlayer stop];
			audioPlayer = nil;
		}
		
		QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
		
		NSString *newAudioFile;
		if (language == LANGUAGE_ENGLISH)
			newAudioFile = [NSBundle.mainBundle.resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"GameData/Audios/EN/%@/%@", [folderNamesArray objectAtIndex:question.type - 1], question.sound]];
		else if (language == LANGUAGE_PORTUGUESE)
			newAudioFile = [NSBundle.mainBundle.resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"GameData/Audios/PT/%@/%@", [folderNamesArray objectAtIndex:question.type - 1], question.sound]];
		else if (language == LANGUAGE_SPANISH)
			newAudioFile = [NSBundle.mainBundle.resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"GameData/Audios/SP/%@/%@", [folderNamesArray objectAtIndex:question.type - 1], question.sound]];
		else
			newAudioFile = [NSBundle.mainBundle.resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"GameData/Audios/FR/%@/%@", [folderNamesArray objectAtIndex:question.type - 1], question.sound]];
		
		if (newAudioFile == nil) {
			[self playAnswerAudio];
			return;
		}
		
		audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:newAudioFile] error:nil];
		audioPlayer.delegate = self;
		[audioPlayer play];
	}
}

- (void)refreshPhotos {
	NSInteger currentStudentIndex = [self getStudentIndex:currentQuestionIndex];
	
	for (NSInteger i = 0; i < photoViewsArray.count; i ++) {
		UIImageView *photoView = [photoViewsArray objectAtIndex:i];
		FXLabel *nameLabel = [nameLabelsArray objectAtIndex:i];
		
		if (i == currentStudentIndex) {
			photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundSelectedPad.png"];
			nameLabel.textColor = [UIColor colorWithRed:208 / 255.0 green:91 / 255.0 blue:39 / 255.0 alpha:1.0];
			nameLabel.strokeColor = [UIColor colorWithRed:117 / 255.0 green:47 / 255.0 blue:47 / 255.0 alpha:1.0];
		}
		else {
			photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundPad.png"];
			nameLabel.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
			nameLabel.strokeColor = [UIColor whiteColor];
		}
	}
}

- (UIImageView *)getQuestionImageViewWithQuestion:(QuestionEntity *)question {
	UIImageView *questionImageView = [UIImageView.alloc initWithFrame:imageRect];
	questionImageView.image = [UIImage imageNamed:@"TestContentBackgroundPad.png"];
	questionImageView.userInteractionEnabled = YES;
	
	UIImageView *imageView;
	if (IS_IPAD)
    {
        //ipad 12.9 inch
        if (screenWidth >= IPAD_12_9_INCH)
        {
            //inner big image
            //imageRect = CGRectMake(28, 0, 732, 815);
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(40, 40, 652, 735)];
            //[imageView setBackgroundColor:UIColor.yellowColor];
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(40, 40, 552, 535)];
        }
        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
        {
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(40, 40, 552, 535)];
        }
        else if (screenWidth == 568)
        {
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(40, 40, 552, 535)];
        }
    }
	else
    {
        if (screenWidth >= IPhone_6_1_INCH)
        {
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(15, 15, 270, 264)];
        }
        else if (screenWidth > IPhone_5_5_INCH && screenWidth < IPhone_6_1_INCH)
        {
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(15, 15, 240, 234)];
        }
        else
        {
            imageView = [UIImageView.alloc initWithFrame:CGRectMake(15, 15, 210, 204)];
        }
    
    }
		
    imageView.image = [UIImage imageWithContentsOfFile:[NSBundle.mainBundle.resourcePath stringByAppendingPathComponent:[NSString stringWithFormat:@"GameData/Images/%@/%@", [folderNamesArray objectAtIndex:question.type - 1], question.image]]];
	imageView.contentMode = UIViewContentModeScaleAspectFit;
	imageView.userInteractionEnabled = YES;
	[questionImageView addSubview:imageView];
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onImageTapGesture:)];
	[imageView addGestureRecognizer:tapGesture];
	
	return questionImageView;
}

- (void)onImageTapGesture:(UITapGestureRecognizer *)gesture {
	[self stopAnswerAudio];
	[self playQuestionAudio];
}

- (NSInteger)getStudentIndex:(NSInteger)questionIndex {
	return (questionIndex / [WhQuestionsSetting numberOfQuestionsPerStudent]) % studentsArray.count;
}

- (SessionEntity *)getCurrentSession {
	NSInteger studentIndex = [self getStudentIndex:currentQuestionIndex];
	StudentEntity *student = [studentsArray objectAtIndex:studentIndex];
	QuestionsManagement *questionsManagement = (QuestionsManagement *)student.addtionalObject;
	return questionsManagement.currentSession;
}

- (QuestionEntity *)getQuestionWithIndex:(NSInteger)questionIndex {
	NSInteger studentIndex = [self getStudentIndex:questionIndex];
	StudentEntity *student = [studentsArray objectAtIndex:studentIndex];
	QuestionsManagement *questionsManagement = (QuestionsManagement *)student.addtionalObject;
	NSInteger index = questionIndex / studentsArray.count + (questionIndex + 1) % [WhQuestionsSetting numberOfQuestionsPerStudent];
	
	return [questionsManagement questionentityForIndex:index];
}

- (void)nextQuestion {
	currentQuestionIndex ++;
	if (currentQuestionIndex == questionImageViewsArray.count) {
		[questionImageViewsArray addObject:@""];
	}
	
	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	UIImageView *questionImageView = [questionImageViewsArray objectAtIndex:currentQuestionIndex];
	if (![questionImageView isKindOfClass:[UIImageView class]]) {
		questionImageView = [self getQuestionImageViewWithQuestion:question];
		
		CGRect frame = questionImageView.frame;
		frame.origin.x = contentScrollView.frame.size.width * currentQuestionIndex + imageRect.origin.x;
		questionImageView.frame = frame;
        //[questionImageView setBackgroundColor:UIColor.redColor];
		[contentScrollView addSubview:questionImageView];
		[questionImageViewsArray removeObjectAtIndex:currentQuestionIndex];
		[questionImageViewsArray insertObject:questionImageView atIndex:currentQuestionIndex];
	}
	
	if (questionImageViewsArray.count == 1) {
		[self refreshPhotos];
		[self refreshViews];
	}
	else
    {
		self.view.userInteractionEnabled = NO;
	}
	
	contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width * (questionImageViewsArray.count + 1), contentScrollView.frame.size.height);
	[contentScrollView scrollRectToVisible:CGRectMake(contentScrollView.frame.size.width * currentQuestionIndex, 0, contentScrollView.frame.size.width, contentScrollView.frame.size.height) animated:YES];
}

// UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[self stopAnswerAudio];
	
	if (!questionLabel.hidden) {
		questionLabel.hidden = YES;
		recordButton.hidden = YES;
		playButton.hidden = YES;
		speakScoreView.hidden = YES;
		selectScoreView.hidden = YES;
	}
	
	NSInteger offsetX = contentScrollView.contentOffset.x;
	if (offsetX < 0)
		return;
	
	NSInteger tempQuestionIndex = currentQuestionIndex;
	offsetX -= currentQuestionIndex * contentScrollView.frame.size.width;
	if (offsetX < 0) {
		offsetX += contentScrollView.frame.size.width;
		if (offsetX <= imageRect.origin.x + imageRect.size.width) {
			tempQuestionIndex = currentQuestionIndex - 1;
		}
	}
	else {
		if (offsetX >= imageRect.origin.x) {
			tempQuestionIndex = currentQuestionIndex + 1;
		}
	}
	
	if (tempQuestionIndex < questionImageViewsArray.count) {
		UIImageView *questionImageView = [questionImageViewsArray objectAtIndex:tempQuestionIndex];
		if ([questionImageView isKindOfClass:[UIImageView class]]) {
			return;
		}
	}
	
	if (tempQuestionIndex == questionImageViewsArray.count) {
		[questionImageViewsArray addObject:@""];
	}
	
	QuestionEntity *question = [self getQuestionWithIndex:tempQuestionIndex];
	UIImageView *questionImageView = [self getQuestionImageViewWithQuestion:question];
	
	CGRect frame = questionImageView.frame;
	frame.origin.x = contentScrollView.frame.size.width * tempQuestionIndex + imageRect.origin.x;
	questionImageView.frame = frame;

	[contentScrollView addSubview:questionImageView];
	[questionImageViewsArray removeObjectAtIndex:tempQuestionIndex];
	[questionImageViewsArray insertObject:questionImageView atIndex:tempQuestionIndex];
	
	contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width * (questionImageViewsArray.count + 1), contentScrollView.frame.size.height);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	self.view.userInteractionEnabled = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (!decelerate) {
		[self refreshViews];
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	[self refreshViews];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	[self refreshViews];
}

- (void)refreshViews {
	self.view.userInteractionEnabled = YES;
	
	if (questionLabel.hidden) {
		questionLabel.hidden = NO;
		recordButton.hidden = NO;
		playButton.hidden = NO;
		speakScoreView.hidden = NO;
		selectScoreView.hidden = NO;
	}
	
	NSInteger tempQuestionIndex = contentScrollView.contentOffset.x / contentScrollView.frame.size.width;
	if (tempQuestionIndex > questionImageViewsArray.count - 1)
		return;
	
	currentQuestionIndex = tempQuestionIndex;
	contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width * (questionImageViewsArray.count + 1), contentScrollView.frame.size.height);

	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	
	if (language == LANGUAGE_ENGLISH)
		questionLabel.text = question.questionEN;
	else if (language == LANGUAGE_PORTUGUESE)
		questionLabel.text = question.questionPT;
	else if (language == LANGUAGE_SPANISH)
		questionLabel.text = question.questionSP;
	else
		questionLabel.text = question.questionFR;

	answerAudioIndex = -1;
	shouldPlayAnswersAudio = YES;
	[self playQuestionAudio];
	
	if (activityType == ACTIVITY_SELECT) {
		NSMutableArray *tagsArray = [[NSMutableArray alloc] init];
		for (NSInteger i = 0; i < selectButtonsArray.count; i ++) {
			[tagsArray addObject:[NSNumber numberWithInteger:i]];
		}
		
		for (NSInteger i = 0; i < 10; i ++) {
			NSInteger index1 = arc4random() % selectButtonsArray.count;
			NSInteger index2 = arc4random() % selectButtonsArray.count;
			if (index1 == index2)
				continue;
			
			if (index1 > index2) {
				NSInteger a = index1;
				index1 = index2;
				index2 = a;
			}
			
			NSNumber *value1 = [tagsArray objectAtIndex:index1];
			NSNumber *value2 = [tagsArray objectAtIndex:index2];
			[tagsArray removeObjectAtIndex:index2];
			[tagsArray removeObjectAtIndex:index1];
			[tagsArray insertObject:value2 atIndex:index1];
			[tagsArray insertObject:value1 atIndex:index2];
		}
		
		for (NSInteger i = 0; i < selectButtonsArray.count; i ++) {
			UIButton *selectButton = [selectButtonsArray objectAtIndex:i];
			NSInteger tag = [[tagsArray objectAtIndex:i] integerValue];
			selectButton.tag = tag;
			selectButton.hidden = NO;
			if (tag == 0) {
				if (language == LANGUAGE_ENGLISH)
					[selectButton setImage:[self getSelectButtonImage:question.correctEN] forState:UIControlStateNormal];
				else if (language == LANGUAGE_PORTUGUESE)
					[selectButton setImage:[self getSelectButtonImage:question.correctPT] forState:UIControlStateNormal];
				else if (language == LANGUAGE_SPANISH)
					[selectButton setImage:[self getSelectButtonImage:question.correctSP] forState:UIControlStateNormal];
				else
					[selectButton setImage:[self getSelectButtonImage:question.correctFR] forState:UIControlStateNormal];
			}
			else if (tag == 1) {
				if (language == LANGUAGE_ENGLISH)
					[selectButton setImage:[self getSelectButtonImage:question.wrong1EN] forState:UIControlStateNormal];
				else if (language == LANGUAGE_PORTUGUESE)
					[selectButton setImage:[self getSelectButtonImage:question.wrong1PT] forState:UIControlStateNormal];
				else if (language == LANGUAGE_SPANISH)
					[selectButton setImage:[self getSelectButtonImage:question.wrong1SP] forState:UIControlStateNormal];
				else
					[selectButton setImage:[self getSelectButtonImage:question.wrong1FR] forState:UIControlStateNormal];
			}
			else {
				if (language == LANGUAGE_ENGLISH)
					[selectButton setImage:[self getSelectButtonImage:question.wrong2EN] forState:UIControlStateNormal];
				else if (language == LANGUAGE_PORTUGUESE)
					[selectButton setImage:[self getSelectButtonImage:question.wrong2PT] forState:UIControlStateNormal];
				else if (language == LANGUAGE_SPANISH)
					[selectButton setImage:[self getSelectButtonImage:question.wrong2SP] forState:UIControlStateNormal];
				else
					[selectButton setImage:[self getSelectButtonImage:question.wrong2FR] forState:UIControlStateNormal];
			}
		}
	}
	
	NSInteger count = questionImageViewsArray.count;
	for (NSInteger i = 0; i < count; i ++) {
		UIImageView *questionImageView = [questionImageViewsArray objectAtIndex:i];
		if ((i != currentQuestionIndex) && ([questionImageView isKindOfClass:[UIImageView class]])) {
			[questionImageView removeFromSuperview];
			[questionImageViewsArray removeObjectAtIndex:i];
			[questionImageViewsArray insertObject:@"" atIndex:i];
		}
	}
	
	[self refreshPhotos];
	
	SessionEntity *session = [self getCurrentSession];
	
	RecordingDAO *recordingDAO = [RecordingDAO recordingDAO];
	
	if (![recordingDAO isRecordingPresentForSS_ID:session.identifier forWord:question.identifier]) {
		playButton.alpha = 0.0;
	}
	else {
		playButton.alpha = 1.0;
	}
	
	bAnswered = NO;
	bFirstAnswered = YES;
}

- (UIImage *)getSelectButtonImage:(NSString *)title {
	CGRect frame;
	if (IS_IPAD)
		frame = CGRectMake(0, 0, 322, 50);
	else {
		if (screenWidth == 568)
			frame = CGRectMake(0, 0, 179, 25);
		else
			frame = CGRectMake(0, 0, 150, 25);
	}
	
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:247 / 255.0 green:245 / 255.0 blue:245 / 255.0 alpha:1.0];
	label.numberOfLines = 0;
	label.textAlignment = NSTextAlignmentCenter;
	label.text = title;
	
	NSInteger fontSize;
	if (IS_IPAD)
		fontSize = 28;
	else
		fontSize = 14;
	while (YES) {
		label.frame = CGRectMake(0, 0, frame.size.width, MAXFLOAT);
		label.font = [UIFont fontWithName:ARIAL_FONT size:fontSize];
		[label sizeToFit];
		
		if (fontSize == 1 || label.frame.size.height <= frame.size.height) {
			label.frame = frame;
			break;
		}
		else {
			fontSize --;
		}
	}
	
	UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0);
	[label.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

- (void)playCorrectAudio {
	if ([WhQuestionsSetting transitionSounds]) {
		NSString *filePath = [NSBundle.mainBundle pathForResource:@"correct" ofType:@"mp3"];
		
		transitionAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:filePath] error:nil];
		transitionAudioPlayer.delegate = self;
		[transitionAudioPlayer play];
	}
}

- (void)playWrongAudio {
	if ([WhQuestionsSetting transitionSounds]) {
		NSString *filePath = [NSBundle.mainBundle pathForResource:@"wrong" ofType:@"mp3"];
		
		transitionAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:filePath] error:nil];
		transitionAudioPlayer.delegate = self;
		[transitionAudioPlayer play];
	}
}

- (void)onTapGesture:(UITapGestureRecognizer *)gesture {
	NSInteger studentIndex = [self getStudentIndex:currentQuestionIndex];
	if (studentIndex == gesture.view.tag) {
		[self stopAnswerAudio];
		[self playQuestionAudio];
	}
}

- (void)onQuestionTapGesture:(UITapGestureRecognizer *)gesture {
	[self stopAnswerAudio];
	[self playQuestionAudio];
}

- (void)refreshStarView {
	NSInteger currentStudentIndex = [self getStudentIndex:currentQuestionIndex];
	NSInteger starCount = [[starCountsArray objectAtIndex:currentStudentIndex] integerValue];
	UIImageView *starView = [starViewsArray objectAtIndex:currentStudentIndex];
	if (starCount == 0) {
		starView.hidden = YES;
	}
	else {
		starView.hidden = NO;
		FXLabel *label = (FXLabel *)[starView viewWithTag:STAR_LABEL_TAG];
		label.text = [NSString stringWithFormat:@"%ld", (long)starCount];
	}
}

// PopoverControllerDelegate
- (void)popoverControllerDidDismissed:(PopoverController *)popoverController {
	if (popoverController == settingsPopoverController) {
		settingsPopoverController = nil;
		
		[self refreshSettings];
	}
}

- (void)refreshSettings {
	if ([WhQuestionsSetting displayWrittenQuestions])
		questionLabel.alpha = 1.0;
	else
		questionLabel.alpha = 0.0;
}

// AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	[self onDidFinishPlaying:player];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
	[self onDidFinishPlaying:player];
}

- (void)onDidFinishPlaying:(AVAudioPlayer *)player {
	if (player == audioPlayer) {
		[playButton setBackgroundImage:[UIImage imageNamed:@"TestPlayButtonPad.png"] forState:UIControlStateNormal];
		playButton.tag = 0;
		[audioPlayer stop];
		audioPlayer = nil;
		
		recordButton.userInteractionEnabled = YES;
		
		if (shouldPlayAnswersAudio) {
			[self playAnswerAudio];
		}
	}
	else if (player == transitionAudioPlayer) {
		transitionAudioPlayer = nil;
	}
}

- (void)playAnswerAudio {
	if (activityType == ACTIVITY_SPEAK || language != LANGUAGE_ENGLISH || shouldPlayAnswersAudio == NO) {
		[self stopAnswerAudio];
		return;
	}
	
	answerAudioIndex ++;
	if (answerAudioIndex == 3) {
		[self stopAnswerAudio];
		return;
	}
	
	QuestionEntity *question = [self getQuestionWithIndex:currentQuestionIndex];
	UIButton *selectButton = [selectButtonsArray objectAtIndexedSubscript:answerAudioIndex];
	NSString *filename;
	if (selectButton.tag == 0)
		filename = question.correctEN;
	else if (selectButton.tag == 1)
		filename = question.wrong1EN;
	else
		filename = question.wrong2EN;
	if ([filename hasSuffix:@"."] || [filename hasSuffix:@" "])
		filename = [filename substringToIndex:filename.length - 1];
	NSString *audioPath = [answerAudiosPath stringByAppendingPathComponent:[filename stringByAppendingPathExtension:@"mp3"]];
	if ([fileManager fileExistsAtPath:audioPath]) {
		audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:audioPath] error:nil];
		audioPlayer.delegate = self;
		[audioPlayer play];
		
		for (NSInteger i = 0; i < selectButtonsArray.count; i ++) {
			selectButton = [selectButtonsArray objectAtIndex:i];
			if (i == answerAudioIndex) {
				selectButton.layer.shadowRadius = 15;
			}
			else {
				selectButton.layer.shadowRadius = 0;
			}
		}
	}
	else {
		[self playAnswerAudio];
	}
}

- (void)stopAnswerAudio {
	if (audioPlayer) {
		[audioPlayer stop];
		audioPlayer = nil;
	}
	
	shouldPlayAnswersAudio = NO;
	
	for (UIButton *selectButton in selectButtonsArray) {
		selectButton.layer.shadowRadius = 0;
	}
}

@end
