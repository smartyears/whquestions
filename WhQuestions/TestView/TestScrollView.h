//
//  TestScrollView.h
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestScrollView : UIScrollView {
	CGRect responseRect;
}

@property (nonatomic) CGRect responseRect;

@end
