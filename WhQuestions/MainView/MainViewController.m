//
//  MainViewController.m
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "MainViewController.h"
#import "FXLabel.h"
#import "FMDatabase+SharedInstance.h"
#import "BackupManager.h"
#import "NSString+Folder.h"
#import "StudentsViewController.h"
#import "ReportViewController.h"
#import "SupportViewController.h"
#import "RequestConfirmViewController.h"
#import "SettingsViewController.h"
#import "SelectLanguageViewController.h"
#import "AppCenterViewController.h"
#import "BackupManagementVC.h"
#import "PopupViewController.h"
#import "PopoverController.h"
#import <MessageUI/MessageUI.h>
#import "RestrictedPopupViewController.h"
#import "SplashView.h"

#import "XCDYouTubeKit.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

#define BACKUP_ALERT_TAG				1000
#define RESTORE_ALERT_TAG				1001
#define SUPPORT_ALERT_TAG				1002

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate, PopupViewControllerDelegate, PopoverControllerDelegate, MFMailComposeViewControllerDelegate, RestrictedPopupViewControllerDelegate, SplashViewDelegate>
{
    __weak IBOutlet UIImageView *backgroundView;
    __weak IBOutlet UIImageView *bottomBarView;
    __weak IBOutlet UIButton *backupButton;
    __weak IBOutlet UIButton *restoreButton;
    __weak IBOutlet UIButton *resultsButton;
    __weak IBOutlet UIButton *newGameButton;
    __weak IBOutlet UIButton *settingsButton;
    __weak IBOutlet UIButton *supportButton;
    __weak IBOutlet UIButton *soundButton;

    IBOutlet UITableView *restoreTableView;
    NSMutableArray *restoreArray;
    NSInteger restoreIndex;

    PopoverController *supportPopoverController;
    PopoverController *requestConfirmPopoverController;
    PopoverController *settingsPopoverController;
    PopoverController *selectLanguagePopoverController;

    RestrictedPopupViewController *appCenterRestrictedPopupViewController;
    RestrictedPopupViewController *supportRestrictedPopupViewController;
    RestrictedPopupViewController *backupRestrictedPopupViewController;
    RestrictedPopupViewController *restoreRestrictedPopupViewController;

    BackupManagementVC *backupManagementVC;
}
@end

@implementation MainViewController

- (void)dealloc {
    [NSNotificationCenter.defaultCenter removeObserver:self name:REFRESH_LANGUAGE object:nil];
    [backupManagementVC removeFromParentViewController];
    [backupManagementVC.view removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!IS_IPAD) {
        bottomBarView.image = [[UIImage imageNamed:@"MainBottomBarPhone.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 120, 0, 330)];
    }
    
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(refreshLanguage) name:REFRESH_LANGUAGE object:nil];
    [self refreshLanguage];
    
    restoreArray = NSMutableArray.new;
    restoreIndex = 0;
    
    [self refreshSoundButton];

    [SplashView startInView:self.view withDelegate:self];
}

- (void)splashWillFinished {
    NSLog(@"splashWillFinished");
    [APP_DELEGATE playBackgroundMusic];
    if (![defaults objectForKey:@"LANGUAGE"]) {
        [self showSelectLanguagePopover];
    }
}

- (void)refreshLanguage {
    NSInteger language = [defaults integerForKey:@"LANGUAGE"];
    
    if (language == LANGUAGE_ENGLISH)
        backgroundView.image = [UIImage imageNamed:@"MainBackgroundEN.png"];
    else if (language == LANGUAGE_PORTUGUESE)
        backgroundView.image = [UIImage imageNamed:@"MainBackgroundPT.png"];
    else if (language == LANGUAGE_SPANISH)
        backgroundView.image = [UIImage imageNamed:@"MainBackgroundSP.png"];
    else
        backgroundView.image = [UIImage imageNamed:@"MainBackgroundFR.png"];
    
    {
        FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
        if (IS_IPAD)
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
        else
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:11];
        label.backgroundColor = [UIColor clearColor];
        label.gradientColors = @[UIColor.whiteColor, UICOLOR_RGB(218, 218, 218), UICOLOR_RGB(242, 242, 242)];
        label.gradientStartPoint = CGPointMake(0.5, 0.5);
        label.gradientEndPoint = CGPointMake(0.5, 0.9);
        label.innerShadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        if (IS_IPAD)
            label.innerShadowOffset = CGSizeMake(1, 1);
        else
            label.innerShadowOffset = CGSizeMake(0.5, 0.5);
        label.characterSpacing = -0.05;
        label.oversampling = 4;
        label.text = getStringWithKey(BACKUP_STRING);
        [label sizeToFit];
        
        UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
        [label.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [backupButton setImage:image forState:UIControlStateNormal];
    }
    {
        FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
        if (IS_IPAD)
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
        else
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:11];
        label.backgroundColor = UIColor.clearColor;
        label.gradientColors = @[UIColor.whiteColor, UICOLOR_RGB(218, 218, 218), UICOLOR_RGB(242, 242, 242)];
        label.gradientStartPoint = CGPointMake(0.5, 0.5);
        label.gradientEndPoint = CGPointMake(0.5, 0.9);
        label.innerShadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        if (IS_IPAD)
            label.innerShadowOffset = CGSizeMake(1, 1);
        else
            label.innerShadowOffset = CGSizeMake(0.5, 0.5);
        label.characterSpacing = -0.05;
        label.oversampling = 4;
        label.text = getStringWithKey(RESTORE_STRING);
        [label sizeToFit];
        
        UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
        [label.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [restoreButton setImage:image forState:UIControlStateNormal];
    }
    {
        FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
        if (IS_IPAD)
            label.font = [UIFont fontWithName:ARIAL_FONT size:24];
        else
            label.font = [UIFont fontWithName:ARIAL_FONT size:11];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:133 / 255.0 green:85 / 255.0 blue:41 / 255.0 alpha:1.0];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        if (IS_IPAD)
            label.shadowOffset = CGSizeMake(0, 1);
        else
            label.shadowOffset = CGSizeMake(0, 0.5);
        label.characterSpacing = -0.05;
        label.oversampling = 4;
        label.text = getStringWithKey(RESULTS_STRING);
        [label sizeToFit];
        
        UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
        [label.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [resultsButton setImage:image forState:UIControlStateNormal];
    }
    {
        FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
        if (IS_IPAD)
            label.font = [UIFont fontWithName:ARIAL_FONT size:24];
        else
            label.font = [UIFont fontWithName:ARIAL_FONT size:11];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:133 / 255.0 green:85 / 255.0 blue:41 / 255.0 alpha:1.0];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        if (IS_IPAD)
            label.shadowOffset = CGSizeMake(0, 1);
        else
            label.shadowOffset = CGSizeMake(0, 0.5);
        label.characterSpacing = -0.05;
        label.oversampling = 4;
        label.text = getStringWithKey(NEW_GAME_STRING);
        [label sizeToFit];
        
        UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
        [label.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [newGameButton setImage:image forState:UIControlStateNormal];
    }
    {
        FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
        label.font = [UIFont fontWithName:ARIAL_FONT size:IS_IPAD ? 24 : 11];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:133 / 255.0 green:85 / 255.0 blue:41 / 255.0 alpha:1.0];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        label.shadowOffset = IS_IPAD ? CGSizeMake(0, 1) : CGSizeMake(0, 0.5);
        label.characterSpacing = -0.05;
        label.oversampling = 4;
        label.text = getStringWithKey(SETTINGS_STRING);
        [label sizeToFit];
        
        UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
        [label.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [settingsButton setImage:image forState:UIControlStateNormal];
    }
    {
        FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
        label.font = [UIFont fontWithName:ARIAL_FONT size:IS_IPAD ? 24 : 11];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:133 / 255.0 green:85 / 255.0 blue:41 / 255.0 alpha:1.0];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        label.shadowOffset = IS_IPAD ? CGSizeMake(0, 1) : CGSizeMake(0, 0.5);
        label.characterSpacing = -0.05;
        label.oversampling = 4;
        label.text = getStringWithKey(SUPPORT_STRING);
        [label sizeToFit];
        
        UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
        [label.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [supportButton setImage:image forState:UIControlStateNormal];
    }
}

- (void)refreshSoundButton {
    if (APP_DELEGATE.backgroundAudioMute)
        [soundButton setBackgroundImage:[UIImage imageNamed:@"MainSoundOffButton.png"] forState:UIControlStateNormal];
    else
        [soundButton setBackgroundImage:[UIImage imageNamed:@"MainSoundOnButton.png"] forState:UIControlStateNormal];
}

- (IBAction)onSoundButton:(id)sender {
    APP_DELEGATE.backgroundAudioMute = !APP_DELEGATE.backgroundAudioMute;
    [self refreshSoundButton];
}

- (IBAction)onAppCenterButton:(id)sender {
    if (IS_IPAD)
        appCenterRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPad" bundle:nil];
    else
        appCenterRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPhone" bundle:nil];
    appCenterRestrictedPopupViewController.delegate = self;
    [appCenterRestrictedPopupViewController presentFromView:self.view];
}

- (IBAction)onBackupButton:(id)sender {
    backupRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:IS_IPAD ? @"RestrictedPopupViewControllerPad" : @"RestrictedPopupViewControllerPhone" bundle:nil];
    backupRestrictedPopupViewController.delegate = self;
    [backupRestrictedPopupViewController presentFromView:self.view];
}

- (IBAction)onRestoreButton:(id)sender {
    restoreRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:IS_IPAD ? @"RestrictedPopupViewControllerPad" : @"RestrictedPopupViewControllerPhone" bundle:nil];
    restoreRestrictedPopupViewController.delegate = self;
    [restoreRestrictedPopupViewController presentFromView:self.view];
}

- (void)performBackup {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(PLEASE_ENTER_A_NAME_STRING) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(CANCEL_STRING) style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UITextField *textField = alertController.textFields.firstObject;
        NSString *fileName = [NSString stringWithFormat:@"%@.data", textField.text];
        if ([[NSString.documentFolder stringByAppendingPathComponent:fileName] fileExists]) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(WARNING_STRING) message:getStringWithKey(FILE_ALREADY_EXISTS_STRING) preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
        } else {
            [FMDatabase closeDatabaseIfOpened];
            
            if (![BackupManager backupDataWithFileName:fileName]) {
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(WARNING_STRING) message:getStringWithKey(BACKUP_FAILED_STRING) preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
                [self presentViewController:alertController animated:true completion:nil];
            } else {
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(SUCCESS_STRING) message:getStringWithKey(BACKUP_SUCCESSFULLY_STRING) preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
                [self presentViewController:alertController animated:true completion:nil];
                [APP_DELEGATE playEffectAudio:@"backuptoitunes"];
            }
        }
        
    }]];
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)performRestore {
    [restoreArray removeAllObjects];
    [restoreArray addObjectsFromArray:[[NSString documentFolder] fileContents]];
    
    restoreIndex = 0;
    [restoreTableView reloadData];
    
    if (restoreArray.count > 0) {
        if (IS_IPAD) {
            PopupViewController *popupViewController = [[PopupViewController alloc] init];
            popupViewController.delegate = self;
            popupViewController.subview = restoreTableView;
            popupViewController.title = getStringWithKey(RESTORE_BACKUP_STRING);
            CustomNavigationController *navigationController = [[CustomNavigationController alloc] initWithRootViewController:popupViewController];
            navigationController.modalPresentationStyle = UIModalPresentationPopover;
            navigationController.popoverPresentationController.sourceRect = restoreButton.frame;
            navigationController.popoverPresentationController.sourceView = self.view;
            navigationController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
            [self presentViewController:navigationController animated:YES completion:nil];            
        } else {
            restoreTableView.contentInset = UIEdgeInsetsZero;
            restoreTableView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            PopupViewController *popupViewController = [[PopupViewController alloc] init];
            popupViewController.delegate = self;
            popupViewController.subview = restoreTableView;
            popupViewController.title = getStringWithKey(RESTORE_BACKUP_STRING);
            CustomNavigationController *navigationController = [[CustomNavigationController alloc] initWithRootViewController:popupViewController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
    } else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(NO_BACKUP_FILES_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];
    }
}

#pragma mark - PopupViewControllerDelegate
- (void)didClickDone:(PopupViewController *)popupViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(WARNING_STRING) message:getStringWithKey(RESTORE_CONFIRM_STRING) preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(NO_STRING) style:UIAlertActionStyleCancel handler:nil]];
    [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(YES_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [FMDatabase closeDatabaseIfOpened];
        
        NSString *fileName = self->restoreArray[self->restoreIndex];
        
        if (![BackupManager restoreDataWithFileName:fileName]) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(WARNING_STRING) message:getStringWithKey(RESTORE_FAILED_STRING) preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
        } else {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(SUCCESS_STRING) message:getStringWithKey(RESTORE_SUCCESSFULLY_STRING) preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
        }
        
        [self->restoreArray removeAllObjects];
        self->restoreIndex = 0;
        [self->restoreTableView reloadData];
    }]];
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)didClickCancel:(PopupViewController *)popupViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [restoreArray removeAllObjects];
    restoreIndex = 0;
    [restoreTableView reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return restoreArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"RESTORE_TABLEVIEWCELL";
    UITableViewCell *cell = [restoreTableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSInteger index = indexPath.row;
    if (index == restoreIndex)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.textLabel.text = [restoreArray objectAtIndex:index];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [restoreTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (restoreIndex == indexPath.row)
        return;
    
    UITableViewCell *cell = [restoreTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:restoreIndex inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    restoreIndex = indexPath.row;
    cell = [restoreTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:restoreIndex inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (IBAction)onResultsButton:(id)sender {
    [APP_DELEGATE stopEffectAudio];
    
    ReportViewController *reportViewController;
    if (IS_IPAD)
        reportViewController = [[ReportViewController alloc] initWithNibName:@"ReportViewControllerPad" bundle:nil];
    else
        reportViewController = [[ReportViewController alloc] initWithNibName:@"ReportViewControllerPhone" bundle:nil];
    [self.navigationController pushViewController:reportViewController animated:YES];
}

- (IBAction)onNewGameButton:(id)sender {
    [APP_DELEGATE stopEffectAudio];
    
    StudentsViewController *studentsViewController;
    if (IS_IPAD)
        studentsViewController = [[StudentsViewController alloc] initWithNibName:@"StudentsViewControllerPad" bundle:nil];
    else
        studentsViewController = [[StudentsViewController alloc] initWithNibName:@"StudentsViewControllerPhone" bundle:nil];
    [self.navigationController pushViewController:studentsViewController animated:YES];
}

- (IBAction)onSettingsButton:(id)sender {
    [APP_DELEGATE playEffectAudio:@"settings"];

    SettingsViewController *settingsViewController;
    if (IS_IPAD)
        settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewControllerPad" bundle:nil];
    else
        settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewControllerPhone" bundle:nil];
    settingsViewController.mainViewController = self;
    settingsPopoverController = [[PopoverController alloc] initWithContentViewController:settingsViewController];
    settingsPopoverController.delegate = self;

    if (IS_IPAD)
        [settingsPopoverController
         presentPopoverFromPoint:
         CGPointMake((418 + 420) * screenWidthRatio - 420,
                     (216 + 466) * screenHeightRatio - 466)
         inView:self.view];
    else
        [settingsPopoverController presentPopoverFromPoint:CGPointMake(screenWidth - 368, 2 + (screenHeight - 320)) inView:self.view];
}

- (IBAction)onSupportButton:(id)sender {
    if (IS_IPAD)
        supportRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPad" bundle:nil];
    else
        supportRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPhone" bundle:nil];
    supportRestrictedPopupViewController.delegate = self;
    [supportRestrictedPopupViewController presentFromView:self.view];
}

#pragma mark - RestrictedPopupViewControllerDelegate
- (void)didEnterCorrect:(RestrictedPopupViewController *)controller {
    if (controller == appCenterRestrictedPopupViewController) {
        AppCenterViewController *appCenterViewController = [[AppCenterViewController alloc] initWithNibName:@"AppCenterViewController" bundle:nil];
        [self.navigationController pushViewController:appCenterViewController animated:YES];
    } else if (controller == supportRestrictedPopupViewController) {
        supportRestrictedPopupViewController = nil;
        SupportViewController *vc = [SupportViewController.alloc initWithNibName:IS_IPAD ? @"SupportViewControllerPad" : @"SupportViewControllerPhone" bundle:nil];
        vc.mainViewController = self;
        supportPopoverController = [PopoverController.alloc initWithContentViewController:vc];
        supportPopoverController.delegate = self;

        if (IS_IPAD)
            [supportPopoverController
             presentPopoverFromPoint:
             CGPointMake((768 + 242) * screenWidthRatio - 242,
                         (417 + 265) * screenHeightRatio - 265)
             inView:self.view];
        else
            [supportPopoverController presentPopoverFromPoint:CGPointMake(screenWidth - 170, 100 + (screenHeight - 320)) inView:self.view];
    } else if (controller == backupRestrictedPopupViewController || controller == restoreRestrictedPopupViewController) {
        backupRestrictedPopupViewController = nil;
        backupManagementVC = [BackupManagementVC.alloc initWithNibName:@"BackupManagementPad" bundle:nil];
        backupManagementVC.delegate = self;
        backupManagementVC.view.frame = self.view.frame;
        [self addChildViewController:backupManagementVC];
        [backupManagementVC didMoveToParentViewController:self];
        [self.view addSubview:backupManagementVC.view];
    }
}

- (void)didCancel:(RestrictedPopupViewController *)controller {
    if (controller == appCenterRestrictedPopupViewController) {
        appCenterRestrictedPopupViewController = nil;
    } else if (controller == supportRestrictedPopupViewController) {
        supportRestrictedPopupViewController = nil;
    } else if (controller == backupRestrictedPopupViewController) {
        backupRestrictedPopupViewController = nil;
    } else if (controller == restoreRestrictedPopupViewController) {
        restoreRestrictedPopupViewController = nil;
    }
}

- (void)onSupportRequestSupportButton {
    if (requestConfirmPopoverController == nil) {
        RequestConfirmViewController *requestConfirmViewController = [[RequestConfirmViewController alloc] initWithNibName:IS_IPAD ? @"RequestConfirmViewControllerPad" : @"RequestConfirmViewControllerPhone" bundle:nil];
        requestConfirmViewController.mainViewController = self;
        requestConfirmPopoverController = [PopoverController.alloc initWithContentViewController:requestConfirmViewController];
        requestConfirmPopoverController.delegate = self;
        requestConfirmPopoverController.dismissedWhenTap = NO;
        requestConfirmPopoverController.popoverType = POPOVER_TYPE_TOPRIGHT;
    }
    
    [requestConfirmPopoverController presentPopoverFromPoint:IS_IPAD ? CGPointMake((screenWidth - 472) / 2, 239) : CGPointMake((screenWidth - 300) / 2, 67) inView:self.view];
}

- (void)onSupportWatchVideoButton {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://youtu.be/8b9EmuX7ZIs"] options:@{} completionHandler:nil];
}

- (void)onSupportMoreAppsButton {
    [APP_DELEGATE stopEffectAudio];
    [self onAppCenterButton:nil];
}

- (void)onSupportTellFriendsButton {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCompsoseViewController = [[MFMailComposeViewController alloc] init];
        mailCompsoseViewController.mailComposeDelegate = self;
        [mailCompsoseViewController setSubject:getStringWithKey(CHECK_OUT_WHQUESTIONS_STRING)];
        [mailCompsoseViewController setMessageBody:getStringWithKey(TELL_FRIENDS_DESCRIPTION_STRING) isHTML:NO];
        [self presentViewController:mailCompsoseViewController animated:YES completion:nil];
    } else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(ADD_ACCOUNT_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self->requestConfirmPopoverController dismissPopover];
        }]];
        [self presentViewController:alertController animated:true completion:nil];
    }
}

- (void)onRequestYesButton {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCompsoseViewController = [[MFMailComposeViewController alloc] init];
        mailCompsoseViewController.mailComposeDelegate = self;
        [mailCompsoseViewController setSubject:getStringWithKey(CONTACT_TITLE)];
        [mailCompsoseViewController setToRecipients:[NSArray arrayWithObject:@"support@smartyearsapps.com"]];
        
        NSString *deviceInfoString = @"";
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"Device : %@\n", [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ? @"iPad": @"iPhone"];
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"Device Model : %@\n", [[UIDevice currentDevice] model]];
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"Device System Name : %@\n", [[UIDevice currentDevice] systemName]];
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"Device System Version : %@\n", [[UIDevice currentDevice] systemVersion]];
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"Device Name : %@\n", [[UIDevice currentDevice] name]];
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"Application Version : %@\n", [[NSBundle.mainBundle infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]];
        deviceInfoString = [deviceInfoString stringByAppendingFormat:@"\n\n"];
        
        [mailCompsoseViewController setMessageBody:deviceInfoString isHTML:NO];
        [self presentViewController:mailCompsoseViewController animated:YES completion:nil];
    } else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(ADD_ACCOUNT_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self->requestConfirmPopoverController dismissPopover];
        }]];
        [self presentViewController:alertController animated:true completion:nil];
    }
}

- (void)onRequestNoButton {
    [requestConfirmPopoverController dismissPopover];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (requestConfirmPopoverController)
        [self performSelector:@selector(onRequestNoButton) withObject:nil afterDelay:0.3];
}

#pragma mark - PopoverControllerDelegate
- (void)popoverControllerDidDismissed:(PopoverController *)popoverController {
    if (popoverController == supportPopoverController) {
        supportPopoverController = nil;
    } else if (popoverController == requestConfirmPopoverController) {
        requestConfirmPopoverController = nil;
    } else if (popoverController == settingsPopoverController) {
        settingsPopoverController = nil;
        
        [APP_DELEGATE stopEffectAudio];
    } else if (popoverController == selectLanguagePopoverController) {
        selectLanguagePopoverController = nil;
    }
}

- (void)showSelectLanguagePopover {
    SelectLanguageViewController *selectLanguageViewController = [SelectLanguageViewController.alloc initWithNibName:IS_IPAD ? @"SelectLanguageViewControllerPad" : @"SelectLanguageViewControllerPhone" bundle:nil];
    selectLanguageViewController.mainViewController = self;
    selectLanguagePopoverController = [[PopoverController alloc] initWithContentViewController:selectLanguageViewController];
    selectLanguagePopoverController.delegate = self;
    selectLanguagePopoverController.showDarkBackground = YES;
    
    if ([defaults objectForKey:@"LANGUAGE"] == nil) {
        selectLanguagePopoverController.dismissedWhenTap = NO;
    }
    
    if (IS_IPAD)
        [selectLanguagePopoverController presentPopoverFromPoint:CGPointMake((screenWidth - 956) / 2, (screenHeight - 366) / 2) inView:self.view];
    else
        [selectLanguagePopoverController presentPopoverFromPoint:CGPointMake((screenWidth - 478) / 2, (screenHeight - 183) / 2) inView:self.view];
}

- (void)dismissSelectLanguagePopover {
    [selectLanguagePopoverController dismissPopover];
}

@end
