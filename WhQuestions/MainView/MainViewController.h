//
//  MainViewController.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : CustomViewController

- (void)onSupportRequestSupportButton;
- (void)onSupportWatchVideoButton;
- (void)onSupportMoreAppsButton;
- (void)onSupportTellFriendsButton;

- (void)onRequestYesButton;
- (void)onRequestNoButton;

- (void)performBackup;
- (void)performRestore;

- (void)showSelectLanguagePopover;
- (void)dismissSelectLanguagePopover;

@end
