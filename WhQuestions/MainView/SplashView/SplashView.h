//
//  SplashViewController.h
//  LanguageEmpires
//
//  Created by Igor Poltavstev on 01/26/17.
//  Copyright (c) 2017 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SplashViewDelegate <NSObject>
@optional
- (void)splashWillFinished;
- (void)splashDidFinished;
@end


@interface SplashView : UIView

+ (void)startInView:(UIView*)vc withDelegate:(id<SplashViewDelegate>)delegate;

@end
