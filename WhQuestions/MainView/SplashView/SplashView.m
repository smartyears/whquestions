//
//  SplashView.m
//  Bapa
//
//  Created by Igor Poltavstev on 01/26/17.
//  Copyright (c) 2017 Smarty Ears. All rights reserved.
//

#import "SplashView.h"
#import <AVFoundation/AVFoundation.h>

#define RATE 1
#define AUDIORATE RATE * 1

#ifndef IS_IPAD
#define IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#endif

@interface SplashView ()
{
    AVAudioPlayer *audioPlayer;
    AVPlayerLayer *playerLayer;
    
    id <SplashViewDelegate> delegate;
    BOOL isRemovingFromSuperView;
}
@end

@implementation SplashView

+ (void)startInView:(UIView*)view withDelegate:(id<SplashViewDelegate>)delegate_ {
    SplashView *splashView = [SplashView new];
    [splashView startInView:view withDelegate:delegate_];
}

- (void)startInView:(UIView*)view withDelegate:(id<SplashViewDelegate>)delegate_ {
    self.backgroundColor = [UIColor whiteColor];
    delegate = delegate_;
    [view addSubview:self];
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];

    if (!newSuperview) {
        return;
    }

    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(refreshViews) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];

    NSURL *url = [NSBundle.mainBundle URLForResource:IS_IPAD && UIScreen.mainScreen.scale > 1 ? @"splash" : @"splash" withExtension:@"mp4"];
    AVPlayer *player = [AVPlayer playerWithURL:url];
    playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    playerLayer.backgroundColor = [UIColor whiteColor].CGColor;
    player.rate = RATE;
    [self.layer addSublayer:playerLayer];
    [self refreshViews];
    
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(removeFromSuperview) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [player addObserver:self forKeyPath:@"status" options:0 context:nil];

    //if (!APP_DELEGATE.backgroundAudioMute) {
    //if ([soundTobePlayed isEqualToString:@"Yes"]) {
    NSURL *audioURL = [NSURL fileURLWithPath:[NSBundle.mainBundle pathForResource:@"splash" ofType:@"mp3"]];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
    audioPlayer.enableRate = YES;
    audioPlayer.rate = AUDIORATE;
    audioPlayer.volume = 0.05;
    //}

    UIView *tapView = [[UIView alloc] initWithFrame:self.frame];
    [tapView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeFromSuperview)]];
    [self addSubview:tapView];

    // Bug with AVPlayerItemDidPlayToEndTimeNotification in iOS 11.2.5? Apple - Specifically we were unable to get passed the initial loading screen.
    CGFloat duration = CMTimeGetSeconds(player.currentItem.asset.duration);
    SplashView __weak *weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((duration + 1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf removeFromSuperview];
    });
}

- (void)dealloc {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [NSNotificationCenter.defaultCenter removeObserver:self];
    @try {
        [playerLayer.player removeObserver:self forKeyPath:@"status"];
    } @catch (NSException *exception) {
        NSLog(@"error %@", exception);
    }
}

- (void)removeFromSuperview {
    if (isRemovingFromSuperView) { return; }
    isRemovingFromSuperView = YES;
    if ([self->delegate respondsToSelector:@selector(splashWillFinished)]) {
        [self->delegate splashWillFinished];
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if ([self->delegate respondsToSelector:@selector(splashDidFinished)]) {
            [self->delegate splashDidFinished];
        }
        [super removeFromSuperview];
    }];
}

- (void)refreshViews {
    self.frame = UIScreen.mainScreen.bounds;
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    playerLayer.frame = CGRectMake(0, (height - 3 * width / 4) / 2, width, 3 * width / 4);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == playerLayer.player && [keyPath isEqualToString:@"status"] && playerLayer.player.status == AVPlayerStatusReadyToPlay) {
            [playerLayer.player play];
            [audioPlayer play];
    }
}

@end
