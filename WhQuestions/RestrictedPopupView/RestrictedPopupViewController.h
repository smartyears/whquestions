//
//  RestrictedPopupViewController.h
//  WhQuestions
//
//  Created by Frank J. on 3/26/15.
//  Copyright (c) 2015 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RestrictedPopupViewControllerDelegate;

@interface RestrictedPopupViewController : UIViewController <UITextFieldDelegate> {
	id<RestrictedPopupViewControllerDelegate> __weak delegate;
	
	IBOutlet UIView *tapView;
	IBOutlet UIView *contentView;
	IBOutlet UILabel *descriptionLabel;
	IBOutlet UILabel *questionLabel;
	IBOutlet UITextField *answerTextField;
}

@property (nonatomic, weak) id<RestrictedPopupViewControllerDelegate> delegate;

- (void)presentFromView:(UIView *)view;

@end

@protocol RestrictedPopupViewControllerDelegate <NSObject>

- (void)didEnterCorrect:(RestrictedPopupViewController *)controller;
- (void)didCancel:(RestrictedPopupViewController *)controller;

@end
