//
//  RestrictedPopupViewController.m
//  WhQuestions
//
//  Created by Frank J. on 3/26/15.
//  Copyright (c) 2015 Smarty Ears. All rights reserved.
//

#import "RestrictedPopupViewController.h"

@interface RestrictedPopupViewController ()

@end

@implementation RestrictedPopupViewController

@synthesize delegate;

- (void)viewDidLoad {
	[super viewDidLoad];
	
	descriptionLabel.text = getStringWithKey(RESTRICTED_DESCRIPTION_STRING);
	questionLabel.text = @"9-3=";
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
	[tapView addGestureRecognizer:tapGesture];
	
	[APP_DELEGATE playEffectAudio:@"restriction"];
}

- (void)presentFromView:(UIView *)view {
	self.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
	[view addSubview:self.view];
	self.view.alpha = 0.0;
	[UIView animateWithDuration:0.2
					 animations:^(void) {
						 self.view.alpha = 1.0;
					 }
					 completion:^(BOOL finished) {
                         [self->answerTextField becomeFirstResponder];
					 }];
}

- (void)onTapGesture:(UITapGestureRecognizer *)gesture {
	[UIView animateWithDuration:0.2
					 animations:^(void) {
						 self.view.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 [self.view removeFromSuperview];
						 
                         [self->delegate didCancel:self];
					 }];
}

// UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
	if ([value isEqualToString:[NSString stringWithFormat:@"%ld", (long)[value integerValue]]] || value.length == 0) {
		return YES;
	}
	else {
		return NO;
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if ([textField.text integerValue] == 6) {
		[UIView animateWithDuration:0.2
						 animations:^(void) {
							 self.view.alpha = 0.0;
						 }
						 completion:^(BOOL finished) {
							 [self.view removeFromSuperview];
							 
                             [self->delegate didEnterCorrect:self];
						 }];
	}
	return YES;
}

@end
