//
//  SelectQuestionsViewController.m
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "SelectQuestionsViewController.h"
#import "StudentsViewController.h"
#import "StudentEntity.h"
#import "QuestionEntity.h"
#import "QuestionsManagement.h"

@interface SelectQuestionsViewController ()
{
    __weak IBOutlet FXLabel *nameLabel;
    __weak IBOutlet UIButton *backButton;
    __weak IBOutlet UIButton *prevButton;
    __weak IBOutlet UIButton *nextButton;
    __weak IBOutlet UIButton *whyButton;
    __weak IBOutlet UIButton *howButton;
    __weak IBOutlet UIButton *whereButton;
    __weak IBOutlet UIButton *whoButton;
    __weak IBOutlet UIButton *whenButton;
    __weak IBOutlet UIButton *whatButton;

    NSInteger currentStudentIndex;
    NSMutableArray *studentsSelectedArray;

    NSArray *questionButtonsArray;
}
@end

@implementation SelectQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

	if (IS_IPAD)
		self.preferredContentSize = CGSizeMake(802, 528);
	else
		self.preferredContentSize = CGSizeMake(402, 264);
	
	if (IS_IPAD)
		nameLabel.font = [UIFont fontWithName:CALIBRI_FONT size:42];
	else
		nameLabel.font = [UIFont fontWithName:CALIBRI_FONT size:21];
	nameLabel.characterSpacing = -0.1;
	nameLabel.oversampling = 4;

    [self setText:getStringWithKey(PREV_STRING) toButton:prevButton];
    [self setText:getStringWithKey(NEXT_STRING) toButton:nextButton];
    [self setText:getStringWithKey(BACK_STRING) toButton:backButton];

	questionButtonsArray = @[whyButton, howButton, whereButton, whoButton, whenButton, whatButton];
	
	NSArray *questionNamesArray = @[[NSString stringWithFormat:@"%@?", getStringWithKey(WHY_STRING)], [NSString stringWithFormat:@"%@?", getStringWithKey(HOW_STRING)], [NSString stringWithFormat:@"%@?", getStringWithKey(WHERE_STRING)], [NSString stringWithFormat:@"%@?", getStringWithKey(WHO_STRING)], [NSString stringWithFormat:@"%@?", getStringWithKey(WHEN_STRING)], [NSString stringWithFormat:@"%@?", getStringWithKey(WHAT_STRING)]];
	
	for (NSInteger i = 0; i < questionButtonsArray.count; i ++) {
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:45];
		else
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:22];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:244 / 255.0 green:246 / 255.0 blue:224 / 255.0 alpha:1.0];
		label.characterSpacing = -0.1;
		label.oversampling = 4;
		label.text = questionNamesArray[i];
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		UIButton *questionButton = [questionButtonsArray objectAtIndex:i];
		[questionButton setImage:image forState:UIControlStateNormal];
	}
	
	studentsSelectedArray = [[NSMutableArray alloc] init];
	for (NSInteger studentIndex = 0; studentIndex < self.studentsArray.count; studentIndex ++) {
		NSMutableDictionary *studentSelectedDictionary = [[NSMutableDictionary alloc] init];
		for (NSInteger i = 0; i < questionButtonsArray.count; i ++) {
			[studentSelectedDictionary setObject:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"%ld", (long)i]];
		}
		
		[studentsSelectedArray addObject:studentSelectedDictionary];
	}
	
	currentStudentIndex = 0;
	[self showCurrentStudent];
}

- (IBAction)backButton:(id)sender {
    [self.studentsViewController dismissSelectQuestionsPopoverController];
}

- (IBAction)onPrevButton:(id)sender {
	if (currentStudentIndex == 0)
		return;
	
	currentStudentIndex --;
	[self showCurrentStudent];
}

- (IBAction)onNextButton:(id)sender {
	BOOL selected = NO;
	NSMutableDictionary *studentSelectedDictionary = [studentsSelectedArray objectAtIndex:currentStudentIndex];
	for (NSInteger i = 0; i < questionButtonsArray.count; i ++) {
		if ([[studentSelectedDictionary objectForKey:[NSString stringWithFormat:@"%ld", (long)i]] boolValue]) {
			selected = YES;
			break;
		}
	}
	
	if (!selected) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(NO_SELECTION_ERROR_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];		
		return;
	}
	
	if (currentStudentIndex == self.studentsArray.count - 1) {
		for (NSInteger studentIndex = 0; studentIndex < self.studentsArray.count; studentIndex ++) {
			NSMutableDictionary *studentSelectedDictionary = [studentsSelectedArray objectAtIndex:studentIndex];
			NSMutableArray *selectedArray = [NSMutableArray array];
			for (NSInteger i = 0; i < questionButtonsArray.count; i ++) {
				if ([[studentSelectedDictionary objectForKey:[NSString stringWithFormat:@"%ld", (long)i]] boolValue]) {
					[selectedArray addObject:[NSNumber numberWithInteger:i + 1]];
				}
			}
			
			StudentEntity *student = [self.studentsArray objectAtIndex:studentIndex];
			student.addtionalObject = [[QuestionsManagement alloc] initWithQuestions: [QuestionEntity questionsByTypes:selectedArray activitType:self.activityType] andStudent:student activityType:self.activityType];
		}
		
		[self.studentsViewController startGame];
		
		return;
	}
	
	currentStudentIndex ++;
	[self showCurrentStudent];
}

- (IBAction)onItemButton:(UIButton *)button {
	NSMutableDictionary *studentSelectedDictionary = [studentsSelectedArray objectAtIndex:currentStudentIndex];
	
	NSInteger index = [questionButtonsArray indexOfObject:button];
	BOOL value = ![[studentSelectedDictionary objectForKey:[NSString stringWithFormat:@"%ld", (long)index]] boolValue];
	[studentSelectedDictionary setObject:[NSNumber numberWithBool:value] forKey:[NSString stringWithFormat:@"%ld", (long)index]];
	
	UIButton *questionButton = [questionButtonsArray objectAtIndex:index];
	if (value) {
		[questionButton setBackgroundImage:[UIImage imageNamed:@"SelectQuestionsItemSelectedPad.png"] forState:UIControlStateNormal];
	} else {
		[questionButton setBackgroundImage:[UIImage imageNamed:@"SelectQuestionsItemPad.png"] forState:UIControlStateNormal];
	}
}

- (void)showCurrentStudent {
    prevButton.hidden = !currentStudentIndex;

	StudentEntity *student = [self.studentsArray objectAtIndex:currentStudentIndex];
	nameLabel.text = student.name;
	
	NSMutableDictionary *studentSelectedDictionary = studentsSelectedArray[currentStudentIndex];
	for (NSInteger i = 0; i < questionButtonsArray.count; i ++) {
		UIButton *questionButton = [questionButtonsArray objectAtIndex:i];
		if ([studentSelectedDictionary[[NSString stringWithFormat:@"%ld", (long)i]] boolValue]) {
			[questionButton setBackgroundImage:[UIImage imageNamed:@"SelectQuestionsItemSelectedPad.png"] forState:UIControlStateNormal];
		} else {
			[questionButton setBackgroundImage:[UIImage imageNamed:@"SelectQuestionsItemPad.png"] forState:UIControlStateNormal];
		}
	}
}

- (void)setText:(NSString*)text toButton:(UIButton*)button {
    FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
    label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:IS_IPAD ? 24 : 11];
    label.backgroundColor = [UIColor clearColor];
    label.gradientColors = @[UIColor.whiteColor, UICOLOR_RGB(218, 218, 218), UICOLOR_RGB(242, 242, 242)];
    label.gradientStartPoint = CGPointMake(0.5, 0.5);
    label.gradientEndPoint = CGPointMake(0.5, 0.9);
    label.innerShadowColor = [UIColor colorWithWhite:0.0 alpha:0.75];
    label.innerShadowOffset = IS_IPAD ? CGSizeMake(1, 1) : CGSizeMake(0.5, 0.5);
    label.characterSpacing = -0.05;
    label.oversampling = 4;
    label.text = text;
    [label sizeToFit];

    UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
    [label.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    [button setImage:image forState:UIControlStateNormal];
}

@end
