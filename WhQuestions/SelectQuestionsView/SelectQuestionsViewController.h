//
//  SelectQuestionsViewController.h
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"

@class StudentsViewController;

@interface SelectQuestionsViewController : UIViewController 

@property (nonatomic, weak) StudentsViewController *studentsViewController;
@property (nonatomic, strong) NSArray *studentsArray;
@property (nonatomic) NSInteger activityType;

- (IBAction)onPrevButton:(id)sender;
- (IBAction)onNextButton:(id)sender;
- (IBAction)onItemButton:(UIButton *)button;

@end
