//
//  SelectLanguageViewController.m
//  WhQuesions
//
//  Created by Frank J. on 7/7/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "SelectLanguageViewController.h"
#import "MainViewController.h"

@interface SelectLanguageViewController ()

@end

@implementation SelectLanguageViewController

@synthesize mainViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (IS_IPAD)
		self.preferredContentSize = CGSizeMake(956, 366);
	else
		self.preferredContentSize = CGSizeMake(478, 183);
	
	titleLabel.text = getStringWithKey(SELECT_LANGUAGE_STRING);
	
	englishButton.tag = LANGUAGE_ENGLISH;
	portugueseButton.tag = LANGUAGE_PORTUGUESE;
	spanishButton.tag = LANGUAGE_SPANISH;
	frenchButton.tag = LANGUAGE_FRENCH;

	buttonsArray = [NSArray arrayWithObjects:
					englishButton,
					portugueseButton,
					spanishButton,
					frenchButton,
					nil];
	imageNamesArray = [NSArray arrayWithObjects:
					   @"SelectLanguageEnglishPad.png",
					   @"SelectLanguagePortuguesePad.png",
					   @"SelectLanguageSpanishPad.png",
					   @"SelectLanguageFrenchPad.png",
					   nil];
	selectedImageNamesArray = [NSArray arrayWithObjects:
							   @"SelectLanguageEnglishSelectedPad.png",
							   @"SelectLanguagePortugueseSelectedPad.png",
							   @"SelectLanguageSpanishSelectedPad.png",
							   @"SelectLanguageFrenchSelectedPad.png",
							   nil];
	
	[self refreshButtons];
}

- (IBAction)onLanguageButton:(id)sender {
	NSInteger selectedLanguage = ((UIButton *)sender).tag;
	NSInteger language;
	if ([defaults objectForKey:@"LANGUAGE"])
		language = [defaults integerForKey:@"LANGUAGE"];
	else
		language = -1;
	
	if (selectedLanguage == language)
		return;
	
	[defaults setInteger:selectedLanguage forKey:@"LANGUAGE"];
	[defaults synchronize];
	
	[self refreshButtons];
	
	[NSNotificationCenter.defaultCenter postNotificationName:REFRESH_LANGUAGE object:nil userInfo:nil];
	
	if (language == -1)
		[mainViewController dismissSelectLanguagePopover];
}

- (void)refreshButtons {
	NSInteger language;
	if ([defaults objectForKey:@"LANGUAGE"])
		language = [defaults integerForKey:@"LANGUAGE"];
	else
		language = -1;
	
	for (NSInteger i = 0; i < buttonsArray.count; i ++) {
		UIButton *languageButton = [buttonsArray objectAtIndex:i];
		if (i == language) {
			[languageButton setBackgroundImage:[UIImage imageNamed:[selectedImageNamesArray objectAtIndex:i]] forState:UIControlStateNormal];
		}
		else {
			[languageButton setBackgroundImage:[UIImage imageNamed:[imageNamesArray objectAtIndex:i]] forState:UIControlStateNormal];
		}
	}
	
	titleLabel.text = getStringWithKey(SELECT_LANGUAGE_STRING);

	englishLabel.text = getStringWithKey(ENGLISH_STRING);
	portugueseLabel.text = getStringWithKey(PORTUGUESE_STRING);
	spanishLabel.text = getStringWithKey(SPANISH_STRING);
	frenchLabel.text = getStringWithKey(FRENCH_STRING);
}

@end
