//
//  SelectLanguageViewController.h
//  WhQuesions
//
//  Created by Frank J. on 7/7/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"

@class MainViewController;

@interface SelectLanguageViewController : UIViewController {
	MainViewController __weak *mainViewController;
	
	IBOutlet FXLabel *titleLabel;
	IBOutlet UIButton *englishButton;
	IBOutlet FXLabel *englishLabel;
	IBOutlet UIButton *portugueseButton;
	IBOutlet FXLabel *portugueseLabel;
	IBOutlet UIButton *spanishButton;
	IBOutlet FXLabel *spanishLabel;
	IBOutlet UIButton *frenchButton;
	IBOutlet FXLabel *frenchLabel;
	
	NSArray *buttonsArray;
	NSArray *imageNamesArray;
	NSArray *selectedImageNamesArray;
}

@property (nonatomic, weak) MainViewController *mainViewController;

- (IBAction)onLanguageButton:(id)sender;

@end
