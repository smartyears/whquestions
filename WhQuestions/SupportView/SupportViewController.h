//
//  SupportViewController.h
//  Map
//
//  Created by Frank J. on 5/9/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"

@class MainViewController;

@interface SupportViewController : UIViewController {
	MainViewController __weak *mainViewController;
	
	IBOutlet FXLabel *versionLabel;
	IBOutlet UIButton *requestSupportButton;
	IBOutlet UIButton *watchVideoButton;
	IBOutlet UIButton *moreAppsButton;
	IBOutlet UIButton *tellFriendsButton;
}

@property (nonatomic, weak) MainViewController *mainViewController;

- (IBAction)onRequestSupportButton:(id)sender;
- (IBAction)onWatchVideoButton:(id)sender;
- (IBAction)onMoreAppsButton:(id)sender;
- (IBAction)onTellFriendsButton:(id)sender;

@end
