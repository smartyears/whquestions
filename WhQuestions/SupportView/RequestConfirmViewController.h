//
//  RequestConfirmViewController.h
//  iPracticeVerbs
//
//  Created by Frank J. on 9/20/13.
//  Copyright (c) 2013 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface RequestConfirmViewController : CustomViewController {
	MainViewController *__weak mainViewController;

	IBOutlet UILabel *contentLabel;
	IBOutlet UIImageView *stampView;
	IBOutlet UIButton *yesButton;
	IBOutlet UIButton *noButton;
}

@property (nonatomic, weak) MainViewController *mainViewController;

- (IBAction)onYesButton:(id)sender;
- (IBAction)onNoButton:(id)sender;

@end
