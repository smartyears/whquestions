//
//  SupportViewController.m
//  Map
//
//  Created by Frank J. on 5/9/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "SupportViewController.h"
#import "MainViewController.h"

@implementation SupportViewController

@synthesize mainViewController;

- (void)viewDidLoad {
	[super viewDidLoad];
	
	if (IS_IPAD)
		self.preferredContentSize = CGSizeMake(242, 265);
	else
		self.preferredContentSize = CGSizeMake(161, 177);
	
	if (IS_IPAD)
		versionLabel.font = [UIFont fontWithName:ARIAL_FONT size:19];
	else
		versionLabel.font = [UIFont fontWithName:ARIAL_FONT size:13];
	versionLabel.characterSpacing = -0.1;
	versionLabel.oversampling = 4;
	versionLabel.text = [NSString stringWithFormat:@"%@ %@", getStringWithKey(VERSION_STRING), [[NSBundle.mainBundle infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]];
	
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:30];
		else
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:20];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		label.characterSpacing = -0.1;
		label.oversampling = 4;
		label.text = getStringWithKey(REQUEST_SUPPORT_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[requestSupportButton setImage:image forState:UIControlStateNormal];
	}
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:30];
		else
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:20];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		label.characterSpacing = -0.1;
		label.oversampling = 4;
		label.text = getStringWithKey(WATCH_VIDEO_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[watchVideoButton setImage:image forState:UIControlStateNormal];
	}
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:30];
		else
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:20];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		label.characterSpacing = -0.1;
		label.oversampling = 4;
		label.text = getStringWithKey(MORE_APPS_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[moreAppsButton setImage:image forState:UIControlStateNormal];
	}
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:30];
		else
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:20];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
		label.characterSpacing = -0.1;
		label.oversampling = 4;
		label.text = getStringWithKey(TELL_FRIENDS_STRING);
		[label sizeToFit];
		
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[tellFriendsButton setImage:image forState:UIControlStateNormal];
	}
}

- (IBAction)onRequestSupportButton:(id)sender {
	[mainViewController onSupportRequestSupportButton];
}

- (IBAction)onWatchVideoButton:(id)sender {
	[mainViewController onSupportWatchVideoButton];
}

- (IBAction)onMoreAppsButton:(id)sender {
	[mainViewController onSupportMoreAppsButton];
}

- (IBAction)onTellFriendsButton:(id)sender {
	[mainViewController onSupportTellFriendsButton];
}

@end
