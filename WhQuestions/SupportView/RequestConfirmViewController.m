//
//  RequestConfirmViewController.m
//  iPracticeVerbs
//
//  Created by Frank J. on 9/20/13.
//  Copyright (c) 2013 Smarty Ears. All rights reserved.
//

#import "RequestConfirmViewController.h"
#import "MainViewController.h"

@interface RequestConfirmViewController ()

@end

@implementation RequestConfirmViewController

@synthesize mainViewController;

- (void)viewDidLoad {
	[super viewDidLoad];

	if (IS_IPAD)
		self.preferredContentSize = CGSizeMake(472, 290);
	else
		self.preferredContentSize = CGSizeMake(300, 185);

	if (IS_IPAD)
		contentLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:15];
	else
		contentLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:9];
	contentLabel.text = getStringWithKey(REQUEST_SUPPORT_CONFIRM_STRING);
	if (IS_IPAD)
		yesButton.titleLabel.font = [UIFont fontWithName:@"SegoeScript" size:50];
	else
		yesButton.titleLabel.font = [UIFont fontWithName:@"SegoeScript" size:32];
	yesButton.titleLabel.adjustsFontSizeToFitWidth = YES;
	yesButton.titleLabel.minimumScaleFactor = 0.1;
	[yesButton setTitle:getStringWithKey(YES_STRING) forState:UIControlStateNormal];
	if (IS_IPAD)
		noButton.titleLabel.font = [UIFont fontWithName:@"SegoeScript" size:50];
	else
		noButton.titleLabel.font = [UIFont fontWithName:@"SegoeScript" size:32];
	noButton.titleLabel.adjustsFontSizeToFitWidth = YES;
	noButton.titleLabel.minimumScaleFactor = 0.1;
	[noButton setTitle:getStringWithKey(NO_STRING) forState:UIControlStateNormal];

	stampView.alpha = 0.0;
}

- (IBAction)onYesButton:(id)sender {
	self.view.userInteractionEnabled = NO;

	[UIView animateWithDuration:0.2
					 animations:^(void) {
                         self->stampView.alpha = 1.0;
						 if (IS_IPAD)
                             self->stampView.frame = CGRectInset(self->stampView.frame, 5, 5);
						 else
                             self->stampView.frame = CGRectInset(self->stampView.frame, 3, 3);
					 }
					 completion:^(BOOL finished) {
                         [self->mainViewController onRequestYesButton];
					 }];
}

- (IBAction)onNoButton:(id)sender {
	[mainViewController onRequestNoButton];
}

@end
