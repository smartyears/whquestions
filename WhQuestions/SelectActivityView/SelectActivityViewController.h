//
//  SelectActivityViewController.h
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"

@class StudentsViewController;

@interface SelectActivityViewController : UIViewController {
	StudentsViewController __weak *studentsViewController;
	
	IBOutlet FXLabel *titleLabel;
	IBOutlet UIButton *speakButton;
	IBOutlet UIButton *selectButton;
}

@property (nonatomic, weak) StudentsViewController *studentsViewController;

- (IBAction)onSpeakButton:(id)sender;
- (IBAction)onSelectButton:(id)sender;

@end
