//
//  SelectActivityViewController.m
//  WhQuesions
//
//  Created by Frank J. on 7/1/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "SelectActivityViewController.h"
#import "StudentsViewController.h"

@interface SelectActivityViewController ()

@end

@implementation SelectActivityViewController

@synthesize studentsViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
	
    self.preferredContentSize = IS_IPAD ? CGSizeMake(802, 528) : CGSizeMake(402, 264);
    titleLabel.font = [UIFont fontWithName:CALIBRI_FONT size:IS_IPAD ? 42 : 21];
	titleLabel.characterSpacing = -0.1;
	titleLabel.oversampling = 4;
	titleLabel.text = getStringWithKey(SELECT_ACTIVITY_STRING);
	
	{
		UIImageView *imageView;
		if (IS_IPAD)
			imageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 354, 428)];
		else
			imageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 177, 214)];
		imageView.image = [UIImage imageNamed:@"SelectActivitySpeakButtonPad.png"];
		
		FXLabel *label;
		if (IS_IPAD) {
			label = [FXLabel.alloc initWithFrame:CGRectMake(10, 275, 334, 132)];
			label.font = [UIFont fontWithName:@"DINAlternate-Bold" size:56];
		}
		else {
			label = [FXLabel.alloc initWithFrame:CGRectMake(5, 137, 167, 66)];
			label.font = [UIFont fontWithName:@"DINAlternate-Bold" size:28];
		}
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor whiteColor];
		label.characterSpacing = -0.15;
		label.lineSpacing = -0.55;
		label.numberOfLines = 0;
		label.textAlignment = NSTextAlignmentCenter;
		label.oversampling = 4;
		label.text = getStringWithKey(SPEAK_MY_ANSWERS_STRING);
		[imageView addSubview:label];
		
		UIGraphicsBeginImageContextWithOptions(imageView.frame.size, NO, 0);
		[imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[speakButton setBackgroundImage:image forState:UIControlStateNormal];
	}
	{
		UIImageView *imageView;
		if (IS_IPAD)
			imageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 354, 428)];
		else
			imageView = [UIImageView.alloc initWithFrame:CGRectMake(0, 0, 177, 214)];
		imageView.image = [UIImage imageNamed:@"SelectActivitySelectButtonPad.png"];
		
		FXLabel *label;
		if (IS_IPAD) {
			label = [FXLabel.alloc initWithFrame:CGRectMake(10, 275, 334, 132)];
			label.font = [UIFont fontWithName:@"DINAlternate-Bold" size:56];
		}
		else {
			label = [FXLabel.alloc initWithFrame:CGRectMake(5, 137, 167, 66)];
			label.font = [UIFont fontWithName:@"DINAlternate-Bold" size:28];
		}
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor whiteColor];
		label.characterSpacing = -0.15;
		label.lineSpacing = -0.55;
		label.numberOfLines = 0;
		label.textAlignment = NSTextAlignmentCenter;
		label.oversampling = 4;
		label.text = getStringWithKey(SELECT_MY_ANSWERS_STRING);
		[imageView addSubview:label];
		
		UIGraphicsBeginImageContextWithOptions(imageView.frame.size, NO, 0);
		[imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[selectButton setBackgroundImage:image forState:UIControlStateNormal];
	}
}

- (IBAction)onSpeakButton:(id)sender {
	[studentsViewController onSpeakButton];
}

- (IBAction)onSelectButton:(id)sender {
	[studentsViewController onSelectButton];
}

@end
