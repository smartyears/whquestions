//
//  AppDelegate.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDelegate : UIResponder

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL backgroundAudioMute;

- (void)generatePDF:(NSString *)htmlString;

- (void)playBackgroundMusic;
- (void)stopBackgroundMusic;

- (void)playEffectAudio:(NSString *)filename;
- (void)stopEffectAudio;

@end
