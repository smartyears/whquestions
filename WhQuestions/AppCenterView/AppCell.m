//
//  AppCell.m
//  Version 1.1
//
//  Created by Frank Jacob on 01/07/16.
//  Copyright © 2016 Smarty Ears. All rights reserved.
//

#import "AppCell.h"
#import "AppCenterViewController.h"

@implementation AppCell

#define UBUNTU_REGULAR_FONT						@"Ubuntu"
#define UBUNTU_MEDIUM_FONT						@"Ubuntu-Medium"
#define UBUNTU_BOLD_FONT						@"Ubuntu-Bold"
#define UBUNTU_LIGHT_FONT						@"Ubuntu-Light"
#define UICOLOR_RGB(r, g, b)					[UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:1.0]


@synthesize appCenterViewController;
@synthesize trackViewUrl;
@synthesize imageName;
@synthesize nameLabel;
@synthesize priceLabel;

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 90, 90)];
		backView.backgroundColor = [UIColor whiteColor];
		backView.clipsToBounds = YES;
		backView.layer.cornerRadius = 18;
		[self.contentView addSubview:backView];
		
		itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
		itemButton.frame = CGRectMake(7, 7, 76, 76);
		itemButton.clipsToBounds = YES;
		itemButton.layer.cornerRadius = 15;
		[itemButton setBackgroundImage:[UIImage imageNamed:@"AppCenterItemBack"] forState:UIControlStateNormal];
		[itemButton addTarget:self action:@selector(onItemButton:) forControlEvents:UIControlEventTouchUpInside];
		[backView addSubview:itemButton];
		
        if (@available(iOS 13.0, *)) {
            activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
        } else {
            activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }
		activityIndicator.center = itemButton.center;
		[activityIndicator startAnimating];
		[backView addSubview:activityIndicator];
		
		nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 91, 140, 24)];
		nameLabel.font = [UIFont fontWithName:UBUNTU_REGULAR_FONT size:19];
		nameLabel.backgroundColor = [UIColor clearColor];
		nameLabel.textColor = UICOLOR_RGB(169, 33, 71);
		nameLabel.textAlignment = NSTextAlignmentCenter;
		[self.contentView addSubview:nameLabel];
		
		priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 113, 140, 16)];
		priceLabel.font = [UIFont fontWithName:UBUNTU_REGULAR_FONT size:14];
		priceLabel.backgroundColor = [UIColor clearColor];
		priceLabel.textColor = UICOLOR_RGB(242, 176, 92);
		priceLabel.textAlignment = NSTextAlignmentCenter;
		[self.contentView addSubview:priceLabel];
		
		[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(refreshImage:) name:@"APP_CENTER_RFERESH_IMAGE" object:nil];
	}
	return self;
}

- (void)dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)onItemButton:(id)sender {
	[appCenterViewController onItemButton:trackViewUrl];
}

- (void)refreshImage:(NSNotification *)notification {
	NSString *notificationImageName = (NSString *)notification.object;
	if ([imageName isEqualToString:notificationImageName]) {
		[self loadImage];
	}
}

- (void)loadImage {
	[activityIndicator stopAnimating];
	UIImage *image = [UIImage imageWithContentsOfFile:imageName];
	[itemButton setImage:image forState:UIControlStateNormal];
}

@end
