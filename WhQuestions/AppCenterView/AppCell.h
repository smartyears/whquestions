//
//  AppCell.h
//  Version 1.1
//
//  Created by Frank Jacob on 01/07/16.
//  Copyright © 2016 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppCenterViewController;

@interface AppCell : UICollectionViewCell {
	AppCenterViewController __weak *appCenterViewController;
	NSString *trackViewUrl;
	NSString *imageName;

	UIButton *itemButton;
	UIActivityIndicatorView *activityIndicator;
	UILabel *nameLabel;
	UILabel *priceLabel;
}

@property (nonatomic, weak) AppCenterViewController *appCenterViewController;
@property (nonatomic, strong) NSString *trackViewUrl;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, readonly) UILabel *nameLabel;
@property (nonatomic, readonly) UILabel *priceLabel;

- (void)loadImage;

@end
