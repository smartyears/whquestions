//
//  AppCenterViewController.m
//  Version 1.1
//
//  Created by Frank Jacob on 01/07/16.
//  Copyright © 2016 Smarty Ears. All rights reserved.
//

#import "AppCenterViewController.h"
#import "AppCell.h"

@interface AppCenterViewController () <UICollectionViewDataSource, UICollectionViewDelegate> {
    IBOutlet UICollectionView *newReleasesCollectionView;
    IBOutlet UICollectionView *storeCollecitonView;
    IBOutlet UICollectionView *myAppsCollectionView;
    
    NSMutableArray *newReleasesAppsArray, *storeAppsArray, *myAppsArray;
    NSURLSession *session;
    NSString *cachePath;
    MBProgressHUD *hud;
    
//    BOOL statusBarInitialState;
}
@end

@implementation AppCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    statusBarInitialState = UIApplication.sharedApplication.statusBarHidden;
//    UIApplication.sharedApplication.statusBarHidden = YES;
    
    newReleasesAppsArray = @[].mutableCopy;
    storeAppsArray = @[].mutableCopy;
    myAppsArray = @[].mutableCopy;
    
    [newReleasesCollectionView registerClass:[AppCell class] forCellWithReuseIdentifier:@"AppCell"];
    [storeCollecitonView registerClass:[AppCell class] forCellWithReuseIdentifier:@"AppCell"];
    [myAppsCollectionView registerClass:[AppCell class] forCellWithReuseIdentifier:@"AppCell"];
    
    //remove old-version cache in documents
    NSString *thumbPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Thumb"];
    if ([FILE_MANAGER fileExistsAtPath:thumbPath]) [FILE_MANAGER removeItemAtPath:thumbPath error:nil];
    
    cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"AppCenterIcons"];
    if (![FILE_MANAGER fileExistsAtPath:cachePath]) [FILE_MANAGER createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    session = [NSURLSession sessionWithConfiguration:configuration];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.userInteractionEnabled = NO;
    hud.removeFromSuperViewOnHide = YES;
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/lookup?id=348779167&entity=iPadSoftware&limit=200"];
    [[session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                [self->hud hideAnimated:YES];
                
                //prevent keyboard appearence
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"iTunes is unavailable" preferredStyle:UIAlertControllerStyleAlert];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alertController animated:true completion:nil];
                });
            }
            else
                [self checkContent:data ? [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] : nil];
        });
    }] resume];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)onBackButton:(id)sender {
//    UIApplication.sharedApplication.statusBarHidden = statusBarInitialState;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)checkContent:(id)JSON {
    NSArray *resultsArray = JSON[@"results"];
    if (![resultsArray isKindOfClass:[NSArray class]]) {
        [hud hideAnimated:YES];
        return;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    for (NSDictionary *dictionary in resultsArray) {
        if (![dictionary isKindOfClass:[NSDictionary class]])   continue;
        if (![dictionary[@"kind"] isEqualToString:@"software"]) continue;
        
        NSMutableDictionary *appInfoDictionary = @{}.mutableCopy;
        
        if (dictionary[@"trackName"] == nil) continue;
        else appInfoDictionary[@"trackName"] = dictionary[@"trackName"];
        
        if (dictionary[@"bundleId"] == nil) continue;
        else appInfoDictionary[@"bundleId"] = dictionary[@"bundleId"];
        
        if (dictionary[@"price"] == nil) continue;
        else appInfoDictionary[@"price"] = dictionary[@"price"];
        
        NSString *artworkUrl = dictionary[@"artworkUrl100"];
        if (artworkUrl == nil) {
            artworkUrl = dictionary[@"artworkUrl512"];
            if (artworkUrl == nil) {
                artworkUrl = dictionary[@"artworkUrl60"];
                if (artworkUrl == nil) {
                    continue;
                }
            }
        }
        appInfoDictionary[@"artworkUrl"] = artworkUrl;
        
        if (dictionary[@"releaseDate"] == nil) continue;
        else appInfoDictionary[@"releaseDate"] = [dateFormatter dateFromString:dictionary[@"releaseDate"]];
        
        if (dictionary[@"genres"] == nil) continue;
        else appInfoDictionary[@"categories"] = dictionary[@"genres"];
        
        if (dictionary[@"trackViewUrl"] == nil) continue;
        else appInfoDictionary[@"trackViewUrl"] = dictionary[@"trackViewUrl"];
        
        NSString *urlScheme = [dictionary[@"bundleId"] stringByReplacingOccurrencesOfString:@"." withString:@""];
        if ([urlScheme isEqualToString:@"iscrenaphasiasmartyears"]) urlScheme = @"smartyearsiscreenaphasia";
        appInfoDictionary[@"urlScheme"] = urlScheme;
        
        [[UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:[urlScheme stringByAppendingString:@"://"]]]
         ? myAppsArray : storeAppsArray addObject:appInfoDictionary];
        
        appInfoDictionary[@"imageName"] = [cachePath stringByAppendingPathComponent:[urlScheme stringByAppendingString:@".jpg"]];
        [self loadImage:appInfoDictionary[@"imageName"] urlString:artworkUrl];
    }
    
    if (storeAppsArray.count > 0)  {
        NSArray *sortedArray = [storeAppsArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"releaseDate" ascending:NO]]];
        for (NSInteger i = 0; i < 6; i++) {
            if (i < sortedArray.count) {
                [newReleasesAppsArray addObject:sortedArray[i]];
            }
        }
    }
    
    [newReleasesCollectionView reloadData];
    [storeCollecitonView reloadData];
    [myAppsCollectionView reloadData];
    
    [hud hideAnimated:YES];
}

#pragma mark UICollectionViewDataSource, UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (collectionView == newReleasesCollectionView ? newReleasesAppsArray : collectionView == storeCollecitonView ? storeAppsArray : myAppsArray).count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AppCell *cell = (AppCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"AppCell" forIndexPath:indexPath];
    
    NSDictionary *appInfoDictionary = collectionView == newReleasesCollectionView ? newReleasesAppsArray[indexPath.item]
    : collectionView == storeCollecitonView ? storeAppsArray[indexPath.item] : myAppsArray[indexPath.item];
    
    cell.appCenterViewController = self;
    cell.trackViewUrl = collectionView == myAppsCollectionView ? [NSString stringWithFormat:@"%@://", appInfoDictionary[@"urlScheme"]] : appInfoDictionary[@"trackViewUrl"];
    
    cell.imageName = appInfoDictionary[@"imageName"];
    if ([FILE_MANAGER fileExistsAtPath:cell.imageName]) [cell loadImage];
    
    cell.nameLabel.text = appInfoDictionary[@"trackName"];
    CGFloat price = [appInfoDictionary[@"price"] floatValue];
    cell.priceLabel.text = price == 0.0 ? @"Free" : [NSString stringWithFormat:@"$%.02f", price];
    
    return cell;
}

- (void)onItemButton:(NSString *)trackViewUrl {
    [UIApplication.sharedApplication openURL:[NSURL URLWithString:trackViewUrl] options:@{} completionHandler:nil];
}

- (void)loadImage:(NSString *)imageName urlString:(NSString*)urlString {
    [[session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        UIImage *image = data ? [UIImage imageWithData:data] : nil;
        if (image) {
            [UIImageJPEGRepresentation(image, 0.7) writeToFile:imageName atomically:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                [NSNotificationCenter.defaultCenter postNotificationName:@"APP_CENTER_RFERESH_IMAGE" object:imageName];
            });
        }
    }] resume];
}

@end
