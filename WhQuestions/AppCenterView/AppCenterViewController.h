//
//  AppCenterViewController.h
//  Version 1.1
//
//  Created by Frank Jacob on 01/07/16.
//  Copyright © 2016 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface AppCenterViewController : UIViewController

- (void)onItemButton:(NSString *)trackViewUrl;

@end
