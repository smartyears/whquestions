//
//  SettingsViewController.m
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "SettingsViewController.h"
#import "WhQuestionsSetting.h"
#import "MainViewController.h"

@implementation SettingsViewController

@synthesize mainViewController;

- (void)dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self name:REFRESH_LANGUAGE object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (IS_IPAD)
		self.preferredContentSize = CGSizeMake(420, 466);
	else
		self.preferredContentSize = CGSizeMake(280, 275);
	
	if (IS_IPAD) {
		randomQuestionLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		displayWrittenQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		speakQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		transitionSoundsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		moveAutomaticallyLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		audioInstructionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		numberOfQuestionsPerStudentLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		languageLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		languageButton.titleLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
	}
	else {
		randomQuestionLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		displayWrittenQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		speakQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		transitionSoundsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		moveAutomaticallyLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		audioInstructionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		numberOfQuestionsPerStudentLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		languageLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		languageButton.titleLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
	}
	languageButton.titleLabel.adjustsFontSizeToFitWidth = YES;
	languageButton.titleLabel.minimumScaleFactor = 0.1;
	
	randomQuestionSwitch.on = [WhQuestionsSetting randomQuestion];
	displayWrittenQuestionsSwitch.on = [WhQuestionsSetting displayWrittenQuestions];
	speakQuestionsSwitch.on = [WhQuestionsSetting speakQuestions];
	transitionSoundsSwitch.on = [WhQuestionsSetting transitionSounds];
	moveAutomaticallySwitch.on = [WhQuestionsSetting moveAutomatically];
	audioInstructionsSwitch.on = [WhQuestionsSetting audioInstructions];
	numberOfQuestionsPerStudentSegmentControl.selectedSegmentIndex = [WhQuestionsSetting numberOfQuestionsPerStudent] - 1;

	[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(refreshLanguage) name:REFRESH_LANGUAGE object:nil];
	[self refreshLanguage];
}

- (void)refreshLanguage {
	randomQuestionLabel.text = getStringWithKey(RANDOMIZE_QUESTIONS_STRING);
	displayWrittenQuestionsLabel.text = getStringWithKey(DISPLAY_WRITTEN_QUESTIONS_STRING);
	speakQuestionsLabel.text = getStringWithKey(SPEAK_QUESTIONS_STRING);
	transitionSoundsLabel.text = getStringWithKey(TRANSITION_SOUNDS_STRING);
	moveAutomaticallyLabel.text = getStringWithKey(MOVE_AUTOMATICALLY_STRING);
	audioInstructionsLabel.text = getStringWithKey(AUDIO_INSTRUCTIONS_STRING);
	numberOfQuestionsPerStudentLabel.text = getStringWithKey(NUMBER_OF_QUESTION_STUDENT_STRING);
	
	languageLabel.text = getStringWithKey(LANGUAGE_STRING);
	NSInteger language = [defaults integerForKey:@"LANGUAGE"];
	if (language == LANGUAGE_ENGLISH)
		[languageButton setTitle:getStringWithKey(ENGLISH_STRING) forState:UIControlStateNormal];
	else if (language == LANGUAGE_PORTUGUESE)
		[languageButton setTitle:getStringWithKey(PORTUGUESE_STRING) forState:UIControlStateNormal];
	else if (language == LANGUAGE_SPANISH)
		[languageButton setTitle:getStringWithKey(SPANISH_STRING) forState:UIControlStateNormal];
	else
		[languageButton setTitle:getStringWithKey(FRENCH_STRING) forState:UIControlStateNormal];
}

- (IBAction)onRandomQuestionSwitch:(id)sender {
	[WhQuestionsSetting setRandomQuestion:randomQuestionSwitch.on];
}

- (IBAction)onDisplayWrittenQuestionsSwitch:(id)sender {
	[WhQuestionsSetting setDisplayWrittenQuestions:displayWrittenQuestionsSwitch.on];
}

- (IBAction)onSpeakQuestionsSwitch:(id)sender {
	[WhQuestionsSetting setSpeakQuestions:speakQuestionsSwitch.on];
}

- (IBAction)onTransitionSoundsSwitch:(id)sender {
	[WhQuestionsSetting setTransitionSounds:transitionSoundsSwitch.on];
}

- (IBAction)onMoveAutomaticallySwitch:(id)sender {
	[WhQuestionsSetting setMoveAutomatically:moveAutomaticallySwitch.on];
}

- (IBAction)onAudioInstructionsSwitch:(id)sender {
	[WhQuestionsSetting setAudioInstructions:audioInstructionsSwitch.on];
}

- (IBAction)onNumberOfQuestionsPerStudentSegmentControl:(id)sender {
	[WhQuestionsSetting setNumberOfQuestionsPerStudent:numberOfQuestionsPerStudentSegmentControl.selectedSegmentIndex + 1];
}

- (IBAction)onLanguageButton:(id)sender {
	[mainViewController showSelectLanguagePopover];
}

@end
