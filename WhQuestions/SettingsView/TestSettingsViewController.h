//
//  TestSettingsViewController.h
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestSettingsViewController : UIViewController {
	IBOutlet UILabel *displayWrittenQuestionsLabel;
	IBOutlet UISwitch *displayWrittenQuestionsSwitch;
	
	IBOutlet UILabel *speakQuestionsLabel;
	IBOutlet UISwitch *speakQuestionsSwitch;
	
	IBOutlet UILabel *transitionSoundsLabel;
	IBOutlet UISwitch *transitionSoundsSwitch;
	
	IBOutlet UILabel *moveAutomaticallyLabel;
	IBOutlet UISwitch *moveAutomaticallySwitch;
}

- (IBAction)onDisplayWrittenQuestionsSwitch:(id)sender;
- (IBAction)onSpeakQuestionsSwitch:(id)sender;
- (IBAction)onTransitionSoundsSwitch:(id)sender;
- (IBAction)onMoveAutomaticallySwitch:(id)sender;

@end
