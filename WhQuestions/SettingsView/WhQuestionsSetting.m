//
//  WhQuestionsSetting.m
//  WhQuesions
//
//  Created by Frank J. on 10/12/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import "WhQuestionsSetting.h"

@interface WhQuestionsSetting()

@end

@implementation WhQuestionsSetting

+ (BOOL)randomQuestion {
	if ([defaults objectForKey:@"randomQuestion"] == nil) {
		return YES;
	}
	return [defaults boolForKey:@"randomQuestion"];
}

+ (void)setRandomQuestion:(BOOL)newValue {
	[defaults setBool:newValue forKey:@"randomQuestion"];
	[defaults synchronize];
}

+ (BOOL)displayWrittenQuestions {
	if ([defaults objectForKey:@"displayWrittenQuestions"] == nil) {
		return YES;
	}
	return [defaults boolForKey:@"displayWrittenQuestions"];
}

+ (void)setDisplayWrittenQuestions:(BOOL)newValue {
	[defaults setBool:newValue forKey:@"displayWrittenQuestions"];
	[defaults synchronize];
}

+ (BOOL)transitionSounds {
	if ([defaults objectForKey:@"transitionSounds"] == nil) {
		return YES;
	}
	return [defaults boolForKey:@"transitionSounds"];
}

+ (void)setTransitionSounds:(BOOL)newValue {
	[defaults setBool:newValue forKey:@"transitionSounds"];
	[defaults synchronize];
}

+ (NSInteger)numberOfQuestionsPerStudent {
	if ([defaults objectForKey:@"numberOfQuestionsPerStudent"] == nil) {
		return 1;
	}
	return [defaults integerForKey:@"numberOfQuestionsPerStudent"];
}

+ (void)setNumberOfQuestionsPerStudent:(NSInteger)newValue {
	[defaults setInteger:newValue forKey:@"numberOfQuestionsPerStudent"];
	[defaults synchronize];
}

+ (BOOL)speakQuestions {
	if ([defaults objectForKey:@"speakQuestions"] == nil) {
		return YES;
	}
	return [defaults boolForKey:@"speakQuestions"];
}

+ (void)setSpeakQuestions:(BOOL)newValue {
	[defaults setBool:newValue forKey:@"speakQuestions"];
	[defaults synchronize];
}

+ (BOOL)moveAutomatically {
	if ([defaults objectForKey:@"moveAutomatically"] == nil) {
		return YES;
	}
	return [defaults boolForKey:@"moveAutomatically"];
}

+ (void)setMoveAutomatically:(BOOL)newValue {
	[defaults setBool:newValue forKey:@"moveAutomatically"];
	[defaults synchronize];
}

+ (BOOL)audioInstructions {
	if ([defaults objectForKey:@"audioInstructions"] == nil) {
		return YES;
	}
	return [defaults boolForKey:@"audioInstructions"];
}

+ (void)setAudioInstructions:(BOOL)newValue {
	[defaults setBool:newValue forKey:@"audioInstructions"];
	[defaults synchronize];
}

@end
