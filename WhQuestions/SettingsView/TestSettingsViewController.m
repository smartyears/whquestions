//
//  TestSettingsViewController.m
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "TestSettingsViewController.h"
#import "WhQuestionsSetting.h"

@implementation TestSettingsViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	if (IS_IPAD)
		self.preferredContentSize = CGSizeMake(420, 234);
	else
		self.preferredContentSize = CGSizeMake(280, 156);
	
	if (IS_IPAD) {
		displayWrittenQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		speakQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		transitionSoundsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		moveAutomaticallyLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
	}
	else {
		displayWrittenQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		speakQuestionsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		transitionSoundsLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
		moveAutomaticallyLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:16];
	}
	
	displayWrittenQuestionsSwitch.on = [WhQuestionsSetting displayWrittenQuestions];
	speakQuestionsSwitch.on = [WhQuestionsSetting speakQuestions];
	transitionSoundsSwitch.on = [WhQuestionsSetting transitionSounds];
	moveAutomaticallySwitch.on = [WhQuestionsSetting moveAutomatically];
	
	displayWrittenQuestionsLabel.text = getStringWithKey(DISPLAY_WRITTEN_QUESTIONS_STRING);
	speakQuestionsLabel.text = getStringWithKey(SPEAK_QUESTIONS_STRING);
	transitionSoundsLabel.text = getStringWithKey(TRANSITION_SOUNDS_STRING);
	moveAutomaticallyLabel.text = getStringWithKey(MOVE_AUTOMATICALLY_STRING);
}

- (IBAction)onDisplayWrittenQuestionsSwitch:(id)sender {
	[WhQuestionsSetting setDisplayWrittenQuestions:displayWrittenQuestionsSwitch.on];
}

- (IBAction)onSpeakQuestionsSwitch:(id)sender {
	[WhQuestionsSetting setSpeakQuestions:speakQuestionsSwitch.on];
}

- (IBAction)onTransitionSoundsSwitch:(id)sender {
	[WhQuestionsSetting setTransitionSounds:transitionSoundsSwitch.on];
}

- (IBAction)onMoveAutomaticallySwitch:(id)sender {
	[WhQuestionsSetting setMoveAutomatically:moveAutomaticallySwitch.on];
}

@end
