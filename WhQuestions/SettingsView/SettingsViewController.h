//
//  SettingsViewController.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface SettingsViewController : UIViewController {
	MainViewController __weak *mainViewController;
	
	IBOutlet UILabel *randomQuestionLabel;
	IBOutlet UISwitch *randomQuestionSwitch;
	
	IBOutlet UILabel *displayWrittenQuestionsLabel;
	IBOutlet UISwitch *displayWrittenQuestionsSwitch;

	IBOutlet UILabel *speakQuestionsLabel;
	IBOutlet UISwitch *speakQuestionsSwitch;

	IBOutlet UILabel *transitionSoundsLabel;
	IBOutlet UISwitch *transitionSoundsSwitch;

	IBOutlet UILabel *moveAutomaticallyLabel;
	IBOutlet UISwitch *moveAutomaticallySwitch;
	
	IBOutlet UILabel *audioInstructionsLabel;
	IBOutlet UISwitch *audioInstructionsSwitch;
	
	IBOutlet UILabel *numberOfQuestionsPerStudentLabel;
	IBOutlet UISegmentedControl *numberOfQuestionsPerStudentSegmentControl;
	
	IBOutlet UILabel *languageLabel;
	IBOutlet UIButton *languageButton;
}

@property (nonatomic, weak) MainViewController *mainViewController;

- (IBAction)onRandomQuestionSwitch:(id)sender;
- (IBAction)onDisplayWrittenQuestionsSwitch:(id)sender;
- (IBAction)onSpeakQuestionsSwitch:(id)sender;
- (IBAction)onTransitionSoundsSwitch:(id)sender;
- (IBAction)onMoveAutomaticallySwitch:(id)sender;
- (IBAction)onAudioInstructionsSwitch:(id)sender;
- (IBAction)onNumberOfQuestionsPerStudentSegmentControl:(id)sender;
- (IBAction)onLanguageButton:(id)sender;

@end
