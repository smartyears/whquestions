//
//  WhQuestionsSetting.h
//  WhQuesions
//
//  Created by Frank J. on 10/12/11.
//  Copyright 2014 Smarty Ears. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WhQuestionsSetting : NSObject

+ (BOOL)randomQuestion;
+ (void)setRandomQuestion:(BOOL)newValue;

+ (BOOL)displayWrittenQuestions;
+ (void)setDisplayWrittenQuestions:(BOOL)newValue;

+ (BOOL)transitionSounds;
+ (void)setTransitionSounds:(BOOL)newValue;

+ (NSInteger)numberOfQuestionsPerStudent;
+ (void)setNumberOfQuestionsPerStudent:(NSInteger)newValue;

+ (BOOL)speakQuestions;
+ (void)setSpeakQuestions:(BOOL)newValue;

+ (BOOL)moveAutomatically;
+ (void)setMoveAutomatically:(BOOL)newValue;

+ (BOOL)audioInstructions;
+ (void)setAudioInstructions:(BOOL)newValue;

@end
