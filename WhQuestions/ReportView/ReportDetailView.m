//
//  ReportDetailView.m
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "ReportDetailView.h"
#import "SessionEntity.h"
#import "TTTAttributedLabel.h"
#import "OHASBasicMarkupParser.h"
#import "ReportAccuracyView.h"

#define IPAD_SMALL        568
#define IPAD_9_7_INCH        1024
#define IPAD_11_INCH        1194
#define IPAD_12_9_INCH        1366
@implementation ReportDetailView

@synthesize session;
@synthesize htmlString;

- (void)setSession:(SessionEntity *)_session {
	session = _session;
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"M-d-yyyy"];
	
	NSInteger y;
	if (IS_IPAD)
		y = 10;
	else
		y = 5;
	TTTAttributedLabel *label;
	if (IS_IPAD)
    {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 490, 25)];
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 490, 25)];
        }
//        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
//        {
//            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 490, 25)];
//        }
        else
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 390, 25)];
            //label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 390, 25)];
        }
		
		label.font = [UIFont fontWithName:ARIAL_FONT size:20];
	}
	else
    {
		label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(5, y, 240, 13)];
		label.font = [UIFont fontWithName:ARIAL_FONT size:10];
	}
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:52 / 255.0 green:87 / 255.0 blue:27 / 255.0 alpha:1.0];
	label.adjustsFontSizeToFitWidth = YES;
	label.minimumScaleFactor = 0.1;
	label.text = [NSString stringWithFormat:@"%@ *%@*", getStringWithKey(DATE_STRING), [dateFormatter stringFromDate:session.date]];
	label.attributedText = [OHASBasicMarkupParser attributedStringByProcessingMarkupInAttributedString:label.attributedText];
	[self addSubview:label];

	if (!IS_IPAD)
		y += label.frame.size.height;

	htmlString = [NSString stringWithFormat:@"%@ <b>%@</b>", getStringWithKey(DATE_STRING), [dateFormatter stringFromDate:session.date]];
	
	NSInteger correctCount = session.howCorrect + session.whatCorrect + session.whereCorrect + session.whenCorrect + session.whyCorrect + session.whoCorrect;
	NSInteger wrongCount = session.howWrong + session.whatWrong + session.whereWrong + session.whenWrong + session.whyWrong + session.whoWrong;
	NSInteger totalCount = correctCount + wrongCount;
	NSInteger accuracy;
	if (totalCount == 0)
		accuracy = -1;
	else
		accuracy = correctCount * 100 / totalCount;

	if (IS_IPAD)
    {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(502, y, 190, 25)];
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(502, y, 190, 25)];
        }
        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(302, y, 190, 25)];
        }
        else
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(302, y, 190, 25)];
        }
		
		label.font = [UIFont fontWithName:ARIAL_FONT size:20];
	}
	else
    {
		label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(5, y, 240, 13)];
		label.font = [UIFont fontWithName:ARIAL_FONT size:10];
	}
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:52 / 255.0 green:87 / 255.0 blue:27 / 255.0 alpha:1.0];
	label.adjustsFontSizeToFitWidth = YES;
	label.minimumScaleFactor = 0.1;
	if (IS_IPAD)
		label.textAlignment = NSTextAlignmentRight;
	if (accuracy < 0)
    {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            label.text = [NSString stringWithFormat:@"%@ *%@*", getStringWithKey(ACCURACY_STRING), getStringWithKey(NA_STRING)];
        }
        
        else
        {
            label.text = [NSString stringWithFormat:@"%@ *%@*", getStringWithKey(ACCURACY_STRING), getStringWithKey(NA_STRING)];
        }
		
    }
	else
    {
		label.text = [NSString stringWithFormat:@"%@ *%ld%%*", getStringWithKey(ACCURACY_STRING), (long)accuracy];
    }
	label.attributedText = [OHASBasicMarkupParser attributedStringByProcessingMarkupInAttributedString:label.attributedText];
	[self addSubview:label];
	
	y += label.frame.size.height;

	htmlString = [htmlString stringByAppendingFormat:@"<br />%@ <b>%ld%%</b>", getStringWithKey(ACCURACY_STRING), (long)accuracy];

	NSString *activityString;
	if (session.activityType == ACTIVITY_SELECT)
		activityString = [getStringWithKey(SELECT_MY_ANSWERS_STRING) stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
	else
		activityString = [getStringWithKey(SPEAK_MY_ANSWERS_STRING) stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
	
	if (IS_IPAD) {
		label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 582, 25)];
		label.font = [UIFont fontWithName:ARIAL_FONT size:20];
	}
	else {
		label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(5, y, 240, 13)];
		label.font = [UIFont fontWithName:ARIAL_FONT size:10];
	}
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:52 / 255.0 green:87 / 255.0 blue:27 / 255.0 alpha:1.0];
	label.adjustsFontSizeToFitWidth = YES;
	label.minimumScaleFactor = 0.1;
	label.text = [NSString stringWithFormat:@"%@ *%@*", getStringWithKey(ACTIVITY_STRING), activityString];
	label.attributedText = [OHASBasicMarkupParser attributedStringByProcessingMarkupInAttributedString:label.attributedText];
	[self addSubview:label];
	
	y += label.frame.size.height;

	htmlString = [htmlString stringByAppendingFormat:@"<br />%@ <b>%@</b>", getStringWithKey(ACTIVITY_STRING), activityString];

	if (IS_IPAD) {
        
        if (screenWidth >= IPAD_12_9_INCH)
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 582, 25)];
        }
        
        else
        {
            label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, y, 582, 25)];
        }
		
		label.font = [UIFont fontWithName:ARIAL_FONT size:20];
	}
	else {
		label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(5, y, 240, 13)];
		label.font = [UIFont fontWithName:ARIAL_FONT size:10];
	}
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:52 / 255.0 green:87 / 255.0 blue:27 / 255.0 alpha:1.0];
	label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.1;
	label.text = getStringWithKey(RESULTS_TARGET_STRING);
	label.attributedText = [OHASBasicMarkupParser attributedStringByProcessingMarkupInAttributedString:label.attributedText];
	[self addSubview:label];
	
	y += label.frame.size.height;

	htmlString = [htmlString stringByAppendingFormat:@"<br />%@", getStringWithKey(RESULTS_TARGET_STRING)];

	if (IS_IPAD)
		y += 18;
	else
		y += 9;
	
	NSArray *titlesArray = [NSArray arrayWithObjects:
							getStringWithKey(WHY_STRING),
							getStringWithKey(HOW_STRING),
							getStringWithKey(WHERE_STRING),
							getStringWithKey(WHO_STRING),
							getStringWithKey(WHEN_STRING),
							getStringWithKey(WHAT_STRING),
							nil];
	
	NSArray *correctsArray = [NSArray arrayWithObjects:
							  [NSNumber numberWithInteger:session.whyCorrect],
							  [NSNumber numberWithInteger:session.howCorrect],
							  [NSNumber numberWithInteger:session.whereCorrect],
							  [NSNumber numberWithInteger:session.whoCorrect],
							  [NSNumber numberWithInteger:session.whenCorrect],
							  [NSNumber numberWithInteger:session.whatCorrect],
							  nil];

	NSArray *wrongsArray = [NSArray arrayWithObjects:
							[NSNumber numberWithInteger:session.whyWrong],
							[NSNumber numberWithInteger:session.howWrong],
							[NSNumber numberWithInteger:session.whereWrong],
							[NSNumber numberWithInteger:session.whoWrong],
							[NSNumber numberWithInteger:session.whenWrong],
							[NSNumber numberWithInteger:session.whatWrong],
							nil];

	for (NSInteger i = 0; i < titlesArray.count; i ++) {
		NSInteger col = i % 3;
		
		NSString *title = [titlesArray objectAtIndex:i];
		correctCount = [[correctsArray objectAtIndex:i] integerValue];
		wrongCount = [[wrongsArray objectAtIndex:i] integerValue];
		totalCount = correctCount + wrongCount;
		if (totalCount == 0)
			accuracy = -1;
		else
			accuracy = correctCount * 100 / totalCount;
		
		ReportAccuracyView *accuracyView;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                accuracyView = [[ReportAccuracyView alloc] initWithFrame:CGRectMake(27 + col * 260, y, 200, 200)];
            }
            else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
            {
                accuracyView = [[ReportAccuracyView alloc] initWithFrame:CGRectMake(27 + col * 240, y, 180, 180)];
            }
            else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
            {
                accuracyView = [[ReportAccuracyView alloc] initWithFrame:CGRectMake(27 + col * 170, y, 120, 120)];
            }
            else
            {
                accuracyView = [[ReportAccuracyView alloc] initWithFrame:CGRectMake(27 + col * 206, y, 136, 136)];
            }
        }
		else
        {
			accuracyView = [[ReportAccuracyView alloc] initWithFrame:CGRectMake(10 + col * 81, y, 68, 68)];
        }
		accuracyView.title = title;
		accuracyView.accuracy = accuracy;
		[accuracyView refresh];
		[self addSubview:accuracyView];
		
		if (col == 2)
        {
			if (IS_IPAD)
            {
                if (screenWidth >= IPAD_12_9_INCH)
                {
                    y += 250;
                }
                else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
                {
                    y += 220;
                }
                else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
                {
                    y += 150;
                }
                else
                {
                    y += 150;
                }
				
            }
			else
            {
				y += 75;
            }
		}
		
		if (accuracy < 0)
			htmlString = [htmlString stringByAppendingFormat:@"<br />&nbsp;&nbsp;%@: <b>%@</b>", title, getStringWithKey(NA_STRING)];
		else
			htmlString = [htmlString stringByAppendingFormat:@"<br />&nbsp;&nbsp;%@: <b>%ld%%</b>", title, (long)accuracy];
	}

	CGRect frame = self.frame;
	frame.size.height = y;
	self.frame = frame;
}

@end
