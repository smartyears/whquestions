//
//  ReportViewController.m
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "ReportViewController.h"
#import "StudentEntity.h"
#import "SessionEntity.h"
#import "NSData+CocoaDevUsersAdditions.h"
#import "ReportDetailViewController.h"

#define PHOTO_X_PAD			8
#define PHOTO_Y_PAD			8
#define PHOTO_WIDTH_PAD		143
#define PHOTO_HEIGHT_PAD	143

#define PHOTO_X_PHONE		4
#define PHOTO_Y_PHONE		4
#define PHOTO_WIDTH_PHONE	71.5
#define PHOTO_HEIGHT_PHONE	71.5


#define IPAD_SMALL        568
#define IPAD_9_7_INCH        1024
#define IPAD_11_INCH        1194
#define IPAD_12_9_INCH        1366
@interface ReportViewController ()

@end

@implementation ReportViewController

- (void)dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self name:RELOAD_STUDENTS object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	if (IS_IPAD)
		titleLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:40];
	else
		titleLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:20];
	titleLabel.characterSpacing = -0.1;
	titleLabel.oversampling = 4;
	titleLabel.text = getStringWithKey(RESULTS_STRING);
	
	photoViewsArray = [[NSMutableArray alloc] init];
	nameLabelsArray = [[NSMutableArray alloc] init];
	
	hud = [[MBProgressHUD alloc] initWithView:self.view];
	
	dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	shouldPlayEffectAudio = YES;
	
	[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(reloadStudents) name:RELOAD_STUDENTS object:nil];
	[self reloadStudents];
    //[contentScrollView setBackgroundColor:UIColor.redColor];
}

- (IBAction)onBackButton:(id)sender {
	[APP_DELEGATE stopEffectAudio];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)reloadStudents {
	if (tempStudentsArray)
		return;
	
	contentScrollView.hidden = YES;
	
	for (UIImageView *photoView in photoViewsArray)
		[photoView removeFromSuperview];
	[photoViewsArray removeAllObjects];
	
	for (FXLabel *nameLabel in nameLabelsArray)
		[nameLabel removeFromSuperview];
	[nameLabelsArray removeAllObjects];
	
	studentsArray = nil;
	
	studentsArray = [[NSMutableArray alloc] initWithArray:[StudentEntity allStudents]];
	tempStudentsArray = [[NSMutableArray alloc] initWithArray:studentsArray];
	
	[self.view addSubview:hud];
	[hud showAnimated:YES];
	
	currentIndex = -1;
	[self loadPhoto];
}

- (void)loadPhoto {
	currentIndex ++;
	
	if (studentsArray.count == 0) {
		NSInteger rowCount;
		if (IS_IPAD) {
            
            if (screenWidth >= IPAD_12_9_INCH)
            {
                rowCount = currentIndex / 5 + (currentIndex % 5 == 0 ? 0 : 1);
                contentScrollView.contentSize = CGSizeMake(screenWidth, 216 * rowCount);
            }
            else
            {
                rowCount = currentIndex / 5 + (currentIndex % 5 == 0 ? 0 : 1);
                contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width, 216 * rowCount);
            }
			
		}
		else {
			if (screenWidth == 568)
				rowCount = currentIndex / 6 + (currentIndex % 6 == 0 ? 0 : 1);
			else
				rowCount = currentIndex / 5 + (currentIndex % 5 == 0 ? 0 : 1);
			contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width, 108 * rowCount);
		}
		contentScrollView.hidden = NO;
		
		[hud hideAnimated:YES];
		[hud removeFromSuperview];
		
		[studentsArray addObjectsFromArray:tempStudentsArray];
		tempStudentsArray = nil;
		
		if (shouldPlayEffectAudio) {
			shouldPlayEffectAudio = NO;
		
			if (studentsArray.count > 0) {
				[APP_DELEGATE playEffectAudio:@"reportcard"];
			}
		}
	}
	else {
		loadPhotoThread = [[NSThread alloc] initWithTarget:self selector:@selector(loadPhotoThreadFunc) object:nil];
		[loadPhotoThread start];
	}
}

- (void)loadPhotoThreadFunc {
	@autoreleasepool {
		StudentEntity *student = [studentsArray objectAtIndex:0];
		
		[self performSelectorOnMainThread:@selector(addStudent:) withObject:[student headerImage] waitUntilDone:YES];
		
		[studentsArray removeObjectAtIndex:0];
	}
	
	loadPhotoThread = nil;
	
	[self performSelectorOnMainThread:@selector(loadPhoto) withObject:nil waitUntilDone:NO];
}

- (void)addStudent:(UIImage *)photo {
	NSInteger column;
	NSInteger row;
	if (IS_IPAD) {
		column = currentIndex % 5;
		row = currentIndex / 5;
	}
	else {
		if (screenWidth == 568) {
			column = currentIndex % 6;
			row = currentIndex / 6;
		}
		else {
			column = currentIndex % 5;
			row = currentIndex / 5;
		}
	}
	
	UIImageView *photoView;
	if (IS_IPAD) {
		photoView = [UIImageView.alloc initWithFrame:CGRectMake(63 + 185 * column, 216 * row, 159, 159)];
	}
	else {
		if (screenWidth == 568) {
			photoView = [UIImageView.alloc initWithFrame:CGRectMake(12 + 93 * column, 108 * row, 80, 80)];
		}
		else {
			photoView = [UIImageView.alloc initWithFrame:CGRectMake(14 + 93 * column, 108 * row, 80, 80)];
		}
	}
	photoView.image = [UIImage imageNamed:@"StudentsPhotoBackgroundPad.png"];
	photoView.tag = currentIndex;
	photoView.userInteractionEnabled = YES;
	
	UIImageView *photoImageView;
	if (IS_IPAD)
		photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PAD, PHOTO_Y_PAD, PHOTO_WIDTH_PAD, PHOTO_HEIGHT_PAD)];
	else
		photoImageView = [UIImageView.alloc initWithFrame:CGRectMake(PHOTO_X_PHONE, PHOTO_Y_PHONE, PHOTO_WIDTH_PHONE, PHOTO_HEIGHT_PHONE)];
	photoImageView.contentMode = UIViewContentModeScaleAspectFill;
	if (IS_IPAD)
		photoImageView.layer.cornerRadius = 17;
	else
		photoImageView.layer.cornerRadius = 8.5;
	photoImageView.clipsToBounds = YES;
	photoImageView.image = photo;
	photoImageView.transform = CGAffineTransformMakeRotation(-1.6 * M_PI / 180.0);
	[photoView addSubview:photoImageView];
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
	[photoView addGestureRecognizer:tapGesture];
	
	[photoViewsArray addObject:photoView];
	
	FXLabel *nameLabel;
	if (IS_IPAD) {
		nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 42)];
		nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:34];
	}
	else {
		nameLabel = [FXLabel.alloc initWithFrame:CGRectMake(CGRectGetMinX(photoView.frame), CGRectGetMaxY(photoView.frame), photoView.frame.size.width, 21)];
		nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:17];
	}
	nameLabel.backgroundColor = [UIColor clearColor];
	nameLabel.textColor = [UIColor colorWithRed:166 / 255.0 green:99 / 255.0 blue:36 / 255.0 alpha:1.0];
	nameLabel.strokeColor = [UIColor whiteColor];
	if (IS_IPAD)
		nameLabel.strokeSize = 4;
	else
		nameLabel.strokeSize = 2;
    nameLabel.minimumScaleFactor = 0.1;
	nameLabel.adjustsFontSizeToFitWidth = YES;
	nameLabel.textAlignment = NSTextAlignmentCenter;
	nameLabel.oversampling = 4;
	nameLabel.text = [[studentsArray objectAtIndex:0] name];
	
	[nameLabelsArray addObject:nameLabel];
	
	[contentScrollView addSubview:photoView];
	[contentScrollView addSubview:nameLabel];
}

- (void)onTapGesture:(UITapGestureRecognizer *)gesture {
	NSInteger index = gesture.view.tag;
	StudentEntity *student = [studentsArray objectAtIndex:index];
	NSArray *sessionsArray = [student allTestResult];
	
	if (sessionsArray.count == 0) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(STUDENT_NO_SESSIONS_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];
		return;
	}
	
	[APP_DELEGATE stopEffectAudio];

	ReportDetailViewController *detailViewController;
	if (IS_IPAD)
		detailViewController = [[ReportDetailViewController alloc] initWithNibName:@"ReportDetailViewControllerPad" bundle:nil];
	else
		detailViewController = [[ReportDetailViewController alloc] initWithNibName:@"ReportDetailViewControllerPhone" bundle:nil];
	detailViewController.student = student;
	detailViewController.sessionsArray = sessionsArray;

	[self.navigationController pushViewController:detailViewController animated:YES];
}

- (IBAction)onSyncButton:(id)sender {
	if (IS_IPAD)
		syncRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPad" bundle:nil];
	else
		syncRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPhone" bundle:nil];
	syncRestrictedPopupViewController.delegate = self;
	[syncRestrictedPopupViewController presentFromView:self.view];
}

// RestrictedPopupViewControllerDelegate
- (void)didEnterCorrect:(RestrictedPopupViewController *)controller {
	if (controller == syncRestrictedPopupViewController) {
		syncRestrictedPopupViewController = nil;
		
		[fileManager removeItemAtPath:tempExportDirectoryPath error:nil];
		[fileManager createDirectoryAtPath:tempExportDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil];
		[fileManager removeItemAtPath:syncFilePath error:nil];
		
		NSMutableDictionary *appSyncDictionary = [[NSMutableDictionary alloc] init];
		
		[appSyncDictionary setObject:@"WhQuestions" forKey:@"appName"];
		[appSyncDictionary setObject:@"WhQuestionsIcon.png" forKey:@"appIcon"];
		
		NSMutableArray *studentsDataArray = [[NSMutableArray alloc] init];
		
		BOOL bDataExist = NO;
		
		for (StudentEntity *student in studentsArray) {
			NSMutableDictionary *studentDataDictionary = [[NSMutableDictionary alloc] init];
			[studentDataDictionary setObject:student.name forKey:@"name"];
			
			NSMutableArray *exportArray = [[NSMutableArray alloc] init];
			NSArray *sessionsArray = [student allTestResult];
			for (SessionEntity *session in sessionsArray) {
				bDataExist = YES;
				
				NSMutableDictionary *exportDictionary = [[NSMutableDictionary alloc] init];
				
				[exportDictionary setObject:[dateFormatter stringFromDate:session.date] forKey:@"date"];
				
				NSString *description = [session getTRCDescription];
				[exportDictionary setObject:description forKey:@"description"];
				
				[exportArray addObject:exportDictionary];
			}
			
			[studentDataDictionary setObject:exportArray forKey:@"sessions"];
			
			[studentsDataArray addObject:studentDataDictionary];
		}
		
		if (bDataExist) {
			[appSyncDictionary setObject:studentsDataArray forKey:@"data"];
			
			[fileManager copyItemAtPath:[NSBundle.mainBundle pathForResource:@"Icon-72" ofType:@"png"] toPath:[tempExportDirectoryPath stringByAppendingPathComponent:@"WhQuestionsIcon.png"] error:nil];
			[appSyncDictionary writeToFile:[tempExportDirectoryPath stringByAppendingPathComponent:@"sessions.plist"] atomically:YES];
			
			NSURL *url = [NSURL fileURLWithPath:tempExportDirectoryPath];
			NSFileWrapper *dirWrapper = [[NSFileWrapper alloc] initWithURL:url options:0 error:nil];
			if (dirWrapper == nil) {
				return;
			}
			
			NSData *unzippedData = [dirWrapper serializedRepresentation];
			NSData *zippedData = [unzippedData gzipDeflate];
			[zippedData writeToFile:syncFilePath atomically:YES];
			
			if (documentInteractionController) {
				documentInteractionController.URL = [NSURL fileURLWithPath:syncFilePath];
			}
			else {
				documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:syncFilePath]];
				documentInteractionController.delegate = self;
			}
			
			BOOL bRet = [documentInteractionController presentOpenInMenuFromRect:syncButton.frame inView:self.view animated:YES];
			if (!bRet) {
				UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(CONFIRM_STRING) message:getStringWithKey(NO_TRC_ERROR_STRING) preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(NO_STRING) style:UIAlertActionStyleCancel handler:nil]];
                [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(YES_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:TRC_APP_URL] options:@{} completionHandler:nil];
                }]];
                [self presentViewController:alertController animated:true completion:nil];
			}
		}
		else {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(YOU_DONT_HAVE_ANY_REPORTS_STRING) preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
		}
	}
}

- (void)didCancel:(RestrictedPopupViewController *)controller {
	if (controller == syncRestrictedPopupViewController) {
		syncRestrictedPopupViewController = nil;
	}
}

@end
