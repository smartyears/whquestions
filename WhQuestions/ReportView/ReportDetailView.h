//
//  ReportDetailView.h
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SessionEntity;

@interface ReportDetailView : UIView {
	SessionEntity *session;
	NSString *htmlString;
}

@property (nonatomic, strong) SessionEntity *session;
@property (nonatomic, readonly) NSString *htmlString;

@end
