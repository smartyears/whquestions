//
//  ReportDetailViewController.h
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import <MessageUI/MessageUI.h>
#import "RestrictedPopupViewController.h"

@class StudentEntity;

@interface ReportDetailViewController : CustomViewController <UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate, RestrictedPopupViewControllerDelegate> {
	StudentEntity *student;
	NSArray *sessionsArray;
	
    IBOutlet UIView *containerView;
	IBOutlet UIButton *shareButton;
	IBOutlet UIImageView *photoView;
	IBOutlet UILabel *nameLabel;
	IBOutlet UILabel *reportCardLabel;
	IBOutlet UIScrollView *contentScrollView;
	
	NSString *htmlString;
	
	MBProgressHUD *hud;
	
	NSDateFormatter *dateFormatter;
	UIDocumentInteractionController *documentInteractionController;
	
	RestrictedPopupViewController *shareRestrictedPopupViewController;
}

@property (nonatomic, strong) StudentEntity *student;
@property (nonatomic, strong) NSArray *sessionsArray;

- (IBAction)onBackButton:(id)sender;
- (IBAction)onShareButton:(id)sender;

@end
