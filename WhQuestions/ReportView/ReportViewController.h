//
//  ReportViewController.h
//  WhQuesions
//
//  Created by Frank J. on 6/30/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "CustomViewController.h"
#import "FXLabel.h"
#import "MBProgressHUD.h"
#import "RestrictedPopupViewController.h"

@interface ReportViewController : CustomViewController <UIAlertViewDelegate, UIDocumentInteractionControllerDelegate, RestrictedPopupViewControllerDelegate> {
	IBOutlet FXLabel *titleLabel;
	IBOutlet UIScrollView *contentScrollView;
	IBOutlet UIButton *syncButton;
	
	NSMutableArray *studentsArray;
	NSMutableArray *tempStudentsArray;
	NSThread *loadPhotoThread;
	MBProgressHUD *hud;
	NSInteger currentIndex;
	
	NSMutableArray *photoViewsArray;
	NSMutableArray *nameLabelsArray;
	
	BOOL shouldPlayEffectAudio;
	
	NSDateFormatter *dateFormatter;
	UIDocumentInteractionController *documentInteractionController;
	
	RestrictedPopupViewController *syncRestrictedPopupViewController;
}

- (IBAction)onBackButton:(id)sender;
- (IBAction)onSyncButton:(id)sender;

- (void)reloadStudents;

@end
