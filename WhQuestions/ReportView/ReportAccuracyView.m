//
//  ReportAccuracyView.m
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "ReportAccuracyView.h"

@implementation ReportAccuracyView

@synthesize title;
@synthesize accuracy;
#define IPAD_SMALL        568
#define IPAD_9_7_INCH        1024
#define IPAD_11_INCH        1194
#define IPAD_12_9_INCH        1366
- (void)refresh {
	self.backgroundColor = [UIColor clearColor];
	
	UILabel *label;
	if (IS_IPAD) {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            label= [[UILabel alloc] initWithFrame:CGRectMake(10, 70, 190, 30)];
            label.font = [UIFont fontWithName:ARIAL_FONT size:22];
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            label= [[UILabel alloc] initWithFrame:CGRectMake(10, 70, 170, 30)];
            label.font = [UIFont fontWithName:ARIAL_FONT size:22];
        }
        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
        {
            label= [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 100, 30)];
            label.font = [UIFont fontWithName:ARIAL_FONT size:19];
        }
        else
        {
            label= [[UILabel alloc] initWithFrame:CGRectMake(10, 35, 100, 30)];
            label.font = [UIFont fontWithName:ARIAL_FONT size:19];
        }
		
		
	}
	else {
		label= [[UILabel alloc] initWithFrame:CGRectMake(5, 21, 58, 15)];
		label.font = [UIFont fontWithName:ARIAL_FONT size:11];
	}
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:245 / 255.0 green:246 / 255.0 blue:231 / 255.0 alpha:1.0];
	label.textAlignment = NSTextAlignmentCenter;
	label.minimumScaleFactor = 0.1;
	label.adjustsFontSizeToFitWidth = YES;
	label.text = [title uppercaseString];
	[self addSubview:label];
	
	if (IS_IPAD) {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 98, 190, 30)];
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:22];
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 98, 170, 30)];
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:22];
        }
        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 58, 100, 30)];
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:19];
        }
        else
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 58, 100, 30)];
            label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:19];
        }
		
		
	}
	else {
		label = [[UILabel alloc] initWithFrame:CGRectMake(5, 34, 58, 15)];
		label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:11];
	}
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:245 / 255.0 green:246 / 255.0 blue:231 / 255.0 alpha:1.0];
	label.textAlignment = NSTextAlignmentCenter;
    label.minimumScaleFactor = 0.1;
	label.adjustsFontSizeToFitWidth = YES;
	if (accuracy < 0)
		label.text = getStringWithKey(NA_STRING);
	else
		label.text = [NSString stringWithFormat:@"%ld%%", (long)accuracy];
	[self addSubview:label];
	
	[self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSaveGState(context);
	
	CGContextSetFillColorWithColor(context, [UIColor colorWithRed:69 / 255.0 green:75 / 255.0 blue:64 / 255.0 alpha:1.0].CGColor);
	if (IS_IPAD)
    {
        //background black color view
        if (screenWidth >= IPAD_12_9_INCH)
        {
            CGContextFillEllipseInRect(context, CGRectMake(12, 12, 180, 180));
        }
        else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
        {
            CGContextFillEllipseInRect(context, CGRectMake(12, 12, 160, 160));
        }
        else if (screenWidth >= IPAD_9_7_INCH && screenWidth < IPAD_11_INCH)
        {
            CGContextFillEllipseInRect(context, CGRectMake(12, 12, 97, 97));
        }
        else
        {
            CGContextFillEllipseInRect(context, CGRectMake(12, 12, 97, 97));
        }
		
    }
	else
    {
		CGContextFillEllipseInRect(context, CGRectMake(6, 6, 56, 56));
    }

	CGFloat angle;
	UIColor *strokeColor;
	if (accuracy < 0) {
		angle = 3 * M_PI / 2;
		strokeColor = [UIColor colorWithRed:198 / 255.0 green:198 / 255.0 blue:198 / 255.0 alpha:1.0];
	}
	else {
		angle = (accuracy * 360.0f / 100.0f - 90) * M_PI / 180.0;
		if (accuracy < 50) {
			strokeColor = [UIColor colorWithRed:192 / 255.0 green:24 / 255.0 blue:48 / 255.0 alpha:1.0];
		}
		else if (accuracy >= 50 && accuracy < 80) {
			strokeColor = [UIColor colorWithRed:245 / 255.0 green:175 / 255.0 blue:0 alpha:1.0];
		}
		else {
			strokeColor = [UIColor colorWithRed:165 / 255.0 green:217 / 255.0 blue:52 / 255.0 alpha:1.0];
		}
	}
	
	CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
	if (IS_IPAD) {
		CGContextSetLineWidth(context, 12);
		CGContextAddArc(context, rect.size.width / 2, rect.size.height / 2, rect.size.width / 2 - 6, -M_PI_2, angle, 0);
	}
	else {
		CGContextSetLineWidth(context, 6);
		CGContextAddArc(context, rect.size.width / 2, rect.size.height / 2, rect.size.width / 2 - 3, -M_PI_2, angle, 0);
	}
	CGContextStrokePath(context);
	
	CGContextRestoreGState(context);
}

@end
