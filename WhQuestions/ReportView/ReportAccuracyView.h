//
//  ReportAccuracyView.h
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportAccuracyView : UIView {
	NSString *title;
	NSInteger accuracy;
}

@property (nonatomic, strong) NSString *title;
@property (nonatomic) NSInteger accuracy;

- (void)refresh;

@end
