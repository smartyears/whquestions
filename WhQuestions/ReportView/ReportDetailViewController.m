//
//  ReportDetailViewController.m
//  WhQuesions
//
//  Created by Frank J. on 11/13/14.
//  Copyright (c) 2014 Smarty Ears. All rights reserved.
//

#import "ReportDetailViewController.h"
#import "FXLabel.h"
#import "StudentEntity.h"
#import "SessionEntity.h"
#import "ReportDetailView.h"
#import "NSData+CocoaDevUsersAdditions.h"


#define IPAD_SMALL        568
#define IPAD_9_7_INCH        1024
#define IPAD_11_INCH        1194
#define IPAD_12_9_INCH        1366

@interface ReportDetailViewController ()

@end

@implementation ReportDetailViewController

@synthesize student;
@synthesize sessionsArray;

- (void)dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self name:REPORT_GENERATED object:nil];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(onReportGenerated:) name:REPORT_GENERATED object:nil];
//    if (IS_IPAD)
//    {
//        if (screenWidth >= IPAD_12_9_INCH)
//        {
//            containerView. = CGSizeMake(screenWidth, screenHeight);
//        }
//        else
//        {
//            contentScrollView.contentSize = CGSizeMake(screenWidth, y);
//        }
//
//    }
//    else
//    {
//        contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width, y);
//    }
	dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	{
		FXLabel *label = [FXLabel.alloc initWithFrame:CGRectMake(0, 0, 1000, 100)];
		if (IS_IPAD)
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:24];
		else
			label.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:12];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor colorWithRed:109 / 255.0 green:166 / 255.0 blue:53 / 255.0 alpha:1.0];
		label.characterSpacing = -0.05;
		label.oversampling = 4;
		label.text = [getStringWithKey(SHARE_STRING) uppercaseString];
		[label sizeToFit];
		UIGraphicsBeginImageContextWithOptions(label.frame.size, NO, 0);
		[label.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[shareButton setImage:buttonImage forState:UIControlStateNormal];
	}
	
	photoView.image = student.headerImage;
	if (IS_IPAD)
		nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:33];
	else
		nameLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:17];
	nameLabel.text = student.name;
	if (IS_IPAD)
		reportCardLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:23];
	else
		reportCardLabel.font = [UIFont fontWithName:ARIAL_BOLD_FONT size:12];
	reportCardLabel.text = getStringWithKey(REPORT_CARD_STRING);
	
	NSString *tempString = [NSString stringWithFormat:@"<html>\n<head>\n</head>\n<body style='font-family:ArialMT; font-size:20px'>\n"];
	tempString = [tempString stringByAppendingFormat:@"Name: %@<br /><br /><br />\n", student.name];
	
	NSArray *titlesArray = [[NSArray alloc] initWithObjects:
							getStringWithKey(WHY_STRING),
							getStringWithKey(HOW_STRING),
							getStringWithKey(WHERE_STRING),
							getStringWithKey(WHO_STRING),
							getStringWithKey(WHEN_STRING),
							getStringWithKey(WHAT_STRING),
							nil];
	NSMutableArray *correctsArray = [[NSMutableArray alloc] init];
	NSMutableArray *wrongsArray = [[NSMutableArray alloc] init];
	for (NSInteger i = 0; i < 6; i ++) {
		[correctsArray addObject:[NSNumber numberWithInteger:0]];
		[wrongsArray addObject:[NSNumber numberWithInteger:0]];
	}
	
	NSInteger temp;

	NSInteger y = 0;
	for (NSInteger i = 0; i < sessionsArray.count; i ++) {
		SessionEntity *session = [sessionsArray objectAtIndex:i];
		
		{
			temp = [[correctsArray objectAtIndex:0] integerValue];
			temp += session.whyCorrect;
			[correctsArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInteger:temp]];
			temp = [[wrongsArray objectAtIndex:0] integerValue];
			temp += session.whyWrong;
			[wrongsArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInteger:temp]];
		}
		{
			temp = [[correctsArray objectAtIndex:1] integerValue];
			temp += session.howCorrect;
			[correctsArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithInteger:temp]];
			temp = [[wrongsArray objectAtIndex:1] integerValue];
			temp += session.howWrong;
			[wrongsArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithInteger:temp]];
		}
		{
			temp = [[correctsArray objectAtIndex:2] integerValue];
			temp += session.whereCorrect;
			[correctsArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithInteger:temp]];
			temp = [[wrongsArray objectAtIndex:2] integerValue];
			temp += session.whereWrong;
			[wrongsArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithInteger:temp]];
		}
		{
			temp = [[correctsArray objectAtIndex:3] integerValue];
			temp += session.whoCorrect;
			[correctsArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithInteger:temp]];
			temp = [[wrongsArray objectAtIndex:3] integerValue];
			temp += session.whoWrong;
			[wrongsArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithInteger:temp]];
		}
		{
			temp = [[correctsArray objectAtIndex:4] integerValue];
			temp += session.whenCorrect;
			[correctsArray replaceObjectAtIndex:4 withObject:[NSNumber numberWithInteger:temp]];
			temp = [[wrongsArray objectAtIndex:4] integerValue];
			temp += session.whenWrong;
			[wrongsArray replaceObjectAtIndex:4 withObject:[NSNumber numberWithInteger:temp]];
		}
		{
			temp = [[correctsArray objectAtIndex:5] integerValue];
			temp += session.whatCorrect;
			[correctsArray replaceObjectAtIndex:5 withObject:[NSNumber numberWithInteger:temp]];
			temp = [[wrongsArray objectAtIndex:5] integerValue];
			temp += session.whatWrong;
			[wrongsArray replaceObjectAtIndex:5 withObject:[NSNumber numberWithInteger:temp]];
		}
		
		ReportDetailView *detailView;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                detailView = [[ReportDetailView alloc] initWithFrame:CGRectMake(0, y, 902, 200)];
            }
            else
            {
                detailView = [[ReportDetailView alloc] initWithFrame:CGRectMake(0, y, 602, 100)];
            }
        }
		else
        {
			detailView = [[ReportDetailView alloc] initWithFrame:CGRectMake(0, y, 250, 100)];
        }
		detailView.session = session;
        //[detailView setBackgroundColor:UIColor.yellowColor];
		[contentScrollView addSubview:detailView];
		y += detailView.frame.size.height;
		
		if (i != sessionsArray.count - 1) {
			UIView *sepView;
			if (IS_IPAD)
				sepView = [[UIView alloc] initWithFrame:CGRectMake(10, y, 582, 2)];
			else
				sepView = [[UIView alloc] initWithFrame:CGRectMake(5, y, 240, 1)];
			sepView.backgroundColor = [UIColor colorWithRed:231 / 255.0 green:232 / 255.0 blue:217 / 255.0 alpha:1.0];
			[contentScrollView addSubview:sepView];
			
			if (IS_IPAD)
				y += 2;
			else
				y ++;
		}
		
		if (i == 0)
			tempString = [tempString stringByAppendingFormat:@"\n%@", detailView.htmlString];
		else
			tempString = [tempString stringByAppendingFormat:@"\n<br />______________________________________________________________________________________<br /><br />\n%@", detailView.htmlString];
	}
    if (IS_IPAD)
    {
        if (screenWidth >= IPAD_12_9_INCH)
        {
            contentScrollView.contentSize = CGSizeMake(screenWidth, y);
        }
        else
        {
            contentScrollView.contentSize = CGSizeMake(screenWidth, y);
        }
        
    }
    else
    {
        contentScrollView.contentSize = CGSizeMake(contentScrollView.frame.size.width, y);
    }
	
	
	tempString = [tempString stringByAppendingFormat:@"\n</body>\n</html>"];
	htmlString = tempString;
	
	for (NSInteger i = 0; i < titlesArray.count; i ++)
    {
		NSInteger correctCount = [[correctsArray objectAtIndex:i] integerValue];
		NSInteger wrongCount = [[wrongsArray objectAtIndex:i] integerValue];
		NSInteger totalCount = correctCount + wrongCount;
		NSInteger height;
		UIColor *color;
		if (totalCount == 0)
        {
			if (IS_IPAD)
            {
                if (screenWidth >= IPAD_12_9_INCH)
                {
                    height = 600;
                }
                else
                {
                    height = 350;
                }
            }
			else
            {
				height = 180;
            }
			color = [UIColor colorWithRed:198 / 255.0 green:198 / 255.0 blue:198 / 255.0 alpha:1.0];
		}
		else
        {
			NSInteger accuracy = correctCount * 100 / totalCount;
			if (IS_IPAD)
            {
                if (screenWidth >= IPAD_12_9_INCH)
                {
                    height = accuracy * 700 / 100;
                }
                else  if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
                {
                    height = accuracy * 550 / 100;
                }
                else
                {
                    height = accuracy * 500 / 100;
                }
            }
			else
            {
				height = accuracy * 180 / 100;
            }
			if (accuracy < 50)
            {
				color = [UIColor colorWithRed:192 / 255.0 green:24 / 255.0 blue:48 / 255.0 alpha:1.0];
			}
			else if (accuracy >= 50 && accuracy < 80)
            {
				color = [UIColor colorWithRed:245 / 255.0 green:175 / 255.0 blue:0 alpha:1.0];
			}
			else
            {
				color = [UIColor colorWithRed:165 / 255.0 green:217 / 255.0 blue:52 / 255.0 alpha:1.0];
			}
		}
        // all green orange bars one by one showing
		UIView *graphItemView;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                graphItemView = [[UIView alloc] initWithFrame:CGRectMake(28 + 70 * i, 240, 60, 700)];
            }
            else  if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
            {
                graphItemView = [[UIView alloc] initWithFrame:CGRectMake(28 + 70 * i, 200, 60, 550)];
            }
            else
            {
                graphItemView = [[UIView alloc] initWithFrame:CGRectMake(28 + 60 * i, 180, 50, 500)];
            }
        }
		else
        {
			graphItemView = [[UIView alloc] initWithFrame:CGRectMake(14 + 35 * i, 92, 25, 180)];
        }
		graphItemView.backgroundColor = [UIColor colorWithRed:158 / 255.0 green:175 / 255.0 blue:145 / 255.0 alpha:1.0];
		[containerView addSubview:graphItemView];
		UIView *accuracyView;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                //Background green orange color
                accuracyView = [[UIView alloc] initWithFrame:CGRectMake(0, 700 - height, 60, height)];
            }
            else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
            {
                //Background green orange color
                accuracyView = [[UIView alloc] initWithFrame:CGRectMake(0, 550 - height, 60, height)];
            }
            
            else
            {
                accuracyView = [[UIView alloc] initWithFrame:CGRectMake(0, 500 - height, 50, height)];
            }
			
        }
		else
        {
			accuracyView = [[UIView alloc] initWithFrame:CGRectMake(0, 180 - height, 25, height)];
        }
		accuracyView.backgroundColor = color;
		[graphItemView addSubview:accuracyView];
		
		UILabel *label;
		if (IS_IPAD)
        {
            if (screenWidth >= IPAD_12_9_INCH)
            {
                //how why, labels
                label = [[UILabel alloc] initWithFrame:CGRectMake(-210, 230, 470, 460)];
            }
            else if (screenWidth >= IPAD_11_INCH && screenWidth < IPAD_12_9_INCH)
            {
                //how why, labels
                label = [[UILabel alloc] initWithFrame:CGRectMake(-210, 230, 470, 100)];
            }
            else
            {
                label = [[UILabel alloc] initWithFrame:CGRectMake(-210, 230, 470, 50)];
            }
			label.font = [UIFont fontWithName:ARIAL_FONT size:33];
			label.shadowOffset = CGSizeMake(-1, 1);
		}
		else
        {
			label = [[UILabel alloc] initWithFrame:CGRectMake(-67, 80, 160, 25)];
			label.font = [UIFont fontWithName:ARIAL_FONT size:17];
			label.shadowOffset = CGSizeMake(-0.5, 0.5);
		}
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor whiteColor];
		label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.3];
        label.minimumScaleFactor = 0.1;
		label.adjustsFontSizeToFitWidth = YES;
		label.text = [[titlesArray objectAtIndex:i] uppercaseString];
		[graphItemView addSubview:label];
		
		label.transform = CGAffineTransformMakeRotation(-M_PI_2);
	}
	
	hud = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:hud];
	[hud showAnimated:YES];
    
   // [containerView setBackgroundColor:UIColor.redColor];
	[APP_DELEGATE generatePDF:htmlString];
}

- (IBAction)onBackButton:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)onReportGenerated:(NSNotification *)notification {
	[hud removeFromSuperview];
	hud = nil;
}

- (IBAction)onShareButton:(id)sender {
	if (IS_IPAD)
		shareRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPad" bundle:nil];
	else
		shareRestrictedPopupViewController = [[RestrictedPopupViewController alloc] initWithNibName:@"RestrictedPopupViewControllerPhone" bundle:nil];
	shareRestrictedPopupViewController.delegate = self;
	[shareRestrictedPopupViewController presentFromView:self.view];
}

// RestrictedPopupViewControllerDelegate
- (void)didEnterCorrect:(RestrictedPopupViewController *)controller {
	if (controller == shareRestrictedPopupViewController) {
		shareRestrictedPopupViewController = nil;
		
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(CANCEL_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(EMAIL_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self onEmailButton];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(PRINT_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self onPrintButton];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OPEN_IN_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self onOpenInButton];
        }]];
		if (IS_IPAD)
        {
            [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OPEN_IN_TRC_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self onOpenInTRCButton];
            }]];
            alertController.modalPresentationStyle = UIModalPresentationPopover;
            alertController.popoverPresentationController.sourceRect = shareButton.frame;
            alertController.popoverPresentationController.sourceView = self.view;
        }
        [self presentViewController:alertController animated:true completion:nil];
	}
}

- (void)didCancel:(RestrictedPopupViewController *)controller {
	if (controller == shareRestrictedPopupViewController) {
		shareRestrictedPopupViewController = nil;
	}
}

- (void)onEmailButton {
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *composeController = [[MFMailComposeViewController alloc] init];
		composeController.mailComposeDelegate = self;
		[composeController setSubject:getStringWithKey(APP_RESULTS_STRING)];
		[composeController addAttachmentData:[NSData dataWithContentsOfFile:reportPDFPath] mimeType:@"application/pdf" fileName:[reportPDFPath lastPathComponent]];
		
		[self presentViewController:composeController animated:YES completion:nil];
	}
	else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(ADD_ACCOUNT_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];
	}
}

// MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)onPrintButton{
	UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
	
	UIPrintInfo *printInfo = [UIPrintInfo printInfo];
	printInfo.outputType = UIPrintInfoOutputGeneral;
	printInfo.jobName = getStringWithKey(APP_RESULTS_STRING);
	printController.printInfo = printInfo;
	
	printController.printingItem = [NSURL fileURLWithPath:reportPDFPath];
	
	void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
	};
	
	if (IS_IPAD)
		[printController presentFromRect:shareButton.frame inView:self.view animated:YES completionHandler:completionHandler];
	else
		[printController presentAnimated:YES completionHandler:completionHandler];
}
- (void)onOpenInButton
{
	if (documentInteractionController)
    {
		documentInteractionController.URL = [NSURL fileURLWithPath:reportPDFPath];
	}
	else
    {
		documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:reportPDFPath]];
		documentInteractionController.delegate = self;
	}
	
	BOOL bRet = [documentInteractionController presentOpenInMenuFromRect:shareButton.frame inView:self.view animated:YES];
	if (!bRet) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(ERROR_STRING) message:getStringWithKey(OPEN_IN_ERROR_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(OK_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertController animated:true completion:nil];

	}
}

- (void)onOpenInTRCButton
{
	[fileManager removeItemAtPath:tempExportDirectoryPath error:nil];
	[fileManager createDirectoryAtPath:tempExportDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil];
	[fileManager removeItemAtPath:exportFilePath error:nil];
	
	NSMutableArray *exportArray = [[NSMutableArray alloc] init];
	
	for (SessionEntity *session in sessionsArray) {
		NSMutableDictionary *exportDictionary = [[NSMutableDictionary alloc] init];
		
		[exportDictionary setObject:@"WhQuestions" forKey:@"appName"];
		[exportDictionary setObject:@"WhQuestionsIcon.png" forKey:@"appIcon"];
		[exportDictionary setObject:[dateFormatter stringFromDate:session.date] forKey:@"date"];
		
		NSString *description = [session getTRCDescription];
		[exportDictionary setObject:description forKey:@"description"];
		
		[exportArray addObject:exportDictionary];
	}
	
	[fileManager copyItemAtPath:[NSBundle.mainBundle pathForResource:@"Icon-72" ofType:@"png"] toPath:[tempExportDirectoryPath stringByAppendingPathComponent:@"WhQuestionsIcon.png"] error:nil];
	[exportArray writeToFile:[tempExportDirectoryPath stringByAppendingPathComponent:@"sessions.plist"] atomically:YES];
	
	NSURL *url = [NSURL fileURLWithPath:tempExportDirectoryPath];
	NSFileWrapper *dirWrapper = [[NSFileWrapper alloc] initWithURL:url options:0 error:nil];
	if (dirWrapper == nil) {
		return;
	}
	
	NSData *unzippedData = [dirWrapper serializedRepresentation];
	NSData *zippedData = [unzippedData gzipDeflate];
	[zippedData writeToFile:exportFilePath atomically:YES];
	
	if (documentInteractionController){
		documentInteractionController.URL = [NSURL fileURLWithPath:exportFilePath];
	}
	else
    {
		documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:exportFilePath]];
		documentInteractionController.delegate = self;
	}
	
	BOOL bRet = [documentInteractionController presentOpenInMenuFromRect:shareButton.frame inView:self.view animated:YES];
	if (!bRet) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:getStringWithKey(CONFIRM_STRING) message:getStringWithKey(NO_TRC_ERROR_STRING) preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(NO_STRING) style:UIAlertActionStyleCancel handler:nil]];
        [alertController addAction:[UIAlertAction actionWithTitle:getStringWithKey(YES_STRING) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:TRC_APP_URL] options:@{} completionHandler:nil];
        }]];
        [self presentViewController:alertController animated:true completion:nil];

	}
}

@end
