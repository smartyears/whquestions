//
//  BackupManagementVC.m
//  Map
//
//  Created by Igor Poltavtsev on 02.10.16.
//  Copyright © 2016 Smarty Ears. All rights reserved.
//

#import "BackupManagementVC.h"
#import "BackupManager.h"
#import "MainViewController.h"

@interface BackupManagementVC ()
{
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *descriptionLabel;
    __weak IBOutlet UIButton *backupButton;
    __weak IBOutlet UILabel *backupDescriptionLabel;
    __weak IBOutlet UIButton *restoreButton;
    __weak IBOutlet UILabel *restoreDescriptionLabel;
}
@end

@implementation BackupManagementVC

- (void)viewDidLoad {
    if (!IS_IPAD) {
        backupButton.imageEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 20);
        restoreButton.imageEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 20);
    }
    
    titleLabel.text = getStringWithKey(BACKUP_MANAGER_TITLE);
    descriptionLabel.text = getStringWithKey(BACKUP_MANAGER_DESCRIPTION);
    [backupButton setTitle:getStringWithKey(BACKUP_MANAGER_BACKUP_BUTTON_TEXT) forState:UIControlStateNormal];
    backupDescriptionLabel.text = getStringWithKey(BACKUP_MANAGER_BACKUP_BUTTON_DESCRIPTION);
    [restoreButton setTitle:getStringWithKey(BACKUP_MANAGER_RESTORE_BUTTON_TEXT) forState:UIControlStateNormal];
    restoreDescriptionLabel.text = getStringWithKey(BACKUP_MANAGER_RESTORE_BUTTON_DESCRIPTION);
}

- (IBAction)dismissButton:(id)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)onBackupButton:(id)sender {
    [_delegate performBackup];
}

- (IBAction)onRestoreButton:(id)sender {
    [_delegate performRestore];
}

@end
