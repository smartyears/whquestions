//
//  BackupManagementVC.h
//  Map
//
//  Created by Igor Poltavtsev on 02.10.16.
//  Copyright © 2016 Smarty Ears. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface BackupManagementVC : UIViewController

@property (nonatomic, weak) MainViewController *delegate;

@end
